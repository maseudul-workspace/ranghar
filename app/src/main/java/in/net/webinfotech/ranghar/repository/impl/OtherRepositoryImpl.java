package in.net.webinfotech.ranghar.repository.impl;

import android.util.Log;

import com.google.gson.Gson;

import in.net.webinfotech.ranghar.domain.model.Hotel.HotelListWrapper;
import in.net.webinfotech.ranghar.domain.model.Other.PosterImageWrapper;
import in.net.webinfotech.ranghar.repository.APIclient;
import in.net.webinfotech.ranghar.repository.OtherRepository;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by Raj on 15-03-2019.
 */

public class OtherRepositoryImpl {
    OtherRepository mRepository;

    public OtherRepositoryImpl() {
        mRepository = APIclient.createService(OtherRepository.class);
    }

    public PosterImageWrapper getPosterImage(){
        PosterImageWrapper posterImageWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> removeItem = mRepository.getPosterImage();

            Response<ResponseBody> response = removeItem.execute();
            if(response.body() != null){
                responseBody = response.body().string();
                Log.e("ErrorMsg", responseBody);
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    posterImageWrapper = null;
                }else{
                    posterImageWrapper = gson.fromJson(responseBody, PosterImageWrapper.class);
                }
            } else {
                posterImageWrapper = null;
            }
        }catch (Exception e){
            Log.e("ErrorMsg", e.getMessage());
            posterImageWrapper = null;
        }
        return posterImageWrapper;
    }

}
