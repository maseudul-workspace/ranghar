package in.net.webinfotech.ranghar.domain.interactor;

/**
 * Created by Raj on 20-05-2019.
 */

public interface PaymentStatusUpdateInteractor {
    interface Callback{
        void onStatusUpdateSuccess();
        void onStatusUpdateFail();
    }
}
