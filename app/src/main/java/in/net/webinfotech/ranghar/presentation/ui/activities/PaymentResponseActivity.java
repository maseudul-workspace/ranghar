package in.net.webinfotech.ranghar.presentation.ui.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.net.webinfotech.ranghar.R;
import in.net.webinfotech.ranghar.domain.executor.impl.ThreadExecutor;
import in.net.webinfotech.ranghar.presentation.presenters.PaymentResponsePresenter;
import in.net.webinfotech.ranghar.presentation.presenters.WishListPresenter;
import in.net.webinfotech.ranghar.presentation.presenters.impl.PaymentResponsePresenterImpl;
import in.net.webinfotech.ranghar.threading.MainThreadImpl;

public class PaymentResponseActivity extends AppCompatActivity {

    @BindView(R.id.card_view_payment_success)
    View cardViewPaymentSuccess;
    @BindView(R.id.card_view_payment_failure)
    View cardViewPaymentFailure;
    @BindView(R.id.txt_view_order_id)
    TextView txtViewOrderId;
    @BindView(R.id.txt_view_payment_id)
    TextView txtViewPaymentId;
    @BindView(R.id.txt_view_purpose)
    TextView txtViewPurpose;
    @BindView(R.id.txt_view_payment_fail_reason)
    TextView txtViewPaymentFailReason;
    @BindView(R.id.txt_view_hotel_name)
    TextView txtViewHotelName;
    @BindView(R.id.txt_view_amount)
    TextView txtViewAmount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_response);
        ButterKnife.bind(this);
        loadPaymentResponse();
    }

    public void loadPaymentResponse(){
        String status = getIntent().getStringExtra("status");
        String orderId = getIntent().getStringExtra("orderId");
        String paymentId = getIntent().getStringExtra("paymentId");
        String hotelName = getIntent().getStringExtra("hotelName");
        String purpose = getIntent().getStringExtra("purpose");
        String failureReason = getIntent().getStringExtra("failureReason");
        String amount = getIntent().getStringExtra("amount");
        Log.e("ErrorMsg", "OdrerId: " + orderId);
        Log.e("ErrorMsg", "Payment Id: " + paymentId);
        if(status.equals("success")){
            txtViewHotelName.setText(hotelName);
            txtViewOrderId.setText(orderId);
            txtViewPaymentId.setText(paymentId);
            txtViewPurpose.setText(purpose);
            txtViewAmount.setText(amount);
            cardViewPaymentSuccess.setVisibility(View.VISIBLE);
        }else{
            cardViewPaymentFailure.setVisibility(View.VISIBLE);
            txtViewPaymentFailReason.setText(failureReason);
        }
    }

}
