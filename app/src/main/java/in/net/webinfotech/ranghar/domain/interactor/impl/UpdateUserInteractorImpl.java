package in.net.webinfotech.ranghar.domain.interactor.impl;

import in.net.webinfotech.ranghar.domain.executor.Executor;
import in.net.webinfotech.ranghar.domain.executor.MainThread;
import in.net.webinfotech.ranghar.domain.interactor.UpdateUserInteractor;
import in.net.webinfotech.ranghar.domain.interactor.base.AbstractInteractor;
import in.net.webinfotech.ranghar.domain.model.User.UserUpdateRespone;
import in.net.webinfotech.ranghar.repository.impl.UserRepositoryImpl;

/**
 * Created by Raj on 25-03-2019.
 */

public class UpdateUserInteractorImpl extends AbstractInteractor implements UpdateUserInteractor {

    Callback mCallback;
    UserRepositoryImpl mRepository;
    String api_key;
    int userId;
    String name;
    String mobile;
    String email;
    int state;
    int city;
    int location;
    String pin;

    public UpdateUserInteractorImpl(Executor threadExecutor,
                                    MainThread mainThread,
                                    Callback callback,
                                    UserRepositoryImpl repository,
                                    String api_key,
                                    int userId,
                                    String name,
                                    String mobile,
                                    String email,
                                    int state,
                                    int city,
                                    int location,
                                    String pin
                                    ) {
        super(threadExecutor, mainThread);
        this.mCallback = callback;
        this.mRepository = repository;
        this.api_key = api_key;
        this.userId = userId;
        this.name = name;
        this.mobile = mobile;
        this.email = email;
        this.state = state;
        this.city = city;
        this.location = location;
        this.pin = pin;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onUserUpdateFail(errorMsg);
            }
        });
    }

    private void postMessage(final String successMsg){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onUserUpdateSuccess(successMsg);
            }
        });
    }

    @Override
    public void run() {
        final UserUpdateRespone userUpdateRespone = mRepository.updateUser(api_key, userId, name, mobile, email, state, city, location, pin);
        if(userUpdateRespone == null){
            notifyError("Something went wrong");
        }else if(!userUpdateRespone.status){
            notifyError(userUpdateRespone.message);
        }else{
            postMessage(userUpdateRespone.message);
        }
    }
}
