package in.net.webinfotech.ranghar.domain.model.Hotel;

/**
 * Created by Raj on 02-05-2019.
 */

public class HotelFeatures {
    public String featureName;
    public Boolean isPresent;

    public HotelFeatures(String featureName, Boolean isPresent) {
        this.featureName = featureName;
        this.isPresent = isPresent;
    }
}
