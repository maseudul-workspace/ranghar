package in.net.webinfotech.ranghar.domain.model.Other;

/**
 * Created by Raj on 18-03-2019.
 */

public class HotelStars {
    public int drawableId;
    public int rating;

    public HotelStars(int drawableId, int rating) {
        this.drawableId = drawableId;
        this.rating = rating;
    }
}
