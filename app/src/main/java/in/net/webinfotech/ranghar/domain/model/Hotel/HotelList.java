package in.net.webinfotech.ranghar.domain.model.Hotel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Raj on 14-03-2019.
 */

public class HotelList {
    @SerializedName("hotel_id")
    @Expose
    public int hotel_id;

    @SerializedName("room_id")
    @Expose
    public int room_id;

    @SerializedName("main_image")
    @Expose
    public String main_image;

    @SerializedName("hotel_name")
    @Expose
    public String hotel_name;

    @SerializedName("room_size")
    @Expose
    public int room_size;

    @SerializedName("single_room_price")
    @Expose
    public double single_room_price;

    @SerializedName("double_room_price")
    @Expose
    public double double_room_price;

    @SerializedName("state")
    @Expose
    public String state;

    @SerializedName("city")
    @Expose
    public String city;

    @SerializedName("locality")
    @Expose
    public String locality;

    @SerializedName("availiblity")
    @Expose
    public RoomAvailability availability;

    public boolean isWishListPresent = false;
    public boolean isRoomAvailable = false;

}
