package in.net.webinfotech.ranghar.domain.interactor;

import in.net.webinfotech.ranghar.domain.model.Hotel.BookingConfirm;
import in.net.webinfotech.ranghar.domain.model.Hotel.BookingConfirmWrapper;

/**
 * Created by Raj on 15-05-2019.
 */

public interface HotelBookInteractor {
    interface Callback{
        void onBookingSuccess(BookingConfirm bookingConfirm);
        void onBookingFail(String errorMsg);
    }
}
