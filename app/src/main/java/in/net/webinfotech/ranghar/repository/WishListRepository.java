package in.net.webinfotech.ranghar.repository;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by Raj on 20-04-2019.
 */

public interface WishListRepository {
    @POST("api/wish_list/add_to_wish_list.php")
    @FormUrlEncoded
    Call<ResponseBody> addToWishList(@Field("api_key") String apiKey,
                                     @Field("user_id") int userId,
                                     @Field("hotel_id") int hotelId);

    @POST("api/wish_list/fetch_wish_list.php")
    @FormUrlEncoded
    Call<ResponseBody> fetchWishList(@Field("api_key") String apiKey,
                                     @Field("user_id") int userId);

    @POST("api/wish_list/delete_from_wish_list.php")
    @FormUrlEncoded
    Call<ResponseBody> deleteFromWishList(@Field("api_key") String apiKey,
                                     @Field("user_id") int userId,
                                     @Field("wish_list_id") int wishListId
                                     );

}
