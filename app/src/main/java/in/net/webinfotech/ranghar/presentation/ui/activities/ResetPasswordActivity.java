package in.net.webinfotech.ranghar.presentation.ui.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import in.net.webinfotech.ranghar.AndroidApplication;
import in.net.webinfotech.ranghar.R;
import in.net.webinfotech.ranghar.domain.executor.impl.ThreadExecutor;
import in.net.webinfotech.ranghar.domain.model.User.UserInfo;
import in.net.webinfotech.ranghar.presentation.presenters.ChangePasswordPresenter;
import in.net.webinfotech.ranghar.presentation.presenters.impl.ChangePasswordPresenterImpl;
import in.net.webinfotech.ranghar.threading.MainThreadImpl;

public class ResetPasswordActivity extends AppCompatActivity implements ChangePasswordPresenter.View {

    @BindView(R.id.edit_text_current_password)
    EditText editTextCurrentPassword;
    @BindView(R.id.edit_text_new_password)
    EditText editTextNewPassword;
    ChangePasswordPresenterImpl mPresenter;
    AndroidApplication androidApplication;
    @BindView(R.id.progressBar_layout)
    RelativeLayout progressBarLayout;
    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_password);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.backwhiteicon);
        androidApplication = (AndroidApplication) getApplicationContext();
        initialisePresenter();
    }

    public void initialisePresenter(){
        mPresenter = new ChangePasswordPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    @OnClick(R.id.btn_chnge_pswd) void changePassword(){
        if(editTextCurrentPassword.getText().toString().trim().isEmpty() || editTextNewPassword.getText().toString().trim().isEmpty()){
            Toast toast = Toast.makeText(this, "Some fields are empty", Toast.LENGTH_SHORT);
            TextView v = (TextView) toast.getView().findViewById(android.R.id.message);
            v.setTextColor(getResources().getColor(R.color.appColor));
            toast.show();
        }else {
            UserInfo userInfo = androidApplication.getUserInfo(this);
            if(userInfo != null){
                mPresenter.changePassword(userInfo.api_key, userInfo.user_id,
                                            editTextCurrentPassword.getText().toString(), editTextNewPassword.getText().toString());
                showProgressBar();
            }
        }
    }


    @Override
    public void showProgressBar() {
        progressBarLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgressBar() {
        progressBarLayout.setVisibility(View.GONE);
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransitionExit();
    }

    @Override
    public void startActivity(Intent intent) {
        super.startActivity(intent);
        overridePendingTransitionEnter();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransitionExit();
    }

    protected void overridePendingTransitionExit() {
        overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right);
    }

    protected void overridePendingTransitionEnter() {
        overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
