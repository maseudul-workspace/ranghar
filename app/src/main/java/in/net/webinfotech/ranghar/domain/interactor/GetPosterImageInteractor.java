package in.net.webinfotech.ranghar.domain.interactor;

import in.net.webinfotech.ranghar.domain.model.Other.PosterImage;

/**
 * Created by Raj on 15-03-2019.
 */

public interface GetPosterImageInteractor {
    interface Callback{
        void onGettingPosterImageSuccess(PosterImage posterImage);
        void onGettingPosterImageFail(String errorMsg);
    }
}
