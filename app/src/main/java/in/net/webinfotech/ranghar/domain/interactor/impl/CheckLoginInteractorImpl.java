package in.net.webinfotech.ranghar.domain.interactor.impl;

import in.net.webinfotech.ranghar.domain.executor.Executor;
import in.net.webinfotech.ranghar.domain.executor.MainThread;
import in.net.webinfotech.ranghar.domain.interactor.CheckLoginInteractor;
import in.net.webinfotech.ranghar.domain.interactor.base.AbstractInteractor;
import in.net.webinfotech.ranghar.domain.model.User.UserInfo;
import in.net.webinfotech.ranghar.domain.model.User.UserInfoWrapper;
import in.net.webinfotech.ranghar.repository.UserRepository;
import in.net.webinfotech.ranghar.repository.impl.UserRepositoryImpl;

/**
 * Created by Raj on 21-03-2019.
 */

public class CheckLoginInteractorImpl extends AbstractInteractor implements CheckLoginInteractor {

    Callback mCallback;
    UserRepositoryImpl mRepository;
    String email;
    String password;

    public CheckLoginInteractorImpl(Executor threadExecutor,
                                    MainThread mainThread,
                                    Callback callback,
                                    UserRepositoryImpl repository,
                                    String email,
                                    String password
                                    ) {
        super(threadExecutor, mainThread);
        this.mCallback = callback;
        this.mRepository = repository;
        this.email = email;
        this.password = password;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onLoginFail(errorMsg);
            }
        });
    }

    private void postMessage(final UserInfo userInfo){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onLoginSuccess(userInfo);
            }
        });
    }

    @Override
    public void run() {
        final UserInfoWrapper userInfoWrapper = mRepository.checkLogin(email, password);
        if(userInfoWrapper == null){
            notifyError("Something went wrong");
        }else if(!userInfoWrapper.status){
            notifyError(userInfoWrapper.message);
        }else{
            postMessage(userInfoWrapper.userInfo);
        }
    }
}
