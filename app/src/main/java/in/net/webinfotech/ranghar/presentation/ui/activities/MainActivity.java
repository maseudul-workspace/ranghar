package in.net.webinfotech.ranghar.presentation.ui.activities;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Handler;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PagerSnapHelper;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.tasks.OnSuccessListener;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import in.net.webinfotech.ranghar.AndroidApplication;
import in.net.webinfotech.ranghar.R;
import in.net.webinfotech.ranghar.domain.executor.MainThread;
import in.net.webinfotech.ranghar.domain.executor.impl.ThreadExecutor;
import in.net.webinfotech.ranghar.domain.model.Other.HotelRooms;
import in.net.webinfotech.ranghar.domain.model.Other.HotelStars;
import in.net.webinfotech.ranghar.domain.model.WishList.WishList;
import in.net.webinfotech.ranghar.presentation.presenters.MainActivityPresenter;
import in.net.webinfotech.ranghar.presentation.presenters.impl.MainActivityPresenterImpl;
import in.net.webinfotech.ranghar.presentation.ui.adapters.CitiesAdapter;
import in.net.webinfotech.ranghar.presentation.ui.adapters.HotelRoomTypeAdapter;
import in.net.webinfotech.ranghar.presentation.ui.adapters.HotelStarRatingAdapter;
import in.net.webinfotech.ranghar.threading.MainThreadImpl;
import in.net.webinfotech.ranghar.util.GetLocationClass;
import in.net.webinfotech.ranghar.util.GlideHelper;

public class MainActivity extends AppCompatActivity implements MainActivityPresenter.View, GetLocationClass.Callback {

    @BindView(R.id.recycler_view_places)
    RecyclerView recyclerView;
    @BindView(R.id.location_progress)
    RelativeLayout locationProgressLayout;
    MainActivityPresenterImpl mPresenter;
    String[] appPremisions = {
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.ACCESS_FINE_LOCATION
    };
    private static final int PERMISSIONS_REQUEST_CODE = 1240;

    int REQUEST_CHECK_SETTINGS = 1000;
    @BindView(R.id.img_view_poster_image)
    ImageView imgViewPoster;
    @BindView(R.id.recycler_view_hotels_star)
    RecyclerView recyclerViewHotelStars;
    @BindView(R.id.recycler_view_hotels_rooms)
    RecyclerView recyclerViewHotelRooms;
    @BindView(R.id.bottom_navigation)
    BottomNavigationView bottomNavigationView;
    AndroidApplication androidApplication;
    @BindView(R.id.coordinatorLayout)
    CoordinatorLayout coordinatorLayout;
    GetLocationClass getLocationClass;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        intialisePtesenter();
        initialiseLocationClass();
        mPresenter.getCities();
        mPresenter.getPosterImage();
        loadHotelsByStar();
        loadHotelRoomTypes();
        androidApplication = (AndroidApplication) getApplicationContext();
        if(androidApplication.getUserInfo(getApplicationContext()) != null){
            Log.e("ErrorMsg", "apiKEY " + androidApplication.getUserInfo(this).api_key);
            Log.e("ErrorMsg", "userId " + androidApplication.getUserInfo(this).user_id);
            mPresenter.fetchWishLists(androidApplication.getUserInfo(this).api_key, androidApplication.getUserInfo(this).user_id);
        }
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()){
                    case R.id.action_bookmarks:
                        if(androidApplication.getUserInfo(getApplicationContext()) != null){
                            Intent wishListIntent = new Intent(getApplicationContext(), WishListActivity.class);
                            startActivity(wishListIntent);
                        }else{
                            showLogInSnackbar();
                        }
                        break;
                    case R.id.action_booking:
//                        Intent intent1 = new Intent(getApplicationContext(), AboutActivity.class);
//                        startActivity(intent1);
                        break;
                    case R.id.action_account:
                        if(androidApplication.getUserInfo(getApplicationContext()) != null){
                            Log.e("ErrorMsg", "user info found");
                            Intent profileIntent = new Intent(getApplicationContext(), UserProfileActivity.class);
                            startActivity(profileIntent);
                        }else {
                            Log.e("ErrorMsg", "user info not found");
                            showLogInSnackbar();
                        }
                        break;
                }
                return false;
            }
        });
    }

    public void intialisePtesenter() {
        mPresenter = new MainActivityPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    public void initialiseLocationClass(){
        getLocationClass = new GetLocationClass(this, this, this);
    }

    @Override
    public void loadAdapter(CitiesAdapter adapter) {
        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(adapter);
    }

    @OnClick(R.id.search_relative_layout)
    void onSearchLayoutClick() {
        Intent intent = new Intent(this, SearchActivity.class);
        startActivity(intent);
    }

    @Override
    public void onCityClicked(int position, String cityName, int cityId) {
        if (position == 0) {
            if (checkAndRequestPermissions()) {
                getLocationClass.getCurrentLocation();
            }
        } else {
            goToCityHotelListActivity(cityName, cityId);
        }
    }

    public boolean checkAndRequestPermissions() {
        List<String> listPermissionsNeeded = new ArrayList<>();
        for (String perm : appPremisions) {
            if (ContextCompat.checkSelfPermission(this, perm) != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(perm);
            }
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), PERMISSIONS_REQUEST_CODE);
            return false;
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == PERMISSIONS_REQUEST_CODE) {
            HashMap<String, Integer> permissionResults = new HashMap<>();
            int deniedCount = 0;
            for (int i = 0; i < grantResults.length; i++) {
                if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                    permissionResults.put(permissions[i], grantResults[i]);
                    deniedCount++;
                }
            }

            if (deniedCount == 0) {
//               Do your task
                getLocationClass.getCurrentLocation();
            } else {
                for (Map.Entry<String, Integer> entry : permissionResults.entrySet()) {
                    String permName = entry.getKey();
                    int permResult = entry.getValue();
                    if (ActivityCompat.shouldShowRequestPermissionRationale(this, permName)) {
                        this.showAlertDialog("", "This app needs location permissions",
                                "Yes, Grant permissions",
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                        checkAndRequestPermissions();
                                    }
                                },
                                "Cancel",
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                        Toast.makeText(getApplicationContext(), "Permissions are required to access your location", Toast.LENGTH_SHORT).show();
                                    }
                                },
                                false);
                    } else {
                        this.showAlertDialog("", "You have denied some permissions. Allow all permissions to download file at [Setting] > [Permissions]",
                                "Go to settings",
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                                                Uri.fromParts("package", getPackageName(), null));
                                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                        startActivity(intent);
                                    }
                                },
                                "Cancel",
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                        Toast.makeText(getApplicationContext(), "Permissions are required to access your location", Toast.LENGTH_SHORT).show();
                                    }
                                },
                                false);
                        break;
                    }
                }


            }

        }
    }

    public AlertDialog showAlertDialog(
            String title, String msg, String positiveLabel,
            DialogInterface.OnClickListener positiveOnClick,
            String negativeLabel, DialogInterface.OnClickListener negativeOnClick,
            boolean isCancelable
    ) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(title);
        builder.setCancelable(isCancelable);
        builder.setMessage(msg);
        builder.setPositiveButton(positiveLabel, positiveOnClick);
        builder.setNegativeButton(negativeLabel, negativeOnClick);
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
        return alertDialog;
    }




    @Override
    protected void onStop() {
        super.onStop();

    }

    @Override
    protected void onPause() {
        super.onPause();

//        if (googleApiClient != null) {
//            stopLocationUpdates();
//        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CHECK_SETTINGS) {
            if (resultCode == -1) {
                showLocationProgressBar();
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }
                getLocationClass.getCurrentLocation();
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void showLocationProgressBar(){
        locationProgressLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLocationProgressBar(){
        locationProgressLayout.setVisibility(View.GONE);
    }

    @Override
    public void loadPosterImage(String imageUrl) {
        GlideHelper.setImageView(this, imgViewPoster, getResources().getString(R.string.base_url) + "/uploads/slider_image/" + imageUrl);
    }

    public void goToCityHotelListActivity(String cityName, int cityId){
        Intent intent = new Intent(this, CityHotelsListActivity.class);
        intent.putExtra("cityName", cityName);
        intent.putExtra("cityId", cityId);
        startActivity(intent);
    }

    public void loadHotelsByStar(){
        ArrayList<HotelStars> hotelStarsArrayList = new ArrayList<>();
        HotelStars hotelStars1 = new HotelStars(R.drawable.twostar, 2);
        hotelStarsArrayList.add(hotelStars1);
        HotelStars hotelStars2 = new HotelStars(R.drawable.threestarhotel, 3);
        hotelStarsArrayList.add(hotelStars2);
        HotelStars hotelStars3 = new HotelStars(R.drawable.fourstarroom, 4);
        hotelStarsArrayList.add(hotelStars3);
        HotelStars hotelStars4 = new HotelStars(R.drawable.fivestarhotel, 5);
        hotelStarsArrayList.add(hotelStars4);
        final HotelStarRatingAdapter adapter = new HotelStarRatingAdapter(this, hotelStarsArrayList);
        recyclerViewHotelStars.setAdapter(adapter);
        recyclerViewHotelStars.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        recyclerViewHotelStars.setHasFixedSize(true);
        PagerSnapHelper snapHelper = new PagerSnapHelper();
        snapHelper.attachToRecyclerView(recyclerViewHotelStars);
    }

    public void loadHotelRoomTypes(){
        ArrayList<HotelRooms> hotelRoomsArrayList = new ArrayList<>();
        HotelRooms hotelRooms1 = new HotelRooms(R.drawable.singleroomhotel, 1);
        hotelRoomsArrayList.add(hotelRooms1);
        HotelRooms hotelRooms2 = new HotelRooms(R.drawable.doubleroomhotel, 2);
        hotelRoomsArrayList.add(hotelRooms2);
        HotelRooms hotelRooms3 = new HotelRooms(R.drawable.tripleroomhotel, 3);
        hotelRoomsArrayList.add(hotelRooms3);
        final HotelRoomTypeAdapter adapter = new HotelRoomTypeAdapter(this, hotelRoomsArrayList);
        recyclerViewHotelRooms.setAdapter(adapter);
        recyclerViewHotelRooms.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        recyclerViewHotelRooms.setHasFixedSize(true);
        PagerSnapHelper snapHelper = new PagerSnapHelper();
        snapHelper.attachToRecyclerView(recyclerViewHotelRooms);

    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransitionExit();
    }

    @Override
    public void startActivity(Intent intent) {
        super.startActivity(intent);
        overridePendingTransitionEnter();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransitionExit();
    }

    protected void overridePendingTransitionExit() {
        overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right);
    }

    protected void overridePendingTransitionEnter() {
        overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
    }

    public void showLogInSnackbar(){
        Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content),"You must be logged in",Snackbar.LENGTH_LONG);
        snackbar.setAction("Log In", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent cartIntent = new Intent(getApplicationContext(), LoginActivity.class);
                startActivity(cartIntent);
            }
        });
        snackbar.show();
    }


}
