package in.net.webinfotech.ranghar.presentation.presenters.impl;

import android.content.Context;
import android.util.Log;

import java.util.ArrayList;

import in.net.webinfotech.ranghar.AndroidApplication;
import in.net.webinfotech.ranghar.domain.executor.Executor;
import in.net.webinfotech.ranghar.domain.executor.MainThread;
import in.net.webinfotech.ranghar.domain.interactor.FetchWishListInteractor;
import in.net.webinfotech.ranghar.domain.interactor.GetCitiesInteractor;
import in.net.webinfotech.ranghar.domain.interactor.GetPosterImageInteractor;
import in.net.webinfotech.ranghar.domain.interactor.impl.FetchWishListInteractorImpl;
import in.net.webinfotech.ranghar.domain.interactor.impl.GetCitiesInteractorImpl;
import in.net.webinfotech.ranghar.domain.interactor.impl.GetPosterImageInteractorImpl;
import in.net.webinfotech.ranghar.domain.model.Cities.City;
import in.net.webinfotech.ranghar.domain.model.Other.PosterImage;
import in.net.webinfotech.ranghar.domain.model.WishList.WishList;
import in.net.webinfotech.ranghar.presentation.presenters.MainActivityPresenter;
import in.net.webinfotech.ranghar.presentation.presenters.base.AbstractPresenter;
import in.net.webinfotech.ranghar.presentation.ui.adapters.CitiesAdapter;
import in.net.webinfotech.ranghar.repository.impl.CitiesRepositoryImpl;
import in.net.webinfotech.ranghar.repository.impl.OtherRepositoryImpl;
import in.net.webinfotech.ranghar.repository.impl.WishListRepositoryImpl;

/**
 * Created by Raj on 07-03-2019.
 */

public class MainActivityPresenterImpl extends AbstractPresenter implements MainActivityPresenter,
                                                                            GetCitiesInteractor.Callback,
                                                                            CitiesAdapter.Callback,
                                                                            GetPosterImageInteractor.Callback,
                                                                            FetchWishListInteractor.Callback
{

    CitiesAdapter adapter;
    MainActivityPresenter.View mView;
    Context mContext;
    GetCitiesInteractorImpl mInteractor;
    AndroidApplication androidApplication;
    GetPosterImageInteractorImpl posterImageInteractor;
    FetchWishListInteractorImpl fetchWishListInteractor;

    public MainActivityPresenterImpl(Executor executor,
                                     MainThread mainThread,
                                     Context context,
                                     MainActivityPresenter.View view
                                     ) {
        super(executor, mainThread);
        this.mContext = context;
        this.mView = view;
    }

    @Override
    public void getCities() {
        mInteractor = new GetCitiesInteractorImpl(mExecutor, mMainThread, new CitiesRepositoryImpl(), this);
        mInteractor.execute();
    }

    @Override
    public void getPosterImage() {
        posterImageInteractor = new GetPosterImageInteractorImpl(mExecutor,mMainThread, this, new OtherRepositoryImpl());
        posterImageInteractor.execute();
    }

    @Override
    public void fetchWishLists(String apiKey, int userId) {
        fetchWishListInteractor = new FetchWishListInteractorImpl(mExecutor, mMainThread, this, new WishListRepositoryImpl(), apiKey, userId);
        fetchWishListInteractor.execute();
    }

    @Override
    public void onGettingCitiesSuccess(City[] cities) {

        ArrayList<City> cityArrayList = new ArrayList<>();
        City city1 = new City();
        cityArrayList.add(city1);

        for(int i = 0; i < cities.length; i++){
            cityArrayList.add(cities[i]);
        }

        City city2 = new City();
        cityArrayList.add(city2);

        adapter = new CitiesAdapter(mContext, cityArrayList, this);
        mView.loadAdapter(adapter);
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        androidApplication.setCities(mContext, cities);
    }

    @Override
    public void onCityClicked(int position, String cityName, int cityId) {
        mView.onCityClicked(position, cityName, cityId);
    }

    @Override
    public void onGettingCitiesFail(String erroMsg) {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        if(androidApplication.getCities(mContext) != null){
            City[] cities = androidApplication.getCities(mContext);
            ArrayList<City> cityArrayList = new ArrayList<>();
            City city1 = new City();
            cityArrayList.add(city1);

            for(int i = 0; i < cities.length; i++){
                cityArrayList.add(cities[i]);
            }

            City city2 = new City();
            cityArrayList.add(city2);

            adapter = new CitiesAdapter(mContext, cityArrayList, this);
            mView.loadAdapter(adapter);
        }
    }

    @Override
    public void onGettingPosterImageSuccess(PosterImage posterImage) {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        androidApplication.setPosterImage(mContext, posterImage);
        mView.loadPosterImage(posterImage.image);
    }

    @Override
    public void onGettingPosterImageFail(String errorMsg) {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        if(androidApplication.getPosterImage(mContext) != null){
            mView.loadPosterImage(androidApplication.getPosterImage(mContext).image);
        }
    }

    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void stop() {

    }

    @Override
    public void destroy() {

    }

    @Override
    public void onGettingWishListSuccess(WishList[] wishLists) {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        androidApplication.setWishLists(wishLists);
    }

    @Override
    public void onGettingWishListFail(String errorMsg) {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        androidApplication.setWishLists(null);
    }
}
