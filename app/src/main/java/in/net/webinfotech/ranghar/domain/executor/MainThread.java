package in.net.webinfotech.ranghar.domain.executor;

/**
 * Created by Raj on 06-03-2019.
 */

public interface MainThread {
    /**
     * Make runnable operation run in the main thread.
     *
     * @param runnable The runnable to run.
     */
    void post(final Runnable runnable);
}
