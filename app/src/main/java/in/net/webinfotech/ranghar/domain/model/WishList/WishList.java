package in.net.webinfotech.ranghar.domain.model.WishList;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Raj on 20-04-2019.
 */

public class WishList {
    @SerializedName("wish_list_id")
    @Expose
    public int wishListId;

    @SerializedName("hotel_id")
    @Expose
    public int hotelId;

    @SerializedName("hotel_name")
    @Expose
    public String hotelName;

    @SerializedName("star_rating")
    @Expose
    public int starRating;

    @SerializedName("state")
    @Expose
    public String state;

    @SerializedName("city")
    @Expose
    public String city;

    @SerializedName("area")
    @Expose
    public String area;

    @SerializedName("image")
    @Expose
    public String image;

    public boolean isWishListPresent = true;

}
