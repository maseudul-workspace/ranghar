package in.net.webinfotech.ranghar.presentation.presenters.impl;

import android.content.Context;
import android.widget.TextView;
import android.widget.Toast;

import in.net.webinfotech.ranghar.R;
import in.net.webinfotech.ranghar.domain.executor.Executor;
import in.net.webinfotech.ranghar.domain.executor.MainThread;
import in.net.webinfotech.ranghar.domain.interactor.ChangePasswordInteractor;
import in.net.webinfotech.ranghar.domain.interactor.impl.ChangePasswordInteractorImpl;
import in.net.webinfotech.ranghar.presentation.presenters.ChangePasswordPresenter;
import in.net.webinfotech.ranghar.presentation.presenters.base.AbstractPresenter;
import in.net.webinfotech.ranghar.repository.impl.UserRepositoryImpl;

/**
 * Created by Raj on 25-03-2019.
 */

public class ChangePasswordPresenterImpl extends AbstractPresenter implements ChangePasswordPresenter,
                                                                                ChangePasswordInteractor.Callback{

    Context mContext;
    ChangePasswordPresenter.View mView;
    ChangePasswordInteractorImpl mInteractor;

    public ChangePasswordPresenterImpl(Executor executor,
                                       MainThread mainThread,
                                       Context context,
                                       ChangePasswordPresenter.View view
                                       ) {
        super(executor, mainThread);
        mContext = context;
        mView = view;
    }

    @Override
    public void changePassword(String apiKey, int userId, String currentPswd, String newPswd) {
        mInteractor = new ChangePasswordInteractorImpl(mExecutor, mMainThread, this,
                                                        new UserRepositoryImpl(), apiKey, userId, currentPswd, newPswd);
        mInteractor.execute();
    }

    @Override
    public void onChangePasswordSuccess() {
        mView.hideProgressBar();
        Toast toast = Toast.makeText(mContext, "Password changed successfully", Toast.LENGTH_SHORT);
        TextView v = (TextView) toast.getView().findViewById(android.R.id.message);
        v.setTextColor(mContext.getResources().getColor(R.color.appColor));
        toast.show();
    }

    @Override
    public void onChangePasswordFail(String errorMsg) {
        mView.hideProgressBar();
        Toast toast = Toast.makeText(mContext, errorMsg, Toast.LENGTH_SHORT);
        TextView v = (TextView) toast.getView().findViewById(android.R.id.message);
        v.setTextColor(mContext.getResources().getColor(R.color.appColor));
        toast.show();
    }

    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void stop() {

    }

    @Override
    public void destroy() {

    }
}
