package in.net.webinfotech.ranghar.presentation.routers;

/**
 * Created by Raj on 08-04-2019.
 */

public interface HotelListRouter {
    void goToHotelDetails(int id);
}
