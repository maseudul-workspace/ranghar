package in.net.webinfotech.ranghar.presentation.presenters.impl;

import android.content.Context;
import android.widget.TextView;
import android.widget.Toast;

import in.net.webinfotech.ranghar.AndroidApplication;
import in.net.webinfotech.ranghar.R;
import in.net.webinfotech.ranghar.domain.executor.Executor;
import in.net.webinfotech.ranghar.domain.executor.MainThread;
import in.net.webinfotech.ranghar.domain.interactor.FetchUserDetailsInteractor;
import in.net.webinfotech.ranghar.domain.interactor.UpdateUserInteractor;
import in.net.webinfotech.ranghar.domain.interactor.impl.FetchUserDetailsInteractorImpl;
import in.net.webinfotech.ranghar.domain.interactor.impl.UpdateUserInteractorImpl;
import in.net.webinfotech.ranghar.domain.model.User.UserInfo;
import in.net.webinfotech.ranghar.presentation.presenters.UserDetailsPresenter;
import in.net.webinfotech.ranghar.presentation.presenters.base.AbstractPresenter;
import in.net.webinfotech.ranghar.repository.impl.UserRepositoryImpl;

/**
 * Created by Raj on 21-03-2019.
 */

public class UserDetailsPresenterImpl extends AbstractPresenter implements UserDetailsPresenter,
                                                                            FetchUserDetailsInteractor.Callback,
                                                                            UpdateUserInteractor.Callback
{

    Context mContext;
    UserDetailsPresenter.View mView;
    FetchUserDetailsInteractorImpl mInteractor;
    AndroidApplication androidApplication;
    UpdateUserInteractorImpl updateUserInteractor;

    public UserDetailsPresenterImpl(Executor executor,
                                    MainThread mainThread,
                                    Context context,
                                    UserDetailsPresenter.View view
                                    ) {
        super(executor, mainThread);
        this.mContext = context;
        this.mView = view;
    }

    @Override
    public void fetchUserDetails() {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        if(androidApplication.getUserInfo(mContext) != null){
            UserInfo userInfo = androidApplication.getUserInfo(mContext);
            mInteractor = new FetchUserDetailsInteractorImpl(mExecutor, mMainThread,
                                                            this, new UserRepositoryImpl(), userInfo.api_key, userInfo.user_id);
            mInteractor.execute();
        }
    }

    @Override
    public void onGettingUserDetailsSuccess(UserInfo userInfo) {
        mView.hideProgressBar();
        mView.loadUserDetails(userInfo);
    }

    @Override
    public void onGettitngUserDetailFail(String errorMsg) {
        mView.hideProgressBar();
        Toast toast = Toast.makeText(mContext, errorMsg, Toast.LENGTH_SHORT);
        TextView v = (TextView) toast.getView().findViewById(android.R.id.message);
        v.setTextColor(mContext.getResources().getColor(R.color.appColor));
        toast.show();

    }

    @Override
    public void updateUser(String api_key, int userId, String name, String mobile, String email, int state, int city, int location, String pin) {
        updateUserInteractor = new UpdateUserInteractorImpl(mExecutor, mMainThread, this, new UserRepositoryImpl(),
                                                            api_key, userId, name, mobile, email, state, city, location, pin);
        updateUserInteractor.execute();
    }

    @Override
    public void onUserUpdateSuccess(String successMsg) {
        mView.hideProgressBar();
        Toast toast = Toast.makeText(mContext, "Profile update successfully", Toast.LENGTH_SHORT);
        TextView v = (TextView) toast.getView().findViewById(android.R.id.message);
        v.setTextColor(mContext.getResources().getColor(R.color.appColor));
        toast.show();
    }

    @Override
    public void onUserUpdateFail(String errorMsg) {
        mView.hideProgressBar();
        Toast toast = Toast.makeText(mContext, errorMsg, Toast.LENGTH_SHORT);
        TextView v = (TextView) toast.getView().findViewById(android.R.id.message);
        v.setTextColor(mContext.getResources().getColor(R.color.appColor));
        toast.show();
    }

    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void stop() {

    }

    @Override
    public void destroy() {

    }
}
