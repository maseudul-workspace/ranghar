package in.net.webinfotech.ranghar.presentation.ui.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Patterns;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import in.net.webinfotech.ranghar.R;
import in.net.webinfotech.ranghar.domain.executor.impl.ThreadExecutor;
import in.net.webinfotech.ranghar.presentation.presenters.SignUpPresenter;
import in.net.webinfotech.ranghar.presentation.presenters.impl.SignUpPresenterImpl;
import in.net.webinfotech.ranghar.presentation.routers.SignUpRouter;
import in.net.webinfotech.ranghar.threading.MainThreadImpl;

public class SignUpActivity extends AppCompatActivity implements SignUpPresenter.View, SignUpRouter{

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.edit_text_email)
    EditText editTextEmail;
    @BindView(R.id.edit_text_password)
    EditText editTextPassword;
    @BindView(R.id.edit_text_name)
    EditText editTextName;
    @BindView(R.id.edit_text_phone)
    EditText editTextPhone;
    @BindView(R.id.btn_sign_up)
    Button btnSignUp;
    @BindView(R.id.progressBar_layout)
    RelativeLayout progressBarLayout;
    SignUpPresenterImpl mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.backwhiteicon);
        initialisePresenter();
    }

    public void initialisePresenter(){
        mPresenter = new SignUpPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this, this);
    }

    @OnClick(R.id.btn_sign_up) void onSignUpBtnClicked(){
        if(editTextEmail.getText().toString().trim().isEmpty() ||
                editTextName.getText().toString().trim().isEmpty() ||
                editTextPhone.getText().toString().trim().isEmpty() ||
                editTextPassword.getText().toString().trim().isEmpty()
                )
        {
            Toast toast = Toast.makeText(this, "All fields are mandatory", Toast.LENGTH_SHORT);
            TextView v = (TextView) toast.getView().findViewById(android.R.id.message);
            v.setTextColor(getResources().getColor(R.color.appColor));
            toast.show();
        }else if(!Patterns.EMAIL_ADDRESS.matcher(editTextEmail.getText().toString()).matches()){
            Toast toast =Toast.makeText(this, "Please give a valid email", Toast.LENGTH_SHORT);
            TextView v = (TextView) toast.getView().findViewById(android.R.id.message);
            v.setTextColor(getResources().getColor(R.color.appColor));
            toast.show();
        }else{
            mPresenter.register(editTextName.getText().toString(), editTextPhone.getText().toString(), editTextEmail.getText().toString(), editTextPassword.getText().toString());
            showProgressBar();
        }
    }

    @Override
    public void goToMain() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransitionExit();
    }

    @Override
    public void startActivity(Intent intent) {
        super.startActivity(intent);
        overridePendingTransitionEnter();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransitionExit();
    }

    protected void overridePendingTransitionExit() {
        overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right);
    }

    protected void overridePendingTransitionEnter() {
        overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
    }

    @Override
    public void showProgressBar() {
        progressBarLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgressBar() {
        progressBarLayout.setVisibility(View.GONE);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
