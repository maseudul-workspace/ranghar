package in.net.webinfotech.ranghar.domain.interactor;

import in.net.webinfotech.ranghar.domain.model.User.UserInfo;
import in.net.webinfotech.ranghar.domain.model.User.UserInfoWrapper;

/**
 * Created by Raj on 21-03-2019.
 */

public interface CheckLoginInteractor {
    interface Callback{
        void onLoginSuccess(UserInfo userInfo);
        void onLoginFail(String errorMsg);
    }
}
