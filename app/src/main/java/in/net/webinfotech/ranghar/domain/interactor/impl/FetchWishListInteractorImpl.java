package in.net.webinfotech.ranghar.domain.interactor.impl;

import in.net.webinfotech.ranghar.domain.executor.Executor;
import in.net.webinfotech.ranghar.domain.executor.MainThread;
import in.net.webinfotech.ranghar.domain.interactor.FetchWishListInteractor;
import in.net.webinfotech.ranghar.domain.interactor.base.AbstractInteractor;
import in.net.webinfotech.ranghar.domain.model.WishList.WishList;
import in.net.webinfotech.ranghar.domain.model.WishList.WishListWrapper;
import in.net.webinfotech.ranghar.repository.WishListRepository;
import in.net.webinfotech.ranghar.repository.impl.WishListRepositoryImpl;

/**
 * Created by Raj on 20-04-2019.
 */

public class FetchWishListInteractorImpl extends AbstractInteractor implements FetchWishListInteractor{

    Callback mCallback;
    WishListRepositoryImpl mRepository;
    String apiKey;
    int userId;

    public FetchWishListInteractorImpl(Executor threadExecutor,
                                       MainThread mainThread,
                                       Callback callback,
                                       WishListRepositoryImpl repository,
                                       String apiKey,
                                       int userId
                                       ) {
        super(threadExecutor, mainThread);
        this.mCallback = callback;
        this.mRepository = repository;
        this.apiKey = apiKey;
        this.userId = userId;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingWishListFail(errorMsg);
            }
        });
    }

    private void postMessage(final WishList[] wishLists){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingWishListSuccess(wishLists);
            }
        });
    }

    @Override
    public void run() {
        final WishListWrapper wishListWrapper = mRepository.fetchWishList(apiKey, userId);
        if(wishListWrapper == null){
            notifyError("Something went wrong");
        }else if(!wishListWrapper.status){
            notifyError(wishListWrapper.message);
        }else{
            postMessage(wishListWrapper.wishLists);
        }
    }
}
