package in.net.webinfotech.ranghar.presentation.presenters;

import in.net.webinfotech.ranghar.presentation.presenters.base.BasePresenter;
import in.net.webinfotech.ranghar.presentation.ui.adapters.HotelListAdapter;

/**
 * Created by Raj on 15-03-2019.
 */

public interface GetHotelListPresenter extends BasePresenter {
    void getHotelList(String searchKey, int pageNo, int searchType, String checkInDate, String checkOutDate, String type);
    interface View{
        void loadAdapter(HotelListAdapter adapter, int totalPage);
        void hidePaginationProgressBar();
        void showShimmerAnimation();
        void hideShimmerAnimation();
    }
}
