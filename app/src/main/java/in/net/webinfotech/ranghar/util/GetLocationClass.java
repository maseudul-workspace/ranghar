package in.net.webinfotech.ranghar.util;

import android.app.Activity;
import android.content.Context;
import android.content.IntentSender;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

/**
 * Created by Raj on 03-04-2019.
 */

public class GetLocationClass implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, com.google.android.gms.location.LocationListener{


    LocationManager locationManager;
    Boolean gps_enabled = false;
    Boolean network_enabled = false;
    FusedLocationProviderClient fusedLocationProviderClient;
    GoogleApiClient googleApiClient;
    Location lastLocation;
    LocationRequest locationRequest;
    int REQUEST_CHECK_SETTINGS = 1000;
    Activity mActivity;

    public interface Callback{
        void hideLocationProgressBar();
        void showLocationProgressBar();

    }
    Context mContext;
    Callback mCallback;
    private static String TAG = "errorMsg";

    public GetLocationClass(Context mContext, Callback mCallback, Activity activity) {
        this.mContext = mContext;
        this.mCallback = mCallback;
        this.mActivity = activity;
    }

    public void getCurrentLocation(){
        try {
            mCallback.showLocationProgressBar();
            googleApiClient = new GoogleApiClient.Builder(mContext)
                    .addApi(LocationServices.API)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .build();

            locationRequest = LocationRequest.create();
            locationRequest.setInterval(1000);

            locationRequest.setFastestInterval(1000);
            locationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);

            checkForLocationRequestSetting(locationRequest);
            googleApiClient.connect();

        } catch (SecurityException ex) {
            ex.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        try {

            lastLocation = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);

            Log.e("ErrorMsg", "last location " + lastLocation);

//            LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, locationRequest, this);


            if (lastLocation != null) {
                Toast.makeText(mContext, "Latitiude: " + lastLocation.getLongitude() + " Longitude: " + lastLocation.getLatitude(), Toast.LENGTH_SHORT).show();
                Geocoder geoCoder = new Geocoder(mContext, Locale.getDefault()); //it is Geocoder
                StringBuilder builder = new StringBuilder();
                try {
                    List<Address> address = geoCoder.getFromLocation(lastLocation.getLatitude(), lastLocation.getLongitude(), 1);
//                    int maxLines = address.get(0).getMaxAddressLineIndex();
//                    for (int i=0; i<maxLines; i++) {
//                        String addressStr = address.get(0).getAddressLine(i);
//                        builder.append(addressStr);
//                        builder.append(" ");
//                    }
//
//                    String fnialAddress = builder.toString(); //This is the complete address.

                    Toast.makeText(mContext, "Current city: " + address.get(0).getSubAdminArea(), Toast.LENGTH_SHORT).show();

                    mCallback.hideLocationProgressBar();

                } catch (IOException e) {
                } catch (NullPointerException e) {
                }
            } else {
                LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, locationRequest, this);
            }

        } catch (SecurityException ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(Location location) {
        Toast.makeText(mContext, "Latitiude: " + location.getLongitude() + " Longitude: " + location.getLatitude(), Toast.LENGTH_SHORT).show();
        Geocoder geoCoder = new Geocoder(mContext, Locale.getDefault()); //it is Geocoder
        try {
            List<Address> address = geoCoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
//                    int maxLines = address.get(0).getMaxAddressLineIndex();
//                    for (int i=0; i<maxLines; i++) {
//                        String addressStr = address.get(0).getAddressLine(i);
//                        builder.append(addressStr);
//                        builder.append(" ");
//                    }
//
//                    String fnialAddress = builder.toString(); //This is the complete address.

            Toast.makeText(mContext, "Current city: " + address.get(0).getSubAdminArea(), Toast.LENGTH_SHORT).show();


        } catch (IOException e) {
        } catch (NullPointerException e) {
        }
        stopLocationUpdates();
        mCallback.hideLocationProgressBar();
    }

    protected void stopLocationUpdates() {
        LocationServices.FusedLocationApi.removeLocationUpdates(googleApiClient, this);
    }

    private void checkForLocationRequestSetting(LocationRequest locationRequest) {
        try {
            LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                    .addLocationRequest(locationRequest);

            PendingResult<LocationSettingsResult> result = LocationServices.SettingsApi.checkLocationSettings(googleApiClient, builder.build());

            result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
                @Override
                public void onResult(@NonNull LocationSettingsResult locationSettingsResult) {
                    final Status status = locationSettingsResult.getStatus();
                    final LocationSettingsStates locationSettingsStates = locationSettingsResult.getLocationSettingsStates();

                    switch (status.getStatusCode()) {
                        case LocationSettingsStatusCodes.SUCCESS:
                            // All location settings are satisfied. The client can
                            // initialize location requests here.
                            Log.e("ErrorMsg", "onResult: SUCCESS");
                            break;
                        case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                            Log.e("ErrorMsg", "onResult: RESOLUTION_REQUIRED");
                            // Location settings are not satisfied, but this can be fixed
                            // by showing the user a dialog.
                            try {
                                // Show the dialog by calling startResolutionForResult(),
                                // and check the result in onActivityResult().
                                status.startResolutionForResult(
                                        mActivity,
                                        REQUEST_CHECK_SETTINGS);
                            } catch (IntentSender.SendIntentException e) {
                                // Ignore the error.
                            }
                            break;
                        case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                            Log.d("ErrorMsg", "onResult: SETTINGS_CHANGE_UNAVAILABLE");
                            // Location settings are not satisfied. However, we have no way
                            // to fix the settings so we won't show the dialog.
                            break;
                    }

                }
            });

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

}
