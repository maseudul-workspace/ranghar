package in.net.webinfotech.ranghar.presentation.presenters;

import in.net.webinfotech.ranghar.presentation.presenters.base.BasePresenter;
import in.net.webinfotech.ranghar.presentation.ui.adapters.WishListAdapter;

/**
 * Created by Raj on 22-04-2019.
 */

public interface WishListPresenter extends BasePresenter{
    void getWishList();
    interface View{
        void loadAdapter(WishListAdapter adapter);
        void showShimmerAnimation();
        void hideShimmerAnimation();
    }
}
