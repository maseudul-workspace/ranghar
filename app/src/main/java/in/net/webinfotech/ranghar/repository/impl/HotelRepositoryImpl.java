package in.net.webinfotech.ranghar.repository.impl;

import android.util.Log;

import com.google.gson.Gson;

import in.net.webinfotech.ranghar.domain.model.Cities.CitiesWrapper;
import in.net.webinfotech.ranghar.domain.model.Hotel.BookingConfirmWrapper;
import in.net.webinfotech.ranghar.domain.model.Hotel.HotelAvailabilityResponse;
import in.net.webinfotech.ranghar.domain.model.Hotel.HotelDetailsWrapper;
import in.net.webinfotech.ranghar.domain.model.Hotel.HotelListWrapper;
import in.net.webinfotech.ranghar.domain.model.Hotel.PaymentStatusUpdateResponse;
import in.net.webinfotech.ranghar.repository.APIclient;
import in.net.webinfotech.ranghar.repository.HotelRepository;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by Raj on 15-03-2019.
 */

public class HotelRepositoryImpl {
    HotelRepository mRepository;

    public HotelRepositoryImpl() {
        mRepository = APIclient.createService(HotelRepository.class);
    }

    public HotelListWrapper getHotelList(String searchKey, int pageNo, int searchType, String checkInDate, String checkOutDate){
        HotelListWrapper hotelListWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> removeItem = mRepository.getHotelList(searchKey, pageNo, searchType, checkInDate, checkOutDate);

            Response<ResponseBody> response = removeItem.execute();
            if(response.body() != null){
                responseBody = response.body().string();
                Log.e("ErrorMsg", responseBody);
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    hotelListWrapper = null;
                }else{
                    hotelListWrapper = gson.fromJson(responseBody, HotelListWrapper.class);
                }
            } else {
                hotelListWrapper = null;
            }
        }catch (Exception e){
            Log.e("ErrorMsg", e.getMessage());
            hotelListWrapper = null;
        }
        return hotelListWrapper;
    }

    public HotelDetailsWrapper getHotelDetails(int hotelId, String checkIn, String checkOut){
        HotelDetailsWrapper detailsWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> details = mRepository.getHotelDetails(hotelId, checkIn, checkOut);

            Response<ResponseBody> response = details.execute();
            if(response.body() != null){
                responseBody = response.body().string();
                Log.e("ErrorMsg", responseBody);
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    detailsWrapper = null;
                }else{
                    detailsWrapper = gson.fromJson(responseBody, HotelDetailsWrapper.class);
                }
            } else {
                detailsWrapper = null;
            }
        }catch (Exception e){
            Log.e("ErrorMsg", e.getMessage());
            detailsWrapper = null;
        }
        return detailsWrapper;
    }

    public BookingConfirmWrapper bookHotel(int userId, int hotelId, int quantityDouble, int quantitySingle, String checkIn, String checkOut, int adult, int child, int extra_person, String apiKey){

        BookingConfirmWrapper bookingConfirmWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> book = mRepository.bookRoom(userId, hotelId, quantityDouble, quantitySingle, checkIn, checkOut, adult, child, extra_person, apiKey);

            Response<ResponseBody> response = book.execute();
            if(response.body() != null){
                responseBody = response.body().string();
                Log.e("ErrorMsg", responseBody);
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    bookingConfirmWrapper = null;
                }else{
                    bookingConfirmWrapper = gson.fromJson(responseBody, BookingConfirmWrapper.class);
                }
            } else {
                bookingConfirmWrapper = null;
            }
        }catch (Exception e){
            Log.e("ErrorMsg", e.getMessage());
            bookingConfirmWrapper = null;
        }
        return bookingConfirmWrapper;
    }

    public PaymentStatusUpdateResponse paymentStatusUpdate(String apiKey, int bookingId, String orderId, String paymentId, int paymentStatus, int userId){
        PaymentStatusUpdateResponse paymentStatusUpdateResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> update = mRepository.paymentStatusUpdate(userId, bookingId, orderId, paymentId, paymentStatus, apiKey);

            Response<ResponseBody> response = update.execute();
            if(response.body() != null){
                responseBody = response.body().string();
                Log.e("ErrorMsg", responseBody);
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    paymentStatusUpdateResponse = null;
                }else{
                    paymentStatusUpdateResponse = gson.fromJson(responseBody, PaymentStatusUpdateResponse.class);
                }
            } else {
                paymentStatusUpdateResponse = null;
            }
        }catch (Exception e){
            Log.e("ErrorMsg", e.getMessage());
            paymentStatusUpdateResponse = null;
        }
        return paymentStatusUpdateResponse;
    }

}
