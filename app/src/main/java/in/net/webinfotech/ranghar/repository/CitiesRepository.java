package in.net.webinfotech.ranghar.repository;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.POST;

/**
 * Created by Raj on 07-03-2019.
 */

public interface CitiesRepository {
    @POST("api/city/city_fetch.php")
    Call<ResponseBody> getCities();
}
