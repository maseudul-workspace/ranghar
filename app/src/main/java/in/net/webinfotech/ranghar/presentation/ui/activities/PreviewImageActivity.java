package in.net.webinfotech.ranghar.presentation.ui.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.github.chrisbanes.photoview.PhotoView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import in.net.webinfotech.ranghar.R;
import in.net.webinfotech.ranghar.util.GlideHelper;

public class PreviewImageActivity extends AppCompatActivity {

    @BindView(R.id.photoview)
    PhotoView photoView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preview_image);
        ButterKnife.bind(this);
        String imageUrl = getIntent().getStringExtra("imageUrl");
        GlideHelper.setImageView(this, photoView, getResources().getString(R.string.base_url) + "uploads/hotel_image/" + imageUrl);
    }

    @OnClick(R.id.img_cancel) void onCancelClicked(){
        finish();
    }

}
