package in.net.webinfotech.ranghar.domain.interactor;

import in.net.webinfotech.ranghar.domain.model.Cities.City;

/**
 * Created by Raj on 07-03-2019.
 */

public interface GetCitiesInteractor {
    interface Callback{
        void onGettingCitiesSuccess(City[] cities);
        void onGettingCitiesFail(String erroMsg);
    }
}
