package in.net.webinfotech.ranghar.presentation.presenters.impl;

import android.content.Context;

import in.net.webinfotech.ranghar.AndroidApplication;
import in.net.webinfotech.ranghar.domain.executor.Executor;
import in.net.webinfotech.ranghar.domain.executor.MainThread;
import in.net.webinfotech.ranghar.domain.interactor.PaymentStatusUpdateInteractor;
import in.net.webinfotech.ranghar.domain.interactor.impl.PaymentStatusUpdateInteractorImpl;
import in.net.webinfotech.ranghar.domain.model.User.UserInfo;
import in.net.webinfotech.ranghar.presentation.presenters.PaymentResponsePresenter;
import in.net.webinfotech.ranghar.presentation.presenters.base.AbstractPresenter;
import in.net.webinfotech.ranghar.repository.impl.HotelRepositoryImpl;

/**
 * Created by Raj on 20-05-2019.
 */

public class PaymentResponsePresenterImpl extends AbstractPresenter implements PaymentResponsePresenter,
                                                                                PaymentStatusUpdateInteractor.Callback {

    Context mContext;
    PaymentStatusUpdateInteractorImpl mInteractor;
    AndroidApplication androidApplication;

    public PaymentResponsePresenterImpl(Executor executor, MainThread mainThread, Context context) {
        super(executor, mainThread);
        this.mContext = context;
    }

    @Override
    public void paymentStatusUpdate(int bookingId, String orderId, String paymentId) {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        mInteractor = new PaymentStatusUpdateInteractorImpl(mExecutor, mMainThread, this, new HotelRepositoryImpl(), userInfo.api_key, bookingId, orderId, paymentId, 2, userInfo.user_id);
        mInteractor.execute();
    }

    @Override
    public void onStatusUpdateSuccess() {

    }

    @Override
    public void onStatusUpdateFail() {

    }


    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void stop() {

    }

    @Override
    public void destroy() {

    }
}
