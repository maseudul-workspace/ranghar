package in.net.webinfotech.ranghar.domain.interactor.impl;

import in.net.webinfotech.ranghar.domain.executor.Executor;
import in.net.webinfotech.ranghar.domain.executor.MainThread;
import in.net.webinfotech.ranghar.domain.interactor.GetPosterImageInteractor;
import in.net.webinfotech.ranghar.domain.interactor.base.AbstractInteractor;
import in.net.webinfotech.ranghar.domain.model.Hotel.HotelList;
import in.net.webinfotech.ranghar.domain.model.Other.PosterImage;
import in.net.webinfotech.ranghar.domain.model.Other.PosterImageWrapper;
import in.net.webinfotech.ranghar.repository.impl.OtherRepositoryImpl;

/**
 * Created by Raj on 15-03-2019.
 */

public class GetPosterImageInteractorImpl extends AbstractInteractor implements GetPosterImageInteractor{

    GetPosterImageInteractor.Callback mCallback;
    OtherRepositoryImpl mRepository;

    public GetPosterImageInteractorImpl(Executor threadExecutor,
                                        MainThread mainThread,
                                        GetPosterImageInteractor.Callback callback,
                                        OtherRepositoryImpl repository
                                        ) {
        super(threadExecutor, mainThread);
        this.mCallback = callback;
        this.mRepository = repository;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingPosterImageFail(errorMsg);
            }
        });
    }

    private void postMessage(final PosterImage posterImage){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingPosterImageSuccess(posterImage);
            }
        });
    }


    @Override
    public void run() {
        final PosterImageWrapper posterImageWrapper = mRepository.getPosterImage();
        if(posterImageWrapper == null){
            notifyError("Something went wrong");
        }else if(!posterImageWrapper.status){
            notifyError(posterImageWrapper.message);
        }else {
            postMessage(posterImageWrapper.posterImage);
        }
    }
}
