package in.net.webinfotech.ranghar.domain.interactor;

import in.net.webinfotech.ranghar.domain.model.User.UserInfo;

/**
 * Created by Raj on 21-03-2019.
 */

public interface CreateUserInteractor {
    interface Callback{
        void onUserCreateSuccess(UserInfo userInfo);
        void onUserCreateFail(String errorMsg);
    }
}
