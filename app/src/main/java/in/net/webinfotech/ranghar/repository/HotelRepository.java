package in.net.webinfotech.ranghar.repository;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by Raj on 15-03-2019.
 */

public interface HotelRepository {
    @POST("api/hotel/hotel_search.php")
    @FormUrlEncoded
    Call<ResponseBody> getHotelList(@Field("search_key") String search_key,
                                    @Field("page_no") int page_no,
                                    @Field("search_type") int search_type,
                                    @Field("check_in") String checkInDate,
                                    @Field("check_out") String checkOutDate
    );

    @POST("api/hotel/hotel_details.php")
    @FormUrlEncoded
    Call<ResponseBody> getHotelDetails(@Field("hotel_id") int hotel_id,
                                       @Field("check_in") String checkInDate,
                                       @Field("check_out") String checkOutDate
                                       );

    @POST("api/booking/booking.php")
    @FormUrlEncoded
    Call<ResponseBody> bookRoom(@Field("user_id") int user_id,
                                              @Field("hotel_id") int hotel_id,
                                              @Field("quantity_double") int quantity_double,
                                              @Field("quantity_single") int quantity_single,
                                              @Field("check_in") String checkInDate,
                                              @Field("check_out") String checkOutDate,
                                              @Field("adult_quantity") int adult_quantity,
                                              @Field("child_quantity") int child_quantity,
                                              @Field("extra_person_quantity") int extra_person_quantity,
                                              @Field("api_key") String api_key
                                              );


    @POST("api/booking/payment_status_update.php")
    @FormUrlEncoded
    Call<ResponseBody> paymentStatusUpdate(@Field("user_id") int user_id,
                                @Field("booking_id") int booking_id,
                                @Field("order_id") String order_id,
                                @Field("payment_id") String payment_id,
                                @Field("payment_status") int payment_status,
                                @Field("api_key") String api_key
    );

}
