package in.net.webinfotech.ranghar.domain.interactor.impl;

import in.net.webinfotech.ranghar.domain.executor.Executor;
import in.net.webinfotech.ranghar.domain.executor.MainThread;
import in.net.webinfotech.ranghar.domain.interactor.PaymentStatusUpdateInteractor;
import in.net.webinfotech.ranghar.domain.interactor.base.AbstractInteractor;
import in.net.webinfotech.ranghar.domain.model.Hotel.BookingConfirm;
import in.net.webinfotech.ranghar.domain.model.Hotel.PaymentStatusUpdateResponse;
import in.net.webinfotech.ranghar.repository.impl.HotelRepositoryImpl;

/**
 * Created by Raj on 20-05-2019.
 */

public class PaymentStatusUpdateInteractorImpl extends AbstractInteractor implements PaymentStatusUpdateInteractor {

    Callback mCallback;
    HotelRepositoryImpl mRepository;
    String apiKey;
    int bookingId;
    String orderId;
    String paymentId;
    int paymentStatus;
    int userId;

    public PaymentStatusUpdateInteractorImpl(Executor threadExecutor,
                                             MainThread mainThread,
                                             Callback callback,
                                             HotelRepositoryImpl repository,
                                             String apiKey,
                                             int bookingId,
                                             String orderId,
                                             String paymentId,
                                             int paymentStatus,
                                             int userId) {
        super(threadExecutor, mainThread);
        this.apiKey = apiKey;
        this.bookingId = bookingId;
        this.mCallback = callback;
        this.mRepository = repository;
        this.orderId = orderId;
        this.paymentId = paymentId;
        this.userId = userId;
        this.paymentStatus = paymentStatus;
    }

    private void notifyError() {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onStatusUpdateFail();
            }
        });
    }

    private void postMessage(){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onStatusUpdateSuccess();
            }
        });
    }

    @Override
    public void run() {
        PaymentStatusUpdateResponse paymentStatusUpdateResponse = mRepository.paymentStatusUpdate(apiKey, bookingId, orderId, paymentId, paymentStatus, userId);
        if(paymentStatusUpdateResponse == null){
            notifyError();
        }else if(!paymentStatusUpdateResponse.status){
            notifyError();
        }else{
            postMessage();
        }
    }
}
