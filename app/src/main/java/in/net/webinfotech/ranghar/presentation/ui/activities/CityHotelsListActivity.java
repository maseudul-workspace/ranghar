package in.net.webinfotech.ranghar.presentation.ui.activities;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.AbsListView;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.shimmer.ShimmerFrameLayout;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import in.net.webinfotech.ranghar.AndroidApplication;
import in.net.webinfotech.ranghar.R;
import in.net.webinfotech.ranghar.domain.executor.impl.ThreadExecutor;
import in.net.webinfotech.ranghar.domain.model.Room.Room;
import in.net.webinfotech.ranghar.presentation.presenters.GetHotelListPresenter;
import in.net.webinfotech.ranghar.presentation.presenters.impl.GetHotelListPresenterImpl;
import in.net.webinfotech.ranghar.presentation.routers.HotelListRouter;
import in.net.webinfotech.ranghar.presentation.ui.adapters.HotelListAdapter;
import in.net.webinfotech.ranghar.presentation.ui.dialogs.DatePickerFragment;
import in.net.webinfotech.ranghar.presentation.ui.dialogs.RoomSelectDialog;
import in.net.webinfotech.ranghar.threading.MainThreadImpl;

public class CityHotelsListActivity extends AppCompatActivity implements DatePickerDialog.OnDateSetListener,
                                                                          GetHotelListPresenter.View,
                                                                            HotelListRouter, RoomSelectDialog.Callback
{

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.txt_view_check_in_date)
    TextView txtViewCheckInDate;
    @BindView(R.id.txt_view_check_out_date)
    TextView txtViewCheckOutDate;
    String checkInDate;
    String checkOutDate;
    Boolean isCheckIn;
    android.support.v4.app.DialogFragment dialogFragment;
    int finalAdultCount = 1;
    int finalChildCount = 0;
    int finalRoomCount = 1;
    @BindView(R.id.txt_view_final_room_count)
    TextView txtViewFinalRoomCount;
    @BindView(R.id.txt_view_final_adult)
    TextView txtViewFinalAdult;
    @BindView(R.id.txt_view_final_child)
    TextView txtViewFinalChild;
    @BindView(R.id.img_view_final_adult)
    ImageView imgViewFinalAdult;
    @BindView(R.id.img_view_final_child)
    ImageView imgViewFinalChild;
    String cityName;
    String cityId;
    GetHotelListPresenterImpl mPresenter;
    @BindView(R.id.recycler_view_room_list)
    RecyclerView recyclerViewRoomList;
    Boolean isScrolling = false;
    Integer currentItems;
    Integer totalItems;
    Integer scrollOutItems;
    int pageNo = 1;
    int totalPage = 1;
    LinearLayoutManager linearLayoutManager;
    @BindView(R.id.pagination_progressbar_layout)
    RelativeLayout paginationProgressBar;
    RoomSelectDialog roomSelectDialog;
    @BindView(R.id.shimmer_layout)
    ShimmerFrameLayout shimmerFrameLayout;
    AndroidApplication androidApplication;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_city_hotels_list);
        ButterKnife.bind(this);
        initialisePresenter();
        setSupportActionBar(toolbar);
        cityName = getIntent().getStringExtra("cityName");
        cityId = Integer.toString(getIntent().getIntExtra("cityId", 1));
        getSupportActionBar().setTitle(cityName);
        showShimmerAnimation();
    }

    public void initialisePresenter(){
        mPresenter = new GetHotelListPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this, this);

    }


    public void initialiseRoomSelectDialog(){
        roomSelectDialog = new RoomSelectDialog(this, this, this);
    }

    public void setUpDatePicker(){
        dialogFragment = new DatePickerFragment();
        androidApplication = (AndroidApplication) getApplicationContext();
        checkInDate = androidApplication.getCheckInDate();
        checkOutDate = androidApplication.getCheckOutDate();
        setDates(checkInDate, checkOutDate);
    }

    @Override
    public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
        if(isCheckIn) {
            Calendar c = Calendar.getInstance();
            c.set(Calendar.YEAR, i);
            c.set(Calendar.MONTH, i1);
            c.set(Calendar.DAY_OF_MONTH, i2);
            checkInDate = DateFormat.getDateInstance().format(c.getTime());
            if(!compareDates(checkInDate, checkOutDate)){
                SimpleDateFormat format = new SimpleDateFormat("MMM dd, yyyy");
                Date dt = new Date();
                try {
                    dt = format.parse(checkInDate);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                c = Calendar.getInstance();
                c.setTime(dt);
                c.add(Calendar.DATE, 1);
                checkOutDate = DateFormat.getDateInstance().format(c.getTime());
            }
        }else{
            Calendar c = Calendar.getInstance();
            c.set(Calendar.YEAR, i);
            c.set(Calendar.MONTH, i1);
            c.set(Calendar.DAY_OF_MONTH, i2);
            checkOutDate = DateFormat.getDateInstance().format(c.getTime());
        }
        androidApplication.setDates(checkInDate, checkOutDate);
        setDates(checkInDate, checkOutDate);
        recyclerViewRoomList.setVisibility(View.GONE);
        showShimmerAnimation();
        mPresenter.getHotelList(cityId,1, 2, checkInDate, checkOutDate, "refresh");
    }

    public boolean compareDates(String checkIn, String checkOut){
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("MMM dd, yyyy", Locale.US);
            Date checkInDateObj = sdf.parse(checkIn);
            Date checkOutDateObj = sdf.parse(checkOut);
            if(checkInDateObj.before(checkOutDateObj)){
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public void setDates(String checkInDateString, String checkOutDateString){
        txtViewCheckInDate.setText(checkInDateString);
        txtViewCheckOutDate.setText(checkOutDateString);
    }

    @OnClick(R.id.check_in_linear_layout) void onCheckInClicked(){
        isCheckIn = true;
        Bundle args = new Bundle();
        Date dt = Calendar.getInstance().getTime();
        args.putString("minimumDate", DateFormat.getDateInstance().format(dt));
        args.putString("currentDate", checkInDate);
        dialogFragment.setArguments(args);
        dialogFragment.show(getSupportFragmentManager(), "date picker");
    }

    @OnClick(R.id.check_out_linear_layout) void onCheckOutClicked(){
        isCheckIn = false;
        SimpleDateFormat format = new SimpleDateFormat("MMM dd, yyyy");
        Date dt = new Date();
        try {
            dt = format.parse(checkInDate);
        } catch (ParseException e) {
            Log.e("ErrorMsg", e.getMessage());
            e.printStackTrace();
        }
        Calendar c = Calendar.getInstance();
        c.setTime(dt);
        c.add(Calendar.DATE, 1);
        Bundle args = new Bundle();
        args.putString("minimumDate", DateFormat.getDateInstance().format(c.getTime()));
        args.putString("currentDate", checkOutDate);
        dialogFragment.setArguments(args);
        dialogFragment.show(getSupportFragmentManager(), "date picker");
    }

    @OnClick(R.id.txt_view_room_select) void onRoomSelectClicked(){
        initialiseRoomSelectDialog();
        roomSelectDialog.setUpDialogView();
        roomSelectDialog.showDialog();
    }




    @Override
    public void loadAdapter(HotelListAdapter adapter, final int totalPage) {
        this.totalPage = totalPage;
        recyclerViewRoomList.setVisibility(View.VISIBLE);

//        Recycler view animation
        int resId = R.anim.layout_animation_left_to_right;
        LayoutAnimationController animation = AnimationUtils.loadLayoutAnimation(this, resId);
        recyclerViewRoomList.setLayoutAnimation(animation);


        linearLayoutManager = new LinearLayoutManager(this);
        recyclerViewRoomList.setAdapter(adapter);
        recyclerViewRoomList.setLayoutManager(linearLayoutManager);
        recyclerViewRoomList.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if(newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL)
                {
                    isScrolling= true;
                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                currentItems = linearLayoutManager.getChildCount();
                totalItems  = linearLayoutManager.getItemCount();
                scrollOutItems = linearLayoutManager.findFirstCompletelyVisibleItemPosition();
                if(!recyclerView.canScrollVertically(1))
                {
                    isScrolling  = false;
                    if(pageNo < totalPage){
                        pageNo = pageNo + 1;
                        showPaginationProgressBar();
                        mPresenter.getHotelList(cityId, pageNo, 2, checkInDate, checkOutDate,"");
                    }
                }
            }
        });


    }

    @Override
    public void hidePaginationProgressBar() {
        paginationProgressBar.setVisibility(View.GONE);
    }

    @Override
    public void showShimmerAnimation() {
        shimmerFrameLayout.startShimmer();
        shimmerFrameLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideShimmerAnimation() {
        shimmerFrameLayout.stopShimmer();
        shimmerFrameLayout.setVisibility(View.GONE);
    }

    public void showPaginationProgressBar(){
        paginationProgressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransitionExit();
    }

    @Override
    public void startActivity(Intent intent) {
        super.startActivity(intent);
        overridePendingTransitionEnter();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransitionExit();
    }

    protected void overridePendingTransitionExit() {
        overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right);
    }

    protected void overridePendingTransitionEnter() {
        overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
    }

    @Override
    public void goToHotelDetails(int id) {
        Intent intent = new Intent(this, HotelDetailsActivity.class);
        intent.putExtra("hotelId", id);
        startActivity(intent);
    }

    @Override
    public void setFinalRoomStatus(int adultCount, int childCount, int roomCount) {
        finalAdultCount = adultCount;
        finalChildCount = childCount;
        finalRoomCount = roomCount;
        txtViewFinalRoomCount.setText("Room " + Integer.toString(finalRoomCount));
        if(finalChildCount > 0){
            imgViewFinalChild.setVisibility(View.VISIBLE);
            txtViewFinalChild.setVisibility(View.VISIBLE);
            txtViewFinalChild.setText(Integer.toString(finalChildCount));
        }else{
            imgViewFinalChild.setVisibility(View.GONE);
            txtViewFinalChild.setVisibility(View.GONE);
        }
        txtViewFinalAdult.setText(Integer.toString(finalAdultCount));
        if(!cityId.isEmpty()){
            recyclerViewRoomList.setVisibility(View.GONE);
            showShimmerAnimation();
            mPresenter.getHotelList(cityId,1, 2, checkInDate, checkOutDate, "refresh");
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        androidApplication = (AndroidApplication) getApplicationContext();
        ArrayList<Room> roomArrayList = androidApplication.getRooms();
        Log.e("ErrorMsg", "in City List: rooms " + roomArrayList.size());
        finalRoomCount = roomArrayList.size();
        int adult = 0;
        int child = 0;
        for(int i = 0; i < roomArrayList.size(); i++){
            adult = adult + roomArrayList.get(i).adults;
            child = child + roomArrayList.get(i).child;
        }
        finalAdultCount = adult;
        finalChildCount = child;
        setUpDatePicker();
        setFinalRoomStatus(finalAdultCount, finalChildCount, finalRoomCount);
    }



}
