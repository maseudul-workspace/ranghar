package in.net.webinfotech.ranghar.domain.interactor;

import in.net.webinfotech.ranghar.domain.model.Hotel.HotelDetails;

/**
 * Created by Raj on 16-04-2019.
 */

public interface GetHotelDetailsInteractor {
    interface Callback{
        void onGettingHotelDetailsSuccess(HotelDetails hotelDetails);
        void onGettingHotelDetailsFail(String errorMsg);
    }
}
