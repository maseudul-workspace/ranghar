package in.net.webinfotech.ranghar.domain.model.Cities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Raj on 07-03-2019.
 */

public class CitiesWrapper {
    @SerializedName("status")
    @Expose
    public boolean status;

    @SerializedName("message")
    @Expose
    public String message;

    @SerializedName("code")
    @Expose
    public int code;

    @SerializedName("data")
    @Expose
    public City[] cities;

}
