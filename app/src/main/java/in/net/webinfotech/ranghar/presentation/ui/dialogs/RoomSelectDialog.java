package in.net.webinfotech.ranghar.presentation.ui.dialogs;

import android.app.Activity;
import android.content.Context;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;

import java.util.ArrayList;

import in.net.webinfotech.ranghar.AndroidApplication;
import in.net.webinfotech.ranghar.R;
import in.net.webinfotech.ranghar.domain.model.Room.Room;

/**
 * Created by Raj on 11-04-2019.
 */

public class RoomSelectDialog {

    public interface Callback{
        void setFinalRoomStatus(int adultCount, int childCount, int roomCount);
    }

    Callback mCallback;
    Context mContext;
    View dialogContainer;
    View mainSelectCard;
    LinearLayout containerLinearLayout;
    AlertDialog.Builder builder;
    int adultCount = 1;
    int childCount = 0;
    ArrayList<Room> rooms = new ArrayList<>();
    TextView topAdultCount;
    TextView topChildCount;
    ImageView imgTopAdult;
    ImageView imgTopChild;
    int roomCount = 1;
    TextView txtViewAddNewMain;
    ImageView imgViewCancelDialog;
    int finalAdultCount = 1;
    int finalChildCount = 0;
    Activity mActivity;
    AlertDialog dialog;
    int index = 0;
    AndroidApplication androidApplication;
    TextView txtOneAdultMain;
    TextView txtTwoAdultMain;
    TextView txtThreeAdultMain;
    CheckBox checkBoxMain;
    ImageView addChildMain;

    public RoomSelectDialog(Callback mCallback, Context mContext, Activity activity) {
        this.mCallback = mCallback;
        this.mContext = mContext;
        this.mActivity = activity;
    }

    public void setUpDialogView(){
        Room room = new Room(1, 0);
        rooms.add(room);
        dialogContainer = mActivity.getLayoutInflater().inflate(R.layout.room_select_container_layout, null);
        topAdultCount = (TextView) dialogContainer.findViewById(R.id.txt_view_adult_count_top);
        topChildCount = (TextView) dialogContainer.findViewById(R.id.txt_view_child_count_top);
        imgTopAdult = (ImageView) dialogContainer.findViewById(R.id.img_view_adult_top);
        imgTopChild = (ImageView) dialogContainer.findViewById(R.id.img_view_child_top);
        imgViewCancelDialog = (ImageView) dialogContainer.findViewById(R.id.img_view_cancel_dialog);
        Button applyBtn = (Button) dialogContainer.findViewById(R.id.btn_apply_rooms);
        applyBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finalAdultCount = adultCount;
                finalChildCount = childCount;
                androidApplication = (AndroidApplication) mContext.getApplicationContext();
                Log.e("ErrorMsg", "Room array count: " + rooms.size());
                androidApplication.setRooms(rooms);
                mCallback.setFinalRoomStatus(finalAdultCount, finalChildCount, roomCount);
                dialog.dismiss();
            }
        });
        imgViewCancelDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        mainSelectCard = mActivity.getLayoutInflater().inflate(R.layout.choose_room_layout, containerLinearLayout, false);
        txtViewAddNewMain = (TextView) mainSelectCard.findViewById(R.id.txt_view_add_new_main);
        txtOneAdultMain = (TextView) mainSelectCard.findViewById(R.id.txt_view_one_adult);
        txtTwoAdultMain = (TextView) mainSelectCard.findViewById(R.id.txt_view_two_adult);
        txtThreeAdultMain = (TextView) mainSelectCard.findViewById(R.id.txt_view_three_adult);
        final TextView txtRoomStatus = (TextView) mainSelectCard.findViewById(R.id.txt_view_room_status);
        txtRoomStatus.setText("1 adult");


        txtOneAdultMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(rooms.get(0).child + 1 <= 4) {
                    txtOneAdultMain.setBackgroundColor(mContext.getResources().getColor(R.color.appColor));
                    txtOneAdultMain.setTextColor(mContext.getResources().getColor(R.color.white));
                    txtTwoAdultMain.setBackgroundColor(mContext.getResources().getColor(R.color.white));
                    txtTwoAdultMain.setTextColor(mContext.getResources().getColor(R.color.black));
                    txtThreeAdultMain.setBackgroundColor(mContext.getResources().getColor(R.color.white));
                    txtThreeAdultMain.setTextColor(mContext.getResources().getColor(R.color.black));
                    adultCount = adultCount + 1;
                    adultCount = adultCount - rooms.get(0).adults;
                    rooms.get(0).adults = 1;
                    if(rooms.get(0).child == 0){
                        txtRoomStatus.setText(rooms.get(0).adults + " adult");
                    }else{
                        txtRoomStatus.setText(rooms.get(0).adults + " adult," + rooms.get(0).child + " children");
                    }
                    setTopContents();
                }else{
                    showMaximumPersonSnackbar();
                }
            }
        });

        txtTwoAdultMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(rooms.get(0).child + 2 <= 4){
                    txtTwoAdultMain.setBackgroundColor(mContext.getResources().getColor(R.color.appColor));
                    txtTwoAdultMain.setTextColor(mContext.getResources().getColor(R.color.white));
                    txtOneAdultMain.setBackgroundColor(mContext.getResources().getColor(R.color.white));
                    txtOneAdultMain.setTextColor(mContext.getResources().getColor(R.color.black));
                    txtThreeAdultMain.setBackgroundColor(mContext.getResources().getColor(R.color.white));
                    txtThreeAdultMain.setTextColor(mContext.getResources().getColor(R.color.black));
                    adultCount = adultCount + 2;
                    adultCount = adultCount - rooms.get(0).adults;
                    rooms.get(0).adults = 2;
                    if(rooms.get(0).child == 0){
                        txtRoomStatus.setText(rooms.get(0).adults + " adult");
                    }else{
                        txtRoomStatus.setText(rooms.get(0).adults + " adult," + rooms.get(0).child + " children");
                    }
                    setTopContents();
                }else{
                    showMaximumPersonSnackbar();
                }
            }
        });

        txtThreeAdultMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(rooms.get(0).child + 3 <= 4){
                    txtThreeAdultMain.setBackgroundColor(mContext.getResources().getColor(R.color.appColor));
                    txtThreeAdultMain.setTextColor(mContext.getResources().getColor(R.color.white));
                    txtOneAdultMain.setBackgroundColor(mContext.getResources().getColor(R.color.white));
                    txtOneAdultMain.setTextColor(mContext.getResources().getColor(R.color.black));
                    txtTwoAdultMain.setBackgroundColor(mContext.getResources().getColor(R.color.white));
                    txtTwoAdultMain.setTextColor(mContext.getResources().getColor(R.color.black));
                    adultCount = adultCount + 3;
                    adultCount = adultCount - rooms.get(0).adults;
                    rooms.get(0).adults = 3;
                    if(rooms.get(0).child == 0){
                        txtRoomStatus.setText(rooms.get(0).adults + " adult");
                    }else{
                        txtRoomStatus.setText(rooms.get(0).adults + " adult," + rooms.get(0).child + " children");
                    }
                    setTopContents();
                }else{
                    showMaximumPersonSnackbar();
                }

            }
        });

        txtViewAddNewMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addCard(null);
            }
        });

        checkBoxMain = (CheckBox) mainSelectCard.findViewById(R.id.room_select_checkbox);
        final TextView txtChildCount = (TextView) mainSelectCard.findViewById(R.id.txt_view_child_count);
        final ImageView upArrowImageView = (ImageView) mainSelectCard.findViewById(R.id.img_view_arrow_up);
        final ImageView downArrowImageView = (ImageView) mainSelectCard.findViewById(R.id.img_view_arrow_down);
        final LinearLayout childLinearLayout = (LinearLayout) mainSelectCard.findViewById(R.id.room_select_child_linear_layout);
        final LinearLayout mainLinearLayout = (LinearLayout) mainSelectCard.findViewById(R.id.room_select_main_linear_layout);
        upArrowImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                upArrowImageView.setVisibility(View.GONE);
                mainLinearLayout.setVisibility(View.GONE);
                downArrowImageView.setVisibility(View.VISIBLE);
            }
        });
        downArrowImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                upArrowImageView.setVisibility(View.VISIBLE);
                mainLinearLayout.setVisibility(View.VISIBLE);
                downArrowImageView.setVisibility(View.GONE);
            }
        });
        checkBoxMain.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b){
                    childLinearLayout.setVisibility(View.VISIBLE);
                    rooms.get(0).child = 1;
                    childCount = childCount + 1;
                    txtChildCount.setText(Integer.toString(rooms.get(0).child));
                    if(rooms.get(0).child == 0){
                        txtRoomStatus.setText(rooms.get(0).adults + " adult");
                    }else{
                        txtRoomStatus.setText(rooms.get(0).adults + " adult," + rooms.get(0).child + " children");
                    }
                    setTopContents();
                }else{
                    childLinearLayout.setVisibility(View.GONE);
                    childCount = childCount - rooms.get(0).child;
                    rooms.get(0).child = 0;
                    txtChildCount.setText(Integer.toString(rooms.get(0).child));
                    if(rooms.get(0).child == 0){
                        txtRoomStatus.setText(rooms.get(0).adults + " adult");
                    }else{
                        txtRoomStatus.setText(rooms.get(0).adults + " adult," + rooms.get(0).child + " children");
                    }
                    setTopContents();
                }
            }
        });

        ImageView minusChild = (ImageView) mainSelectCard.findViewById(R.id.img_view_minus_child);
        addChildMain = (ImageView) mainSelectCard.findViewById(R.id.img_view_add_child);
        addChildMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.e("ErrorMsg", "total count" + rooms.get(0).child + rooms.get(0).adults);
                if(rooms.get(0).child + rooms.get(0).adults < 4){
                    childCount = childCount + 1;
                    rooms.get(0).child = rooms.get(0).child + 1;
                    txtChildCount.setText(Integer.toString(rooms.get(0).child));
                    if(rooms.get(0).child == 0){
                        txtRoomStatus.setText(rooms.get(0).adults + " adult");
                    }else{
                        txtRoomStatus.setText(rooms.get(0).adults + " adult," + rooms.get(0).child + " children");
                    }
                    setTopContents();
                }else{
                    showMaximumPersonSnackbar();
                }
            }
        });
        minusChild.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.e("ErrorMsg", "child count" + rooms.get(0).child);
                if(rooms.get(0).child > 1){
                    childCount = childCount - 1;
                    rooms.get(0).child = rooms.get(0).child - 1;
                    txtChildCount.setText(Integer.toString(rooms.get(0).child));
                    if(rooms.get(0).child == 0){
                        txtRoomStatus.setText(rooms.get(0).adults + " adult");
                    }else{
                        txtRoomStatus.setText(rooms.get(0).adults + " adult," + rooms.get(0).child + " children");
                    }
                    setTopContents();
                }
            }
        });

        containerLinearLayout = (LinearLayout) dialogContainer.findViewById(R.id.room_container_linear_layout);
        containerLinearLayout.addView(mainSelectCard);
        builder = new AlertDialog.Builder(mContext, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        builder.setView(dialogContainer);
        dialog = builder.create();
    }

    public void addCard(Room setRooms){
        if(roomCount < 4) {
            index++;
            roomCount++;
            Room room = new Room(1, 0);
            rooms.add(room);
            adultCount = adultCount + 1;
            final int localIndex = index;
            final View cardViews = mActivity.getLayoutInflater().inflate(R.layout.choose_room_layout, containerLinearLayout, false);
            Log.e("ErrorMsg", new Gson().toJson(rooms));

            final TextView txtOneAdult = (TextView) cardViews.findViewById(R.id.txt_view_one_adult);
            final TextView txtTwoAdult = (TextView) cardViews.findViewById(R.id.txt_view_two_adult);
            final TextView txtThreeAdult = (TextView) cardViews.findViewById(R.id.txt_view_three_adult);
            final TextView txtRoomStatus = (TextView) cardViews.findViewById(R.id.txt_view_room_status);
            txtRoomStatus.setText("1 adult");

            txtOneAdult.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(rooms.get(localIndex).child + 1 <= 4) {
                        txtOneAdult.setBackgroundColor(mContext.getResources().getColor(R.color.appColor));
                        txtOneAdult.setTextColor(mContext.getResources().getColor(R.color.white));
                        txtTwoAdult.setBackgroundColor(mContext.getResources().getColor(R.color.white));
                        txtTwoAdult.setTextColor(mContext.getResources().getColor(R.color.black));
                        txtThreeAdult.setBackgroundColor(mContext.getResources().getColor(R.color.white));
                        txtThreeAdult.setTextColor(mContext.getResources().getColor(R.color.black));
                        adultCount = adultCount + 1;
                        adultCount = adultCount - rooms.get(localIndex).adults;
                        rooms.get(localIndex).adults = 1;
                        if (rooms.get(localIndex).child == 0) {
                            txtRoomStatus.setText(rooms.get(localIndex).adults + " adult");
                        } else {
                            txtRoomStatus.setText(rooms.get(localIndex).adults + " adult," + rooms.get(localIndex).child + " children");
                        }
                        setTopContents();
                    }else{
                        showMaximumPersonSnackbar();
                    }
                }
            });

            txtTwoAdult.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(rooms.get(localIndex).child + 2 <= 4) {
                        txtTwoAdult.setBackgroundColor(mContext.getResources().getColor(R.color.appColor));
                        txtTwoAdult.setTextColor(mContext.getResources().getColor(R.color.white));
                        txtOneAdult.setBackgroundColor(mContext.getResources().getColor(R.color.white));
                        txtOneAdult.setTextColor(mContext.getResources().getColor(R.color.black));
                        txtThreeAdult.setBackgroundColor(mContext.getResources().getColor(R.color.white));
                        txtThreeAdult.setTextColor(mContext.getResources().getColor(R.color.black));
                        adultCount = adultCount + 2;
                        adultCount = adultCount - rooms.get(localIndex).adults;
                        rooms.get(localIndex).adults = 2;
                        if (rooms.get(localIndex).child == 0) {
                            txtRoomStatus.setText(rooms.get(localIndex).adults + " adult");
                        } else {
                            txtRoomStatus.setText(rooms.get(localIndex).adults + " adult," + rooms.get(localIndex).child + " children");
                        }
                        setTopContents();
                    }else{
                        showMaximumPersonSnackbar();
                    }
                }
            });

            txtThreeAdult.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(rooms.get(localIndex).child + 3 <= 4) {
                        txtThreeAdult.setBackgroundColor(mContext.getResources().getColor(R.color.appColor));
                        txtThreeAdult.setTextColor(mContext.getResources().getColor(R.color.white));
                        txtOneAdult.setBackgroundColor(mContext.getResources().getColor(R.color.white));
                        txtOneAdult.setTextColor(mContext.getResources().getColor(R.color.black));
                        txtTwoAdult.setBackgroundColor(mContext.getResources().getColor(R.color.white));
                        txtTwoAdult.setTextColor(mContext.getResources().getColor(R.color.black));
                        adultCount = adultCount + 3;
                        adultCount = adultCount - rooms.get(localIndex).adults;
                        rooms.get(localIndex).adults = 3;
                        if (rooms.get(localIndex).child == 0) {
                            txtRoomStatus.setText(rooms.get(localIndex).adults + " adult");
                        } else {
                            txtRoomStatus.setText(rooms.get(localIndex).adults + " adult," + rooms.get(localIndex).child + " children");
                        }
                        setTopContents();
                    }else {
                        showMaximumPersonSnackbar();
                    }
                }
            });


            TextView txtAddNewMain = (TextView) cardViews.findViewById(R.id.txt_view_add_new_main);
            txtAddNewMain.setVisibility(View.GONE);
            LinearLayout addDeleteLinearLayout = (LinearLayout) cardViews.findViewById(R.id.add_delete_linear_layout);
            addDeleteLinearLayout.setVisibility(View.VISIBLE);
            TextView addNewSmall = (TextView) cardViews.findViewById(R.id.txt_view_add_new_small);
            TextView deleteSmall = (TextView) cardViews.findViewById(R.id.txt_view_delete_small);
            deleteSmall.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    containerLinearLayout.removeView(cardViews);
                    adultCount = adultCount - rooms.get(localIndex).adults;
                    childCount = childCount - rooms.get(localIndex).child;
                    rooms.get(localIndex).adults = 0;
                    rooms.get(localIndex).child = 0;
                    rooms.remove(localIndex);
                    index--;
                    roomCount--;
                    setTopContents();
                    checkRoomCount();
                }
            });
            addNewSmall.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    addCard(null);
                }
            });

            CheckBox checkBox = (CheckBox) cardViews.findViewById(R.id.room_select_checkbox);
            final ImageView upArrowImageView = (ImageView) cardViews.findViewById(R.id.img_view_arrow_up);
            final ImageView downArrowImageView = (ImageView) cardViews.findViewById(R.id.img_view_arrow_down);
            final LinearLayout childLinearLayout = (LinearLayout) cardViews.findViewById(R.id.room_select_child_linear_layout);
            final LinearLayout mainLinearLayout = (LinearLayout) cardViews.findViewById(R.id.room_select_main_linear_layout);
            final TextView txtChildCount = (TextView) cardViews.findViewById(R.id.txt_view_child_count);
            upArrowImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    upArrowImageView.setVisibility(View.GONE);
                    mainLinearLayout.setVisibility(View.GONE);
                    downArrowImageView.setVisibility(View.VISIBLE);
                }
            });
            downArrowImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    upArrowImageView.setVisibility(View.VISIBLE);
                    mainLinearLayout.setVisibility(View.VISIBLE);
                    downArrowImageView.setVisibility(View.GONE);
                }
            });
            checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    if (b) {
                        childLinearLayout.setVisibility(View.VISIBLE);
                        rooms.get(localIndex).child = 1;
                        childCount = childCount + 1;
                        txtChildCount.setText(Integer.toString(rooms.get(localIndex).child));
                        if (rooms.get(localIndex).child == 0) {
                            txtRoomStatus.setText(rooms.get(localIndex).adults + " adult");
                        } else {
                            txtRoomStatus.setText(rooms.get(localIndex).adults + " adult," + rooms.get(localIndex).child + " children");
                        }
                        setTopContents();
                    } else {
                        childLinearLayout.setVisibility(View.GONE);
                        childCount = childCount - rooms.get(localIndex).child;
                        rooms.get(localIndex).child = 0;
                        txtChildCount.setText(Integer.toString(rooms.get(localIndex).child));
                        if (rooms.get(localIndex).child == 0) {
                            txtRoomStatus.setText(rooms.get(localIndex).adults + " adult");
                        } else {
                            txtRoomStatus.setText(rooms.get(localIndex).adults + " adult," + rooms.get(localIndex).child + " children");
                        }
                        setTopContents();
                    }
                }
            });
            ImageView minusChild = (ImageView) cardViews.findViewById(R.id.img_view_minus_child);
            ImageView addChild = (ImageView) cardViews.findViewById(R.id.img_view_add_child);
            addChild.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (rooms.get(localIndex).child + rooms.get(localIndex).adults < 4) {
                        childCount = childCount + 1;
                        rooms.get(localIndex).child = rooms.get(localIndex).child + 1;
                        txtChildCount.setText(Integer.toString(rooms.get(localIndex).child));
                        if (rooms.get(localIndex).child == 0) {
                            txtRoomStatus.setText(rooms.get(localIndex).adults + " adult");
                        } else {
                            txtRoomStatus.setText(rooms.get(localIndex).adults + " adult," + rooms.get(localIndex).child + " children");
                        }
                        setTopContents();
                    }else{
                        showMaximumPersonSnackbar();
                    }
                }
            });
            minusChild.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (rooms.get(localIndex).child > 1) {
                        childCount = childCount - 1;
                        rooms.get(localIndex).child = rooms.get(localIndex).child - 1;
                        txtChildCount.setText(Integer.toString(rooms.get(localIndex).child));
                        if (rooms.get(localIndex).child == 0) {
                            txtRoomStatus.setText(rooms.get(localIndex).adults + " adult");
                        } else {
                            txtRoomStatus.setText(rooms.get(localIndex).adults + " adult," + rooms.get(localIndex).child + " children");
                        }
                        setTopContents();
                    }
                }
            });

            cardViews.setFocusableInTouchMode(true);
            cardViews.requestFocus();
            containerLinearLayout.addView(cardViews);

            if(setRooms != null){
                switch (setRooms.adults){
                    case 1:
                        txtOneAdult.performClick();
                        break;
                    case 2:
                        txtTwoAdult.performClick();
                        break;
                    case 3:
                        txtThreeAdult.performClick();
                        break;
                }

                if(setRooms.child > 0){
                    checkBox.setChecked(true);
                    for(int i = 1; i < setRooms.child; i++){
                        addChild.performClick();
                    }
                }
            }

            setTopContents();
            checkRoomCount();

        }else{
            showMaximumRoomSnackbar();
        }
    }

    public void setTopContents(){
        if(childCount > 0){
            imgTopChild.setVisibility(View.VISIBLE);
            topChildCount.setVisibility(View.VISIBLE);
            topChildCount.setText(Integer.toString(childCount));
        }else{
            imgTopChild.setVisibility(View.GONE);
            topChildCount.setVisibility(View.GONE);
        }
        topAdultCount.setText(Integer.toString(adultCount));
    }

    public void checkRoomCount(){
        if(roomCount > 1){
            txtViewAddNewMain.setVisibility(View.GONE);
        }else{
            txtViewAddNewMain.setVisibility(View.VISIBLE);
        }
    }

    public void showMaximumPersonSnackbar(){
        Log.e("ErrorMsg", "showing snack bar");
        Snackbar snackbar = Snackbar.make(dialogContainer, "This room cannot accommodate more people. Please add another room", Snackbar.LENGTH_SHORT);
        snackbar.show();
    }

    public void showMaximumRoomSnackbar(){
        Snackbar snackbar = Snackbar.make(dialogContainer, "You cannot add more than 4 room", Snackbar.LENGTH_SHORT);
        snackbar.show();
    }

    public void showDialog(){
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        ArrayList<Room> roomArrayList = androidApplication.getRooms();
        Log.e("ErrorMsg", "in dialog: rooms " + roomArrayList.size());
        for(int i = 0; i < roomArrayList.size(); i++){
            if(i == 0){
                switch (roomArrayList.get(i).adults){
                    case 1:
                        txtOneAdultMain.performClick();
                        break;
                    case 2:
                        txtTwoAdultMain.performClick();
                        break;
                    case 3:
                        txtThreeAdultMain.performClick();
                        break;
                }

                if(roomArrayList.get(i).child > 0){
                    checkBoxMain.setChecked(true);
                    for(int j = 1; j < roomArrayList.get(i).child; j++){
                        addChildMain.performClick();
                    }
                }
            }else{
                addCard(roomArrayList.get(i));
            }
        }

        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        dialog.show();
    }

//    public void setFinalRoomStatus(int i){
//        txtViewFinalRoomCount.setText("Room " + Integer.toString(roomCount));
//        if(finalChildCount > 0){
//            imgViewFinalChild.setVisibility(View.VISIBLE);
//            txtViewFinalChild.setVisibility(View.VISIBLE);
//            txtViewFinalChild.setText(Integer.toString(finalChildCount));
//        }else{
//            imgViewFinalChild.setVisibility(View.GONE);
//            txtViewFinalChild.setVisibility(View.GONE);
//        }
//        txtViewFinalAdult.setText(Integer.toString(finalAdultCount));
//        if(!searchKey.isEmpty()){
//            showProgressBar();
//            mPresenter.getHotelList(searchKey,1, roomCount, finalAdultCount, 1, "refresh");
//        }
//    }

}
