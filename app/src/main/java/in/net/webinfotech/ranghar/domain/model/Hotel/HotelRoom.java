package in.net.webinfotech.ranghar.domain.model.Hotel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Raj on 12-04-2019.
 */

public class HotelRoom {
    @SerializedName("room_id")
    @Expose
    public int room_id;

    @SerializedName("room_size")
    @Expose
    public int room_size;

    @SerializedName("room_price")
    @Expose
    public int room_price;

    @SerializedName("total_room")
    @Expose
    public int total_room;

    @SerializedName("is_food")
    @Expose
    public int is_food;

}
