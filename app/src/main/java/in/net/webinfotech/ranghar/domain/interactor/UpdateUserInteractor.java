package in.net.webinfotech.ranghar.domain.interactor;

/**
 * Created by Raj on 25-03-2019.
 */

public interface UpdateUserInteractor {
    interface Callback{
        void onUserUpdateSuccess(String successMsg);
        void onUserUpdateFail(String errorMsg);
    }
}
