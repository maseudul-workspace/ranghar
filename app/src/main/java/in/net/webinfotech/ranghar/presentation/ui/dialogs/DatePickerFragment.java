package in.net.webinfotech.ranghar.presentation.ui.dialogs;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.util.Log;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by Raj on 08-03-2019.
 */

public class DatePickerFragment extends DialogFragment {

    DatePickerDialog datePickerDialog;
    String minimumDate;
    String currentDate;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle mArgs = getArguments();
        minimumDate = mArgs.getString("minimumDate");
        currentDate = mArgs.getString("currentDate");
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        SimpleDateFormat format = new SimpleDateFormat("MMM dd, yyyy");
        Date dt = new Date();
        Calendar c = Calendar.getInstance();
        try {
            dt = format.parse(currentDate);
        } catch (ParseException e) {
            Log.e("ErrorMsg", e.getMessage());
            e.printStackTrace();
        }
        c.setTime(dt);
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);
        datePickerDialog = new DatePickerDialog(getActivity(), (DatePickerDialog.OnDateSetListener) getActivity(), year, month, day);
        try {
            dt = format.parse(minimumDate);
        } catch (ParseException e) {
            Log.e("ErrorMsg", e.getMessage());
            e.printStackTrace();
        }
        c.setTime(dt);
        datePickerDialog.getDatePicker().setMinDate(c.getTimeInMillis());
        return datePickerDialog;
    }

    public void setMinimumDate(){
        Date dt = new Date();
        Calendar c = Calendar.getInstance();
        c.setTime(dt);
        c.add(Calendar.DATE, 1);
        datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
    }
}
