package in.net.webinfotech.ranghar.presentation.presenters;

import in.net.webinfotech.ranghar.domain.model.User.UserInfo;
import in.net.webinfotech.ranghar.presentation.presenters.base.BasePresenter;

/**
 * Created by Raj on 21-03-2019.
 */

public interface UserDetailsPresenter extends BasePresenter {
    void fetchUserDetails();
    void updateUser( String api_key,
                     int userId,
                     String name,
                     String mobile,
                     String email,
                     int state,
                     int city,
                     int location,
                     String pin);
    interface View{
        void showProgressBar();
        void hideProgressBar();
        void loadUserDetails(UserInfo userInfo);
    }
}
