package in.net.webinfotech.ranghar.domain.interactor.impl;

import android.content.Context;

import in.net.webinfotech.ranghar.domain.executor.Executor;
import in.net.webinfotech.ranghar.domain.executor.MainThread;
import in.net.webinfotech.ranghar.domain.interactor.CreateUserInteractor;
import in.net.webinfotech.ranghar.domain.interactor.base.AbstractInteractor;
import in.net.webinfotech.ranghar.domain.model.User.UserInfo;
import in.net.webinfotech.ranghar.domain.model.User.UserInfoWrapper;
import in.net.webinfotech.ranghar.repository.impl.UserRepositoryImpl;

/**
 * Created by Raj on 21-03-2019.
 */

public class CreateUserInteractorImpl extends AbstractInteractor implements CreateUserInteractor {

    UserRepositoryImpl mRepository;
    Callback mCallback;
    String name;
    String mobile;
    String email;
    String password;

    public CreateUserInteractorImpl(Executor threadExecutor,
                                    MainThread mainThread,
                                    Callback callback,
                                    UserRepositoryImpl userRepository,
                                    String name,
                                    String mobile,
                                    String email,
                                    String password
                                    ) {
        super(threadExecutor, mainThread);
        this.name = name;
        this.mobile = mobile;
        this.email = email;
        this.password = password;
        this.mRepository = userRepository;
        this.mCallback = callback;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onUserCreateFail(errorMsg);
            }
        });
    }

    private void postMessage(final UserInfo userInfo){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onUserCreateSuccess(userInfo);
            }
        });
    }

    @Override
    public void run() {
        final UserInfoWrapper userInfoWrapper = mRepository.createUser(name, mobile, email, password);
        if(userInfoWrapper == null){
            notifyError("Something went wrong");
        }else if(!userInfoWrapper.status){
            notifyError(userInfoWrapper.message);
        }else{
            postMessage(userInfoWrapper.userInfo);
        }
    }
}
