package in.net.webinfotech.ranghar.presentation.ui.activities;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;

import com.facebook.shimmer.ShimmerFrameLayout;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.net.webinfotech.ranghar.AndroidApplication;
import in.net.webinfotech.ranghar.R;
import in.net.webinfotech.ranghar.domain.executor.impl.ThreadExecutor;
import in.net.webinfotech.ranghar.presentation.presenters.WishListPresenter;
import in.net.webinfotech.ranghar.presentation.presenters.impl.WishListPresenterImpl;
import in.net.webinfotech.ranghar.presentation.routers.WishListRouter;
import in.net.webinfotech.ranghar.presentation.ui.adapters.WishListAdapter;
import in.net.webinfotech.ranghar.threading.MainThreadImpl;

public class WishListActivity extends AppCompatActivity implements WishListPresenter.View, WishListRouter {

    @BindView(R.id.recycler_view_wishlist)
    RecyclerView recyclerView;
    WishListPresenterImpl mPresenter;
    @BindView(R.id.bottom_navigation)
    BottomNavigationView bottomNavigationView;
    @BindView(R.id.progressBar_layout)
    RelativeLayout progressBarLayout;
    @BindView(R.id.shimmer_layout)
    ShimmerFrameLayout shimmerFrameLayout;
    AndroidApplication androidApplication;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wish_list);
        ButterKnife.bind(this);
        androidApplication = (AndroidApplication) getApplicationContext();
        initialisePresenter();
        mPresenter.getWishList();
        showShimmerAnimation();
        initialiseBottomNavigation();
    }

    public void initialisePresenter(){
        mPresenter = new WishListPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this, this);
    }

    public void initialiseBottomNavigation(){
        Menu menu = bottomNavigationView.getMenu();
        MenuItem menuItem = menu.getItem(1);
        menuItem.setChecked(true);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()){
                    case R.id.action_home:
                        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                        startActivity(intent);
                        break;
                    case R.id.action_account:
                        if(androidApplication.getUserInfo(getApplicationContext()) != null){
                            Log.e("ErrorMsg", "user info found");
                            Intent profileIntent = new Intent(getApplicationContext(), UserProfileActivity.class);
                            startActivity(profileIntent);
                        }else {
                            Log.e("ErrorMsg", "user info not found");
                            showLogInSnackbar();
                        }
                        break;
                    case R.id.action_booking:
//                        Intent intent1 = new Intent(getApplicationContext(), AboutActivity.class);
//                        startActivity(intent1);
                        break;
                }
                return false;
            }
        });
    }

    @Override
    public void loadAdapter(WishListAdapter adapter) {
        recyclerView.setVisibility(View.VISIBLE);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void showShimmerAnimation() {
        shimmerFrameLayout.startShimmer();
        shimmerFrameLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideShimmerAnimation() {
        shimmerFrameLayout.stopShimmer();
        shimmerFrameLayout.setVisibility(View.GONE);
    }

    public void showLogInSnackbar(){
        Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content),"You must be logged in",Snackbar.LENGTH_LONG);
        snackbar.setAction("Log In", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent cartIntent = new Intent(getApplicationContext(), LoginActivity.class);
                startActivity(cartIntent);
            }
        });
        snackbar.show();
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransitionExit();
    }

    @Override
    public void startActivity(Intent intent) {
        super.startActivity(intent);
        overridePendingTransitionEnter();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransitionExit();
    }

    protected void overridePendingTransitionExit() {
        overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right);
    }

    protected void overridePendingTransitionEnter() {
        overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
    }

    @Override
    public void goToHotelDetails(int id) {
        Intent intent = new Intent(this, HotelDetailsActivity.class);
        intent.putExtra("hotelId", id);
        startActivity(intent);
    }
}
