package in.net.webinfotech.ranghar.threading;

import android.os.Handler;
import android.os.Looper;

import in.net.webinfotech.ranghar.domain.executor.MainThread;

/**
 * Created by Raj on 07-03-2019.
 */

public class MainThreadImpl implements MainThread {
    private static MainThread sMainThread;

    private Handler mHandler;

    private MainThreadImpl() {
        mHandler = new Handler(Looper.getMainLooper());
    }

    @Override
    public void post(Runnable runnable) {
        mHandler.post(runnable);
    }

    public static MainThread getInstance() {
        if (sMainThread == null) {
            sMainThread = new MainThreadImpl();
        }

        return sMainThread;
    }
}
