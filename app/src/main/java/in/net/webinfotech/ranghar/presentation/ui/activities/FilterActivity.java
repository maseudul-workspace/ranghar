package in.net.webinfotech.ranghar.presentation.ui.activities;

import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.net.webinfotech.ranghar.R;
import in.net.webinfotech.ranghar.presentation.ui.adapters.FilterPageAdapter;
import in.net.webinfotech.ranghar.presentation.ui.fragments.CheckInDateFragment;
import in.net.webinfotech.ranghar.presentation.ui.fragments.CheckOutFragment;
import in.net.webinfotech.ranghar.presentation.ui.fragments.RoomFilterFragment;

public class FilterActivity extends AppCompatActivity {

    @BindView(R.id.viewpager_filter_activity)
    ViewPager viewPager;
    @BindView(R.id.tablayout_filter_activity)
    TabLayout tabLayout;
    FilterPageAdapter pageAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter);
        ButterKnife.bind(this);
        setupViewPager();
        tabLayout.setupWithViewPager(viewPager);
    }

    private void setupViewPager(){
        pageAdapter = new FilterPageAdapter(getSupportFragmentManager());
        pageAdapter.addFragment(new CheckInDateFragment(), "Check In");
        pageAdapter.addFragment(new CheckOutFragment(), "Check Out");
        pageAdapter.addFragment(new RoomFilterFragment(), "Room");
        viewPager.setAdapter(pageAdapter);
    }
}
