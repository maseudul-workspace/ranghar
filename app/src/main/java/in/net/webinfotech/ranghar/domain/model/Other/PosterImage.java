package in.net.webinfotech.ranghar.domain.model.Other;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Raj on 15-03-2019.
 */

public class PosterImage {

    @SerializedName("id")
    @Expose
    public int id;

    @SerializedName("caption")
    @Expose
    public String caption;

    @SerializedName("image")
    @Expose
    public String image;

}
