package in.net.webinfotech.ranghar.presentation.routers;

/**
 * Created by Raj on 22-04-2019.
 */

public interface WishListRouter {
    void goToHotelDetails(int id);
}
