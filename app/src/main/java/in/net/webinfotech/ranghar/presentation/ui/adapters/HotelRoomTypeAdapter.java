package in.net.webinfotech.ranghar.presentation.ui.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.net.webinfotech.ranghar.R;
import in.net.webinfotech.ranghar.domain.model.Other.HotelRooms;
import in.net.webinfotech.ranghar.util.GlideHelper;

/**
 * Created by Raj on 18-03-2019.
 */

public class HotelRoomTypeAdapter extends RecyclerView.Adapter<HotelRoomTypeAdapter.ViewHolder>{

    Context mContext;
    ArrayList<HotelRooms> hotelRoomsArrayList;

    public HotelRoomTypeAdapter(Context mContext, ArrayList<HotelRooms> hotelRoomsArrayList) {
        this.mContext = mContext;
        this.hotelRoomsArrayList = hotelRoomsArrayList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_hotel_rooms, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        GlideHelper.setImageViewWithDrawable(mContext, holder.imgViewPoster, hotelRoomsArrayList.get(position).drawableId);
        switch (hotelRoomsArrayList.get(position).roomType){
            case 1:
                holder.txtViewRoomType.setText("Single room");
                break;
            case 2:
                holder.txtViewRoomType.setText("Double room");
                break;
            case 3:
                holder.txtViewRoomType.setText("Triple room");
                break;
        }
    }

    @Override
    public int getItemCount() {
        return hotelRoomsArrayList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{
        @BindView(R.id.img_view_hotel_room_type_poster)
        ImageView imgViewPoster;
        @BindView(R.id.txt_view_room_type)
        TextView txtViewRoomType;
        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
