package in.net.webinfotech.ranghar.domain.interactor.impl;

import android.util.Log;

import in.net.webinfotech.ranghar.domain.executor.Executor;
import in.net.webinfotech.ranghar.domain.executor.MainThread;
import in.net.webinfotech.ranghar.domain.interactor.GetHotelListInteractor;
import in.net.webinfotech.ranghar.domain.interactor.base.AbstractInteractor;
import in.net.webinfotech.ranghar.domain.model.Hotel.HotelList;
import in.net.webinfotech.ranghar.domain.model.Hotel.HotelListWrapper;
import in.net.webinfotech.ranghar.repository.impl.HotelRepositoryImpl;

/**
 * Created by Raj on 15-03-2019.
 */

public class GetHotelListInteractorImpl extends AbstractInteractor implements GetHotelListInteractor {

    Callback mCallback;
    HotelRepositoryImpl mRepository;
    String searchKey;
    int pageNo;
    int searchType;
    String checkInDate;
    String checkOutDate;

    public GetHotelListInteractorImpl(Executor threadExecutor,
                                      MainThread mainThread,
                                      Callback callback,
                                      HotelRepositoryImpl repository,
                                      String searchKey,
                                      int pageNo,
                                      int searchType,
                                      String checkInDate,
                                      String checkOutDate
                                      ) {
        super(threadExecutor, mainThread);
        this.mCallback = callback;
        this.mRepository = repository;
        this.searchKey = searchKey;
        this.pageNo = pageNo;
        this.searchType = searchType;
        this.checkInDate = checkInDate;
        this.checkOutDate = checkOutDate;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingHotelListFail(errorMsg);
            }
        });
    }

    private void postMessage(final HotelList[] hotelLists, final String searchKey, final int totalPage){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingHotelListSuccess(hotelLists, searchKey, totalPage);
            }
        });
    }

    @Override
    public void run() {
        final HotelListWrapper hotelListWrapper = mRepository.getHotelList(searchKey, pageNo, searchType, checkInDate, checkOutDate);
        if(hotelListWrapper == null){
            notifyError("Something went wrong");
        }else if(!hotelListWrapper.status){
            notifyError(hotelListWrapper.message);
        }else{
            postMessage(hotelListWrapper.data, hotelListWrapper.search_key, hotelListWrapper.total_page);
        }
    }
}
