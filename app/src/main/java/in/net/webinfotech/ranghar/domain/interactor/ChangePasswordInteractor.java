package in.net.webinfotech.ranghar.domain.interactor;

/**
 * Created by Raj on 25-03-2019.
 */

public interface ChangePasswordInteractor {
    interface Callback{
        void onChangePasswordSuccess();
        void onChangePasswordFail(String errorMsg);
    }
}
