package in.net.webinfotech.ranghar.presentation.presenters;

import in.net.webinfotech.ranghar.presentation.presenters.base.BasePresenter;

/**
 * Created by Raj on 20-05-2019.
 */

public interface PaymentResponsePresenter extends BasePresenter {
    void paymentStatusUpdate(int bookingId, String orderId, String paymentId);
}
