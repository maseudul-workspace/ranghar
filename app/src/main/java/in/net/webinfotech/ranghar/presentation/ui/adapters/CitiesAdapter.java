package in.net.webinfotech.ranghar.presentation.ui.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.net.webinfotech.ranghar.R;
import in.net.webinfotech.ranghar.domain.model.Cities.City;
import in.net.webinfotech.ranghar.util.GlideHelper;

/**
 * Created by Raj on 07-03-2019.
 */

public class CitiesAdapter extends RecyclerView.Adapter<CitiesAdapter.ViewHolder> {

    public interface Callback{
        void onCityClicked(int position, String cityName, int cityId);
    }

    Context mContext;
    ArrayList<City> cityArrayList;
    Callback mCallback ;

    public CitiesAdapter(Context mContext, ArrayList<City> cities, Callback callback) {
        this.mContext = mContext;
        this.cityArrayList = cities;
        mCallback = callback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_cities_layout, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {

        if(position == 0){
            GlideHelper.setImageViewCustomRoundedCornersDrawable(mContext, holder.imgViewCityPoster, R.drawable.locatin_radar, 200);
            holder.txtViewCityName.setText("Nearby");
        }else if(position == cityArrayList.size() - 1){

        }else{
            GlideHelper.setImageViewCustomRoundedCorners(mContext, holder.imgViewCityPoster, mContext.getResources().getString(R.string.base_url) + "uploads/city_image/" + cityArrayList.get(position).image, 200);
            holder.txtViewCityName.setText(cityArrayList.get(position).city);
        }
        holder.imgViewCityPoster.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.onCityClicked(position, cityArrayList.get(position).city, cityArrayList.get(position).id);
            }
        });
    }

    @Override
    public int getItemCount() {
        return cityArrayList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.recycler_view_places_city_poster)
        ImageView imgViewCityPoster;
        @BindView(R.id.recycler_view_places_city_name)
        TextView txtViewCityName;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
