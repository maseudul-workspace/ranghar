package in.net.webinfotech.ranghar.repository.impl;

import android.util.Log;

import com.google.gson.Gson;

import in.net.webinfotech.ranghar.domain.model.Other.PosterImageWrapper;
import in.net.webinfotech.ranghar.domain.model.User.ChangePasswordResponse;
import in.net.webinfotech.ranghar.domain.model.User.UserInfoWrapper;
import in.net.webinfotech.ranghar.domain.model.User.UserUpdateRespone;
import in.net.webinfotech.ranghar.repository.APIclient;
import in.net.webinfotech.ranghar.repository.UserRepository;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.http.Field;

/**
 * Created by Raj on 21-03-2019.
 */

public class UserRepositoryImpl {
    UserRepository mRepository;

    public UserRepositoryImpl() {
        mRepository = APIclient.createService(UserRepository.class);
    }

    public UserInfoWrapper checkLogin(String email, String password){
        UserInfoWrapper userInfoWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> login = mRepository.checkLogin(email, password);

            Response<ResponseBody> response = login.execute();
            if(response.body() != null){
                responseBody = response.body().string();
                Log.e("ErrorMsg", responseBody);
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    userInfoWrapper = null;
                }else{
                    userInfoWrapper = gson.fromJson(responseBody, UserInfoWrapper.class);
                }
            } else {
                userInfoWrapper = null;
            }
        }catch (Exception e){
            Log.e("ErrorMsg", e.getMessage());
            userInfoWrapper = null;
        }
        return userInfoWrapper;
    }

    public UserInfoWrapper createUser(String name, String mobile, String email, String password){
        UserInfoWrapper userInfoWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> create = mRepository.createUser(name, mobile, email, password);

            Response<ResponseBody> response = create.execute();
            if(response.body() != null){
                responseBody = response.body().string();
                Log.e("ErrorMsg", responseBody);
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    userInfoWrapper = null;
                }else{
                    userInfoWrapper = gson.fromJson(responseBody, UserInfoWrapper.class);
                }
            } else {
                userInfoWrapper = null;
            }
        }catch (Exception e){
            Log.e("ErrorMsg", e.getMessage());
            userInfoWrapper = null;
        }
        return userInfoWrapper;
    }

    public UserInfoWrapper fetchUserDetails(String apiKey, int userId){
        UserInfoWrapper userInfoWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> fetch = mRepository.fetchUserDetails(apiKey, userId);

            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
                Log.e("ErrorMsg", responseBody);
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    userInfoWrapper = null;
                }else{
                    userInfoWrapper = gson.fromJson(responseBody, UserInfoWrapper.class);
                }
            } else {
                userInfoWrapper = null;
            }
        }catch (Exception e){
            Log.e("ErrorMsg", e.getMessage());
            userInfoWrapper = null;
        }
        return userInfoWrapper;
    }

    public UserUpdateRespone updateUser(String api_key,
                                        int userId, String name, String mobile,
                                        String email, int state, int city, int location, String pin){
        UserUpdateRespone userUpdateRespone;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> update = mRepository.updateUser(api_key, userId, name, mobile, email, state, city, location, pin);

            Response<ResponseBody> response = update.execute();
            if(response.body() != null){
                responseBody = response.body().string();
                Log.e("ErrorMsg", responseBody);
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    userUpdateRespone = null;
                }else{
                    userUpdateRespone = gson.fromJson(responseBody, UserUpdateRespone.class);
                }
            } else {
                userUpdateRespone = null;
            }
        }catch (Exception e){
            Log.e("ErrorMsg", e.getMessage());
            userUpdateRespone = null;
        }
        return userUpdateRespone;
    }

    public ChangePasswordResponse changePassword(String api_key, int userId, String currentPswd, String newPswd){
        ChangePasswordResponse changePasswordResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> change = mRepository.changePassword(api_key, userId, currentPswd, newPswd);

            Response<ResponseBody> response = change.execute();
            if(response.body() != null){
                responseBody = response.body().string();
                Log.e("ErrorMsg", responseBody);
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    changePasswordResponse = null;
                }else{
                    changePasswordResponse = gson.fromJson(responseBody, ChangePasswordResponse.class);
                }
            } else {
                changePasswordResponse = null;
            }
        }catch (Exception e){
            Log.e("ErrorMsg", e.getMessage());
            changePasswordResponse = null;
        }
        return changePasswordResponse;
    }

}
