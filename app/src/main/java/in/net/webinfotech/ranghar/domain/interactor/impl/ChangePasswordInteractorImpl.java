package in.net.webinfotech.ranghar.domain.interactor.impl;

import in.net.webinfotech.ranghar.domain.executor.Executor;
import in.net.webinfotech.ranghar.domain.executor.MainThread;
import in.net.webinfotech.ranghar.domain.interactor.ChangePasswordInteractor;
import in.net.webinfotech.ranghar.domain.interactor.base.AbstractInteractor;
import in.net.webinfotech.ranghar.domain.model.User.ChangePasswordResponse;
import in.net.webinfotech.ranghar.domain.model.User.UserInfo;
import in.net.webinfotech.ranghar.repository.UserRepository;
import in.net.webinfotech.ranghar.repository.impl.UserRepositoryImpl;

/**
 * Created by Raj on 25-03-2019.
 */

public class ChangePasswordInteractorImpl extends AbstractInteractor implements ChangePasswordInteractor {

    Callback mCallback;
    UserRepositoryImpl mRepository;
    String apiKey;
    int userId;
    String currentPswd;
    String newPswd;

    public ChangePasswordInteractorImpl(Executor threadExecutor,
                                        MainThread mainThread,
                                        Callback callback,
                                        UserRepositoryImpl repository,
                                        String apiKey,
                                        int userId,
                                        String currentPswd,
                                        String newPswd
                                        ) {
        super(threadExecutor, mainThread);
        this.mCallback = callback;
        this.mRepository = repository;
        this.apiKey = apiKey;
        this.userId = userId;
        this.currentPswd = currentPswd;
        this.newPswd = newPswd;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onChangePasswordFail(errorMsg);
            }
        });
    }

    private void postMessage(){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onChangePasswordSuccess();
            }
        });
    }

    @Override
    public void run() {
        final ChangePasswordResponse changePasswordResponse = mRepository.changePassword(apiKey, userId, currentPswd, newPswd);
        if(changePasswordResponse == null){
            notifyError("Something went wrong");
        }else if(!changePasswordResponse.status){
            notifyError(changePasswordResponse.message);
        }else{
            postMessage();
        }
    }
}
