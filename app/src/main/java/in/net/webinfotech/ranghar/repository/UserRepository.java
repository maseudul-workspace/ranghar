package in.net.webinfotech.ranghar.repository;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by Raj on 21-03-2019.
 */

public interface UserRepository {
    @POST("api/user/user_login_check.php")
    @FormUrlEncoded
    Call<ResponseBody> checkLogin(@Field("email") String email,
                                    @Field("password") String password
    );

    @POST("api/user/user_create.php")
    @FormUrlEncoded
    Call<ResponseBody> createUser(@Field("name") String name,
                                  @Field("mobile") String mobile,
                                  @Field("email") String email,
                                  @Field("password") String password

    );

    @POST("api/user/user_profile_fetch.php")
    @FormUrlEncoded
    Call<ResponseBody> fetchUserDetails(@Field("api_key") String api_key,
                                  @Field("user_id") int userId
    );

    @POST("api/user/user_profile_update.php")
    @FormUrlEncoded
    Call<ResponseBody> updateUser(@Field("api_key") String api_key,
                                  @Field("user_id") int userId,
                                  @Field("name") String name,
                                  @Field("mobile") String mobile,
                                  @Field("email") String email,
                                  @Field("state") int state,
                                  @Field("city") int city,
                                  @Field("location") int location,
                                  @Field("pin") String pin
    );

    @POST("api/user/user_password_change.php")
    @FormUrlEncoded
    Call<ResponseBody> changePassword(@Field("api_key") String api_key,
                                        @Field("user_id") int userId,
                                      @Field("current_password") String currentPassword,
                                      @Field("new_password") String newPassword
    );

}
