package in.net.webinfotech.ranghar.presentation.ui.dialogs;

import android.app.Activity;
import android.content.Context;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import in.net.webinfotech.ranghar.R;

/**
 * Created by Raj on 15-05-2019.
 */

public class BookingConfirmationDialog {

    public interface Callback{
        void requestRoom();
    }

    AlertDialog.Builder builder;
    Context mContext;
    Activity mActivity;
    AlertDialog dialog;
    View dialogContainer;
    Callback mCallback;

    public BookingConfirmationDialog(Context mContext, Activity mActivity, Callback callback) {
        this.mContext = mContext;
        this.mActivity = mActivity;
        this.mCallback = callback;
    }

    public void showDialog(String hotelName, Double price, String personStatus, String roomStatus, String duration){
        dialogContainer = mActivity.getLayoutInflater().inflate(R.layout.booking_confirm_layout, null);

        TextView txtViewHotelName = (TextView) dialogContainer.findViewById(R.id.txt_view_hotel_name);
        ImageView imgViewCancel = (ImageView) dialogContainer.findViewById(R.id.img_view_cancel_dialog);
        TextView txtViewPrice = (TextView) dialogContainer.findViewById(R.id.txt_view_total_price);
        TextView txtViewRoomStatus = (TextView) dialogContainer.findViewById(R.id.txt_view_room_status);
        TextView txtViewPersonStatus = (TextView) dialogContainer.findViewById(R.id.txt_view_person_status);
        Button btnRequestRoom = (Button) dialogContainer.findViewById(R.id.btn_request_room);
        Button btnCancelDialog = (Button) dialogContainer.findViewById(R.id.btn_cancel_dialog);
        TextView txtViewDateDuration = (TextView) dialogContainer.findViewById(R.id.txt_view_booking_date_duration);

        txtViewHotelName.setText(hotelName);
        txtViewPrice.setText(Double.toString(price));
        txtViewRoomStatus.setText(roomStatus);
        txtViewPersonStatus.setText(personStatus);
        txtViewDateDuration.setText(duration);

        imgViewCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        btnCancelDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        btnRequestRoom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                mCallback.requestRoom();
            }
        });

        builder = new AlertDialog.Builder(mContext);
        builder.setView(dialogContainer);
        dialog = builder.create();
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        dialog.show();
    }

}
