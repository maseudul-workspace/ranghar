package in.net.webinfotech.ranghar.domain.interactor;

/**
 * Created by Raj on 20-04-2019.
 */

public interface AddToWishListInteractor {
    interface Callback{
        void onAddToWishListSuccess();
        void onAddToWishListFail(String errorMsg);
    }
}
