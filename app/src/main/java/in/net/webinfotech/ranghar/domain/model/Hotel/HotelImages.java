package in.net.webinfotech.ranghar.domain.model.Hotel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Raj on 12-04-2019.
 */

public class HotelImages {
    @SerializedName("image")
    @Expose
    public String image;

    @SerializedName("caption")
    @Expose
    public String caption;

}
