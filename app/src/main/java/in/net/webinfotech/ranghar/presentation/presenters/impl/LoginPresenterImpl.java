package in.net.webinfotech.ranghar.presentation.presenters.impl;

import android.content.Context;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import in.net.webinfotech.ranghar.AndroidApplication;
import in.net.webinfotech.ranghar.R;
import in.net.webinfotech.ranghar.domain.executor.Executor;
import in.net.webinfotech.ranghar.domain.executor.MainThread;
import in.net.webinfotech.ranghar.domain.interactor.CheckLoginInteractor;
import in.net.webinfotech.ranghar.domain.interactor.impl.CheckLoginInteractorImpl;
import in.net.webinfotech.ranghar.domain.model.User.UserInfo;
import in.net.webinfotech.ranghar.presentation.presenters.LoginPresenter;
import in.net.webinfotech.ranghar.presentation.presenters.base.AbstractPresenter;
import in.net.webinfotech.ranghar.presentation.routers.LoginRouter;
import in.net.webinfotech.ranghar.repository.impl.UserRepositoryImpl;

/**
 * Created by Raj on 21-03-2019.
 */

public class LoginPresenterImpl extends AbstractPresenter implements LoginPresenter, CheckLoginInteractor.Callback {

    Context mContext;
    LoginPresenter.View mView;
    CheckLoginInteractorImpl mInteractor;
    AndroidApplication androidApplication;
    LoginRouter mRouter;

    public LoginPresenterImpl(Executor executor,
                              MainThread mainThread,
                              Context context,
                              LoginPresenter.View view,
                              LoginRouter router
                              ) {
        super(executor, mainThread);
        this.mContext = context;
        this.mView = view;
        this.mRouter = router;
    }

    @Override
    public void checkLogin(String email, String password) {
        mInteractor = new CheckLoginInteractorImpl(mExecutor, mMainThread, this, new UserRepositoryImpl(), email, password);
        mInteractor.execute();
    }

    @Override
    public void onLoginSuccess(UserInfo userInfo) {
        mView.hideProgressBar();
        Toast toast = Toast.makeText(mContext, "Login Successful", Toast.LENGTH_SHORT);
        TextView v = (TextView) toast.getView().findViewById(android.R.id.message);
        v.setTextColor(mContext.getResources().getColor(R.color.appColor));
        toast.show();
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        androidApplication.setUserInfo(mContext, userInfo);
        mRouter.goToMain();
    }

    @Override
    public void onLoginFail(String errorMsg) {
        mView.hideProgressBar();
        Toast toast = Toast.makeText(mContext, errorMsg, Toast.LENGTH_SHORT);
        TextView v = (TextView) toast.getView().findViewById(android.R.id.message);
        v.setTextColor(mContext.getResources().getColor(R.color.appColor));
        toast.show();
    }

    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void stop() {

    }

    @Override
    public void destroy() {

    }
}
