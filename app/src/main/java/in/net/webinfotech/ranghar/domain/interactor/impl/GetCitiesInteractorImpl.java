package in.net.webinfotech.ranghar.domain.interactor.impl;

import in.net.webinfotech.ranghar.domain.executor.Executor;
import in.net.webinfotech.ranghar.domain.executor.MainThread;
import in.net.webinfotech.ranghar.domain.interactor.GetCitiesInteractor;
import in.net.webinfotech.ranghar.domain.interactor.base.AbstractInteractor;
import in.net.webinfotech.ranghar.domain.model.Cities.CitiesWrapper;
import in.net.webinfotech.ranghar.domain.model.Cities.City;
import in.net.webinfotech.ranghar.repository.CitiesRepository;
import in.net.webinfotech.ranghar.repository.impl.CitiesRepositoryImpl;

/**
 * Created by Raj on 07-03-2019.
 */

public class GetCitiesInteractorImpl extends AbstractInteractor implements GetCitiesInteractor {

    CitiesRepositoryImpl mRepository;
    Callback mCallback;

    public GetCitiesInteractorImpl(Executor threadExecutor,
                                   MainThread mainThread,
                                   CitiesRepositoryImpl repository,
                                   Callback callback
                                   ) {
        super(threadExecutor, mainThread);
        this.mCallback = callback;
        this.mRepository = repository;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingCitiesFail(errorMsg);
            }
        });
    }

    private void postMessage(final City[] cities){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingCitiesSuccess(cities);
            }
        });
    }

    @Override
    public void run() {
        final CitiesWrapper citiesWrapper = mRepository.getCities();
        if(citiesWrapper == null){
            notifyError("Something went wrong");
        }else if(!citiesWrapper.status){
            notifyError(citiesWrapper.message);
        }else {
            postMessage(citiesWrapper.cities);
        }
    }
}
