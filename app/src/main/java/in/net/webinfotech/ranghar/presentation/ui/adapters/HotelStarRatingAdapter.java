package in.net.webinfotech.ranghar.presentation.ui.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.net.webinfotech.ranghar.R;
import in.net.webinfotech.ranghar.domain.model.Other.HotelStars;
import in.net.webinfotech.ranghar.util.GlideHelper;

/**
 * Created by Raj on 18-03-2019.
 */

public class HotelStarRatingAdapter extends RecyclerView.Adapter<HotelStarRatingAdapter.ViewHolder> {

    Context mContext;
    ArrayList<HotelStars> hotelStars = new ArrayList<>();

    public HotelStarRatingAdapter(Context mContext, ArrayList<HotelStars> hotelStars) {
        this.mContext = mContext;
        this.hotelStars = hotelStars;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_hotel_star_rating, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        GlideHelper.setImageViewWithDrawable(mContext, holder.imgViewPoster, hotelStars.get(position).drawableId);
        holder.ratingBar.setNumStars(hotelStars.get(position).rating);
        holder.ratingBar.setRating(hotelStars.get(position).rating);
    }

    @Override
    public int getItemCount() {
        return hotelStars.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{
        @BindView(R.id.img_view_hotel_poster_star)
        ImageView imgViewPoster;
        @BindView(R.id.hotel_rating_bar)
        RatingBar ratingBar;
        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }


}
