package in.net.webinfotech.ranghar.presentation.ui.activities;

import android.Manifest;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.gson.Gson;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import in.net.webinfotech.ranghar.AndroidApplication;
import in.net.webinfotech.ranghar.R;
import in.net.webinfotech.ranghar.domain.executor.impl.ThreadExecutor;
import in.net.webinfotech.ranghar.domain.model.Cities.City;
import in.net.webinfotech.ranghar.domain.model.Room.Room;
import in.net.webinfotech.ranghar.presentation.presenters.GetHotelListPresenter;
import in.net.webinfotech.ranghar.presentation.presenters.impl.GetHotelListPresenterImpl;
import in.net.webinfotech.ranghar.presentation.routers.HotelListRouter;
import in.net.webinfotech.ranghar.presentation.ui.adapters.CityListLinearAdapter;
import in.net.webinfotech.ranghar.presentation.ui.adapters.HotelListAdapter;
import in.net.webinfotech.ranghar.presentation.ui.dialogs.DatePickerFragment;
import in.net.webinfotech.ranghar.presentation.ui.dialogs.RoomSelectDialog;
import in.net.webinfotech.ranghar.threading.MainThreadImpl;
import in.net.webinfotech.ranghar.util.GetLocationClass;

public class SearchActivity extends AppCompatActivity implements DatePickerDialog.OnDateSetListener,
                                                                    GetHotelListPresenter.View, CityListLinearAdapter.Callback, HotelListRouter, RoomSelectDialog.Callback, GetLocationClass.Callback{

    SearchView searchView;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.txt_view_check_in_date)
    TextView txtViewCheckInDate;
    @BindView(R.id.txt_view_check_out_date)
    TextView txtViewCheckOutDate;
    @BindView(R.id.search_constraint_layout)
    ConstraintLayout constraintLayout;
    String checkInDate;
    String checkOutDate;
    Boolean isCheckIn;
    android.support.v4.app.DialogFragment dialogFragment;
    @BindView(R.id.txt_view_final_room_count)
    TextView txtViewFinalRoomCount;
    @BindView(R.id.txt_view_final_adult)
    TextView txtViewFinalAdult;
    @BindView(R.id.txt_view_final_child)
    TextView txtViewFinalChild;
    @BindView(R.id.img_view_final_adult)
    ImageView imgViewFinalAdult;
    @BindView(R.id.img_view_final_child)
    ImageView imgViewFinalChild;
    @BindView(R.id.pagination_progressbar_layout)
    RelativeLayout paginationProgressBar;
    @BindView(R.id.recycler_view_room_list)
    RecyclerView recyclerViewRoomList;
    @BindView(R.id.recycler_view_city_list)
    RecyclerView recyclerViewCityList;
    Boolean isScrolling = false;
    Integer currentItems;
    Integer totalItems;
    Integer scrollOutItems;
    int pageNo = 1;
    int totalPage = 1;
    String searchKey = "";
    GetHotelListPresenterImpl mPresenter;
    LinearLayoutManager linearLayoutManager;
    AndroidApplication androidApplication;
    @BindView(R.id.nested_scroll_view_cities)
    NestedScrollView nestedScrollViewCities;
    @BindView(R.id.location_progress)
    RelativeLayout locationProgressLayout;
    String[] appPremisions = {
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.ACCESS_FINE_LOCATION
    };
    private static final int PERMISSIONS_REQUEST_CODE = 1240;

    int REQUEST_CHECK_SETTINGS = 1000;
    RoomSelectDialog roomSelectDialog;
    int finalAdultCount = 1;
    int finalChildCount = 0;
    int finalRoomCount = 1;
    GetLocationClass getLocationClass;
    @BindView(R.id.shimmer_layout)
    ShimmerFrameLayout shimmerFrameLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        intialisePresenter();
        loadCityList();
        initialiseRoomSelectDialog();
        initialiseGetLocationClass();
        roomSelectDialog.setUpDialogView();
    }

    public void intialisePresenter(){
        mPresenter = new GetHotelListPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this, this);
    }

    public void initialiseRoomSelectDialog(){
        roomSelectDialog = new RoomSelectDialog(this, this, this);
    }

    public void initialiseGetLocationClass(){
        getLocationClass = new GetLocationClass(this, this, this);
    }

    public void setUpDatePicker(){
        dialogFragment = new DatePickerFragment();
        androidApplication = (AndroidApplication) getApplicationContext();
        checkInDate = androidApplication.getCheckInDate();
        checkOutDate = androidApplication.getCheckOutDate();
        setDates(checkInDate, checkOutDate);
    }

    @Override
    public boolean onCreateOptionsMenu( Menu menu) {
        getMenuInflater().inflate( R.menu.search_menu, menu);

        MenuItem myActionMenuItem = menu.findItem( R.id.action_search);
        searchView = (SearchView) myActionMenuItem.getActionView();
        myActionMenuItem.expandActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                if(!query.isEmpty()){
                    pageNo = 1;
                    totalPage = 1;
                    searchKey = query;
                    showShimmerAnimation();
                    mPresenter.getHotelList(searchKey, pageNo, 1, checkInDate, checkOutDate, "refresh");
                }
                return false;
            }
            @Override
            public boolean onQueryTextChange(String s) {
                // UserFeedback.show( "SearchOnQueryTextChanged: " + s);
                if(!s.isEmpty()){
                    pageNo = 1;
                    totalPage = 1;
                    searchKey = s;
                    showShimmerAnimation();
                    mPresenter.getHotelList(searchKey, pageNo, 1, checkInDate, checkOutDate, "refresh");
                }
                return false;
            }
        });
        return true;
    }

    @Override
    public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
        if(isCheckIn) {
            Calendar c = Calendar.getInstance();
            c.set(Calendar.YEAR, i);
            c.set(Calendar.MONTH, i1);
            c.set(Calendar.DAY_OF_MONTH, i2);
            checkInDate = DateFormat.getDateInstance().format(c.getTime());
            if(!compareDates(checkInDate, checkOutDate)){
                SimpleDateFormat format = new SimpleDateFormat("MMM dd, yyyy");
                Date dt = new Date();
                try {
                    dt = format.parse(checkInDate);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                c = Calendar.getInstance();
                c.setTime(dt);
                c.add(Calendar.DATE, 1);
                checkOutDate = DateFormat.getDateInstance().format(c.getTime());
            }
        }else{
            Calendar c = Calendar.getInstance();
            c.set(Calendar.YEAR, i);
            c.set(Calendar.MONTH, i1);
            c.set(Calendar.DAY_OF_MONTH, i2);
            checkOutDate = DateFormat.getDateInstance().format(c.getTime());
        }
        androidApplication.setDates(checkInDate, checkOutDate);
        setDates(checkInDate, checkOutDate);
        if(!searchKey.isEmpty()){
            recyclerViewRoomList.setVisibility(View.GONE);
            showShimmerAnimation();
            mPresenter.getHotelList(searchKey,1, 1, checkInDate, checkOutDate, "refresh");
        }
    }

    public boolean compareDates(String checkIn, String checkOut){
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("MMM dd, yyyy", Locale.US);
            Date checkInDateObj = sdf.parse(checkIn);
            Date checkOutDateObj = sdf.parse(checkOut);
            if(checkInDateObj.before(checkOutDateObj)){
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    @OnClick(R.id.check_in_linear_layout) void onCheckInClicked(){
        isCheckIn = true;
        Bundle args = new Bundle();
        Date dt = Calendar.getInstance().getTime();
        args.putString("minimumDate", DateFormat.getDateInstance().format(dt));
        args.putString("currentDate", checkInDate);
        dialogFragment.setArguments(args);
        dialogFragment.show(getSupportFragmentManager(), "date picker");
    }

    @OnClick(R.id.check_out_linear_layout) void onCheckOutClicked(){
        isCheckIn = false;
        SimpleDateFormat format = new SimpleDateFormat("MMM dd, yyyy");
        Date dt = new Date();
        try {
            dt = format.parse(checkInDate);
        } catch (ParseException e) {
            Log.e("ErrorMsg", e.getMessage());
            e.printStackTrace();
        }
        Calendar c = Calendar.getInstance();
        c.setTime(dt);
        c.add(Calendar.DATE, 1);
        Bundle args = new Bundle();
        args.putString("minimumDate", DateFormat.getDateInstance().format(c.getTime()));
        args.putString("currentDate", checkOutDate);
        dialogFragment.setArguments(args);
        dialogFragment.show(getSupportFragmentManager(), "date picker");
    }

    public void setDates(String checkInDateString, String checkOutDateString){
        txtViewCheckInDate.setText(checkInDateString);
        txtViewCheckOutDate.setText(checkOutDateString);
    }

    @OnClick(R.id.txt_view_room_select) void onRoomSelectClicked(){

        roomSelectDialog.showDialog();
    }


    @Override
    public void loadAdapter(HotelListAdapter adapter, final int totalPage) {
        this.totalPage = totalPage;
        nestedScrollViewCities.setVisibility(View.GONE);
        recyclerViewRoomList.setVisibility(View.VISIBLE);

        //        Recycler view animation
        int resId = R.anim.layout_animation_left_to_right;
        LayoutAnimationController animation = AnimationUtils.loadLayoutAnimation(this, resId);
        recyclerViewRoomList.setLayoutAnimation(animation);

        linearLayoutManager = new LinearLayoutManager(this);
        recyclerViewRoomList.setAdapter(adapter);
        recyclerViewRoomList.setLayoutManager(linearLayoutManager);
        recyclerViewRoomList.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if(newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL)
                {
                    isScrolling= true;
                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                currentItems = linearLayoutManager.getChildCount();
                totalItems  = linearLayoutManager.getItemCount();
                scrollOutItems = linearLayoutManager.findFirstCompletelyVisibleItemPosition();
                if(!recyclerView.canScrollVertically(1))
                {
                    isScrolling  = false;
                    if(pageNo < totalPage){
                        pageNo = pageNo + 1;
                        showPaginationProgressBar();
                        mPresenter.getHotelList(searchKey, pageNo, 1, checkInDate, checkOutDate, "");
                    }
                }
            }
        });
    }



    @Override
    public void hidePaginationProgressBar() {
        paginationProgressBar.setVisibility(View.GONE);
    }



    @Override
    public void showShimmerAnimation() {
        shimmerFrameLayout.startShimmer();
        shimmerFrameLayout.setVisibility(View.VISIBLE);
        nestedScrollViewCities.setVisibility(View.GONE);
        recyclerViewRoomList.setVisibility(View.GONE);
    }

    @Override
    public void hideShimmerAnimation() {
        shimmerFrameLayout.stopShimmer();
        shimmerFrameLayout.setVisibility(View.GONE);
    }

    public void showPaginationProgressBar(){
        paginationProgressBar.setVisibility(View.VISIBLE);
    }

    public void loadCityList(){
        androidApplication  = (AndroidApplication) this.getApplicationContext();
        City[] cities = androidApplication.getCities(this);
        CityListLinearAdapter adapter = new CityListLinearAdapter(this, cities, this);
        recyclerViewCityList.setAdapter(adapter);
        recyclerViewCityList.setLayoutManager(new LinearLayoutManager(this));
        recyclerViewCityList.setNestedScrollingEnabled(false);

    }

    @Override
    public void onCityClicked(int cityId, String cityName) {
        Intent intent = new Intent(this, CityHotelsListActivity.class);
        intent.putExtra("cityName", cityName);
        intent.putExtra("cityId", cityId);
        startActivity(intent);
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransitionExit();
    }

    @Override
    public void startActivity(Intent intent) {
        super.startActivity(intent);
        overridePendingTransitionEnter();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransitionExit();
    }

    protected void overridePendingTransitionExit() {
        overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right);
    }

    protected void overridePendingTransitionEnter() {
        overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
    }

    @OnClick(R.id.card_view_current_location) void onCurrentLocationClicked(){
        if (checkAndRequestPermissions()) {
            getLocationClass.getCurrentLocation();
        }
    }

    public boolean checkAndRequestPermissions() {
        List<String> listPermissionsNeeded = new ArrayList<>();
        for (String perm : appPremisions) {
            if (ContextCompat.checkSelfPermission(this, perm) != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(perm);
            }
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), PERMISSIONS_REQUEST_CODE);
            return false;
        }
        return true;
    }



    public AlertDialog showAlertDialog(
            String title, String msg, String positiveLabel,
            DialogInterface.OnClickListener positiveOnClick,
            String negativeLabel, DialogInterface.OnClickListener negativeOnClick,
            boolean isCancelable
    ) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(title);
        builder.setCancelable(isCancelable);
        builder.setMessage(msg);
        builder.setPositiveButton(positiveLabel, positiveOnClick);
        builder.setNegativeButton(negativeLabel, negativeOnClick);
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
        return alertDialog;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == PERMISSIONS_REQUEST_CODE) {
            HashMap<String, Integer> permissionResults = new HashMap<>();
            int deniedCount = 0;
            for (int i = 0; i < grantResults.length; i++) {
                if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                    permissionResults.put(permissions[i], grantResults[i]);
                    deniedCount++;
                }
            }

            if (deniedCount == 0) {
//               Do your task
                getLocationClass.getCurrentLocation();
            } else {
                for (Map.Entry<String, Integer> entry : permissionResults.entrySet()) {
                    String permName = entry.getKey();
                    int permResult = entry.getValue();
                    if (ActivityCompat.shouldShowRequestPermissionRationale(this, permName)) {
                        this.showAlertDialog("", "This app needs location permissions",
                                "Yes, Grant permissions",
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                        checkAndRequestPermissions();
                                    }
                                },
                                "Cancel",
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                        Toast.makeText(getApplicationContext(), "Permissions are required to access your location", Toast.LENGTH_SHORT).show();
                                    }
                                },
                                false);
                    } else {
                        this.showAlertDialog("", "You have denied some permissions. Allow all permissions to download file at [Setting] > [Permissions]",
                                "Go to settings",
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                                                Uri.fromParts("package", getPackageName(), null));
                                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                        startActivity(intent);
                                    }
                                },
                                "Cancel",
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                        Toast.makeText(getApplicationContext(), "Permissions are required to access your location", Toast.LENGTH_SHORT).show();
                                    }
                                },
                                false);
                        break;
                    }
                }


            }

        }
    }

    @Override
    public void showLocationProgressBar(){
        locationProgressLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLocationProgressBar(){
        locationProgressLayout.setVisibility(View.GONE);
    }

    @Override
    public void goToHotelDetails(int id) {
        Intent intent = new Intent(this, HotelDetailsActivity.class);
        intent.putExtra("hotelId", id);
        startActivity(intent);
    }

    @Override
    public void setFinalRoomStatus(int adultCount, int childCount, int roomCount) {
            finalAdultCount = adultCount;
            finalChildCount = childCount;
            finalRoomCount = roomCount;
            txtViewFinalRoomCount.setText("Room " + Integer.toString(roomCount));
            if(finalChildCount > 0){
                imgViewFinalChild.setVisibility(View.VISIBLE);
                txtViewFinalChild.setVisibility(View.VISIBLE);
                txtViewFinalChild.setText(Integer.toString(finalChildCount));
            }else{
                imgViewFinalChild.setVisibility(View.GONE);
                txtViewFinalChild.setVisibility(View.GONE);
            }
            txtViewFinalAdult.setText(Integer.toString(finalAdultCount));
            if(!searchKey.isEmpty()){
                recyclerViewRoomList.setVisibility(View.GONE);
                showShimmerAnimation();
                mPresenter.getHotelList(searchKey,1, 1, checkInDate, checkOutDate, "refresh");
            }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CHECK_SETTINGS) {
            if (resultCode == -1) {
                showLocationProgressBar();
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }
                getLocationClass.getCurrentLocation();
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        androidApplication = (AndroidApplication) getApplicationContext();
        ArrayList<Room> roomArrayList = androidApplication.getRooms();
        Log.e("ErrorMsg", "in City List: rooms " + roomArrayList.size());
        finalRoomCount = roomArrayList.size();
        int adult = 0;
        int child = 0;
        for(int i = 0; i < roomArrayList.size(); i++){
            adult = adult + roomArrayList.get(i).adults;
            child = child + roomArrayList.get(i).child;
        }
        finalAdultCount = adult;
        finalChildCount = child;
        setUpDatePicker();
        setFinalRoomStatus(finalAdultCount, finalChildCount, finalRoomCount);
    }

}
