package in.net.webinfotech.ranghar.repository;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.POST;

/**
 * Created by Raj on 15-03-2019.
 */

public interface OtherRepository {
    @POST("api/app_slider/slider_fetch.php")
    Call<ResponseBody> getPosterImage();
}
