package in.net.webinfotech.ranghar;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.google.gson.Gson;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import in.net.webinfotech.ranghar.domain.model.Cities.City;
import in.net.webinfotech.ranghar.domain.model.Other.PosterImage;
import in.net.webinfotech.ranghar.domain.model.Room.Room;
import in.net.webinfotech.ranghar.domain.model.User.UserInfo;
import in.net.webinfotech.ranghar.domain.model.WishList.WishList;

/**
 * Created by Raj on 15-03-2019.
 */

public class AndroidApplication extends Application {
    public City[] cities;
    public PosterImage posterImage;
    public UserInfo userInfo;
    public ArrayList<Room> rooms;
    public WishList[] wishLists;
    public String checkInDate;
    public String checkOutDate;

    @Override
    public void onCreate() {
        super.onCreate();
        initialiseRooms();
        initialiseDates();
    }

    public void initialiseRooms(){
        rooms = new ArrayList<>();
        Room room = new Room(1, 0);
        rooms.add(room);
    }

    public void initialiseDates(){
        Date dt = Calendar.getInstance().getTime();
        checkInDate = DateFormat.getDateInstance().format(dt);
        SimpleDateFormat format = new SimpleDateFormat("MMM dd, yyyy");
        try {
            dt = format.parse(checkInDate);
        } catch (ParseException e) {
            Log.e("ErrorMsg", e.getMessage());
            e.printStackTrace();
        }
        Calendar c = Calendar.getInstance();
        c.setTime(dt);
        c.add(Calendar.DATE, 1);
        checkOutDate = DateFormat.getDateInstance().format(c.getTime());
    }

    public void setCities(Context context, City[] cities){
        this.cities = cities;
        SharedPreferences sharedPref = context.getSharedPreferences(
                getString(R.string.KEY_RANGHAR_APP_PREFERENCES), Context.MODE_PRIVATE);

        SharedPreferences.Editor editor = sharedPref.edit();

        if(cities != null){
            editor.putString(getString(R.string.KEY_CITIES), new Gson().toJson(cities));
        } else {
            editor.putString(getString(R.string.KEY_CITIES), "");
        }

        editor.commit();
    }

    public City[] getCities(Context context){
        City[] cities;
        if(this.cities != null){
            cities = this.cities;
        }else{
            SharedPreferences sharedPref = context.getSharedPreferences(
                    getString(R.string.KEY_RANGHAR_APP_PREFERENCES), Context.MODE_PRIVATE);
            String userInfoJson = sharedPref.getString(getString(R.string.KEY_CITIES),"");
            if(userInfoJson.isEmpty()){
                cities = null;
            }else{
                try {
                    cities = new Gson().fromJson(userInfoJson, City[].class);
                    this.cities = cities;
                }catch (Exception e){
                    cities = null;
                }
            }
        }
        return cities;
    }

    public void setPosterImage(Context context, PosterImage posterImage){
        this.posterImage = posterImage;
        SharedPreferences sharedPref = context.getSharedPreferences(
                getString(R.string.KEY_RANGHAR_APP_PREFERENCES), Context.MODE_PRIVATE);

        SharedPreferences.Editor editor = sharedPref.edit();

        if(posterImage != null){
            editor.putString(getString(R.string.KEY_POSTER_IMAGE), new Gson().toJson(posterImage));
        } else {
            editor.putString(getString(R.string.KEY_POSTER_IMAGE), "");
        }

        editor.commit();
    }

    public PosterImage getPosterImage(Context context){
        PosterImage posterImage;
        if(this.posterImage != null){
            posterImage = this.posterImage;
        }else{
            SharedPreferences sharedPref = context.getSharedPreferences(
                    getString(R.string.KEY_RANGHAR_APP_PREFERENCES), Context.MODE_PRIVATE);
            String posterImageJson = sharedPref.getString(getString(R.string.KEY_POSTER_IMAGE),"");
            if(posterImageJson.isEmpty()){
                posterImage = null;
            }else{
                try {
                    posterImage = new Gson().fromJson(posterImageJson, PosterImage.class);
                    this.posterImage = posterImage;
                }catch (Exception e){
                    posterImage = null;
                }
            }
        }
        return posterImage;
    }

    public void setUserInfo(Context context, UserInfo userInfo){
        this.userInfo = userInfo;
        SharedPreferences sharedPref = context.getSharedPreferences(
                getString(R.string.KEY_RANGHAR_APP_PREFERENCES), Context.MODE_PRIVATE);

        SharedPreferences.Editor editor = sharedPref.edit();
        if(userInfo != null){
            editor.putString(getString(R.string.KEY_USER_INFO), new Gson().toJson(userInfo));
        }else{
            editor.putString(getString(R.string.KEY_USER_INFO), "");
        }
        editor.commit();

    }

    public UserInfo getUserInfo(Context context) {
        UserInfo userInfo;
        if (this.userInfo != null) {
            userInfo = this.userInfo;
        } else {
            SharedPreferences sharedPref = context.getSharedPreferences(
                    getString(R.string.KEY_RANGHAR_APP_PREFERENCES), Context.MODE_PRIVATE);
            String userInfoJson = sharedPref.getString(getString(R.string.KEY_USER_INFO), "");
            if (userInfoJson.isEmpty()) {
                userInfo = null;
            } else {
                try {
                    userInfo = new Gson().fromJson(userInfoJson, UserInfo.class);
                    this.userInfo = userInfo;
                } catch (Exception e) {
                    userInfo = null;
                }
            }
        }
        return userInfo;
    }

    public void setRooms(ArrayList<Room> roomsArray){
        this.rooms = roomsArray;
    }

    public ArrayList<Room> getRooms(){
        return rooms;
    }

    public void setWishLists(WishList[] wishLists){
        this.wishLists = wishLists;
    }

    public WishList[] getWishLists(){
        return this.wishLists;
    }

    public void setDates(String checkInDate, String checkOutDate){
        this.checkInDate = checkInDate;
        this.checkOutDate = checkOutDate;
    }

    public String getCheckInDate(){
        return checkInDate;
    }

    public String getCheckOutDate(){
        return checkOutDate;
    }

}
