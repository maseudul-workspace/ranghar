package in.net.webinfotech.ranghar.presentation.presenters;

import in.net.webinfotech.ranghar.presentation.presenters.base.BasePresenter;

/**
 * Created by Raj on 25-03-2019.
 */

public interface ChangePasswordPresenter extends BasePresenter {
    void changePassword(String apiKey, int userId, String currentPswd, String newPswd);
    interface View{
        void showProgressBar();
        void hideProgressBar();
    }
}
