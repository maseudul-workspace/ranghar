package in.net.webinfotech.ranghar.domain.interactor.impl;

import in.net.webinfotech.ranghar.domain.executor.Executor;
import in.net.webinfotech.ranghar.domain.executor.MainThread;
import in.net.webinfotech.ranghar.domain.interactor.DeleteFromWishListInteractor;
import in.net.webinfotech.ranghar.domain.interactor.base.AbstractInteractor;
import in.net.webinfotech.ranghar.domain.model.WishList.DeleteFromWishListResponse;
import in.net.webinfotech.ranghar.repository.impl.WishListRepositoryImpl;

/**
 * Created by Raj on 20-04-2019.
 */

public class DeleteFromWishListInteractorImpl extends AbstractInteractor implements DeleteFromWishListInteractor{

    Callback mCallback;
    WishListRepositoryImpl mRepository;
    String apiKey;
    int userId;
    int wishListId;

    public DeleteFromWishListInteractorImpl(Executor threadExecutor,
                                            MainThread mainThread,
                                            Callback callback,
                                            WishListRepositoryImpl repository,
                                            String apiKey,
                                            int userId,
                                            int wishListId
                                            ) {
        super(threadExecutor, mainThread);
        this.mCallback = callback;
        this.mRepository = repository;
        this.apiKey = apiKey;
        this.userId = userId;
        this.wishListId = wishListId;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onDeleteFail(errorMsg);
            }
        });
    }

    private void postMessage(){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onDeleteSuccess();
            }
        });
    }

    @Override
    public void run() {
        final DeleteFromWishListResponse deleteFromWishListResponse = mRepository.deleteFromWishList(apiKey, userId, wishListId);
        if(deleteFromWishListResponse == null){
            notifyError("Something went wrong");
        }else if(!deleteFromWishListResponse.status){
            notifyError(deleteFromWishListResponse.message);
        }else{
            postMessage();
        }
    }
}
