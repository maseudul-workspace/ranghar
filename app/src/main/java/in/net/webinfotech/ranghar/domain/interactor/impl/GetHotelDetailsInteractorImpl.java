package in.net.webinfotech.ranghar.domain.interactor.impl;

import in.net.webinfotech.ranghar.domain.executor.Executor;
import in.net.webinfotech.ranghar.domain.executor.MainThread;
import in.net.webinfotech.ranghar.domain.interactor.GetHotelDetailsInteractor;
import in.net.webinfotech.ranghar.domain.interactor.base.AbstractInteractor;
import in.net.webinfotech.ranghar.domain.model.Hotel.HotelDetails;
import in.net.webinfotech.ranghar.domain.model.Hotel.HotelDetailsWrapper;
import in.net.webinfotech.ranghar.domain.model.Hotel.HotelList;
import in.net.webinfotech.ranghar.repository.impl.HotelRepositoryImpl;

/**
 * Created by Raj on 16-04-2019.
 */

public class GetHotelDetailsInteractorImpl extends AbstractInteractor implements GetHotelDetailsInteractor{

    Callback mCallback;
    HotelRepositoryImpl mRepository;
    int hotelId;
    String checkIn;
    String checkOut;

    public GetHotelDetailsInteractorImpl(Executor threadExecutor,
                                         MainThread mainThread,
                                         Callback callback,
                                         HotelRepositoryImpl repository,
                                         int hotelId,
                                         String checkIn,
                                         String checkOut
                                         ) {
        super(threadExecutor, mainThread);
        this.mCallback = callback;
        this.mRepository = repository;
        this.hotelId = hotelId;
        this.checkIn = checkIn;
        this.checkOut = checkOut;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingHotelDetailsFail(errorMsg);
            }
        });
    }

    private void postMessage(final HotelDetails hotelDetails){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingHotelDetailsSuccess(hotelDetails);
            }
        });
    }

    @Override
    public void run() {
        final HotelDetailsWrapper hotelDetailsWrapper = mRepository.getHotelDetails(hotelId, checkIn, checkOut);
        if(hotelDetailsWrapper == null){
            notifyError("Something went wrong");
        }else if(!hotelDetailsWrapper.status){
            notifyError(hotelDetailsWrapper.message);
        }else{
            postMessage(hotelDetailsWrapper.data);
        }
    }
}
