package in.net.webinfotech.ranghar.presentation.ui.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.net.webinfotech.ranghar.R;
import in.net.webinfotech.ranghar.domain.model.Hotel.HotelImages;
import in.net.webinfotech.ranghar.util.GlideHelper;

/**
 * Created by Raj on 17-04-2019.
 */

public class HotelImagesAdapter extends RecyclerView.Adapter<HotelImagesAdapter.ViewHolder>{

    public interface Callback{
        void onImageClicked(String imageUrl);
    }

    HotelImages[] hotelImages;
    Context mContext;
    Callback mCallback;

    public HotelImagesAdapter(HotelImages[] hotelImages, Context mContext, Callback callback) {
        this.hotelImages = hotelImages;
        this.mContext = mContext;
        this.mCallback = callback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_hotel_images, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        GlideHelper.setImageView(mContext, holder.imgViewHotelImage, mContext.getResources().getString(R.string.base_url) + "uploads/hotel_image/" + hotelImages[position].image );
        holder.imgViewHotelImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.onImageClicked(hotelImages[position].image);
            }
        });
    }

    @Override
    public int getItemCount() {
        return hotelImages.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.img_view_hotel_image)
        ImageView imgViewHotelImage;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
