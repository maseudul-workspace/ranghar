package in.net.webinfotech.ranghar.presentation.presenters.impl;

import android.content.Context;
import android.widget.TextView;
import android.widget.Toast;

import in.net.webinfotech.ranghar.AndroidApplication;
import in.net.webinfotech.ranghar.R;
import in.net.webinfotech.ranghar.domain.executor.Executor;
import in.net.webinfotech.ranghar.domain.executor.MainThread;
import in.net.webinfotech.ranghar.domain.interactor.CreateUserInteractor;
import in.net.webinfotech.ranghar.domain.interactor.impl.CreateUserInteractorImpl;
import in.net.webinfotech.ranghar.domain.model.User.UserInfo;
import in.net.webinfotech.ranghar.presentation.presenters.SignUpPresenter;
import in.net.webinfotech.ranghar.presentation.presenters.base.AbstractPresenter;
import in.net.webinfotech.ranghar.presentation.routers.SignUpRouter;
import in.net.webinfotech.ranghar.repository.impl.UserRepositoryImpl;

/**
 * Created by Raj on 21-03-2019.
 */

public class SignUpPresenterImpl extends AbstractPresenter implements SignUpPresenter, CreateUserInteractor.Callback {

    Context mContext;
    SignUpPresenter.View mView;
    CreateUserInteractorImpl mInteractor;
    AndroidApplication androidApplication;
    SignUpRouter mRouter;

    public SignUpPresenterImpl(Executor executor,
                               MainThread mainThread,
                               Context context,
                               SignUpPresenter.View view,
                               SignUpRouter router
                               ) {
        super(executor, mainThread);
        this.mContext = context;
        this.mView = view;
        this.mRouter = router;
    }

    @Override
    public void register(String name, String mobile, String email, String password) {
        mInteractor = new CreateUserInteractorImpl(mExecutor, mMainThread,
                                                    this, new UserRepositoryImpl(),
                                                    name, mobile, email, password);
        mInteractor.execute();
    }

    @Override
    public void onUserCreateSuccess(UserInfo userInfo) {
        mView.hideProgressBar();
        Toast toast = Toast.makeText(mContext, "Sign Up Successful", Toast.LENGTH_SHORT);
        TextView v = (TextView) toast.getView().findViewById(android.R.id.message);
        v.setTextColor(mContext.getResources().getColor(R.color.appColor));
        toast.show();
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        androidApplication.setUserInfo(mContext, userInfo);
        mRouter.goToMain();
    }

    @Override
    public void onUserCreateFail(String errorMsg) {
        mView.hideProgressBar();
        Toast toast = Toast.makeText(mContext, errorMsg, Toast.LENGTH_SHORT);
        TextView v = (TextView) toast.getView().findViewById(android.R.id.message);
        v.setTextColor(mContext.getResources().getColor(R.color.appColor));
        toast.show();
    }

    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void stop() {

    }

    @Override
    public void destroy() {

    }
}
