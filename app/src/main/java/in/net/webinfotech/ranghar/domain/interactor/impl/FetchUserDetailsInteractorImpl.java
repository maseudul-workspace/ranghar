package in.net.webinfotech.ranghar.domain.interactor.impl;

import in.net.webinfotech.ranghar.domain.executor.Executor;
import in.net.webinfotech.ranghar.domain.executor.MainThread;
import in.net.webinfotech.ranghar.domain.interactor.FetchUserDetailsInteractor;
import in.net.webinfotech.ranghar.domain.interactor.base.AbstractInteractor;
import in.net.webinfotech.ranghar.domain.model.User.UserInfo;
import in.net.webinfotech.ranghar.domain.model.User.UserInfoWrapper;
import in.net.webinfotech.ranghar.repository.impl.UserRepositoryImpl;

/**
 * Created by Raj on 21-03-2019.
 */

public class FetchUserDetailsInteractorImpl extends AbstractInteractor implements FetchUserDetailsInteractor {

    Callback mCallback;
    UserRepositoryImpl mRepository;
    String apiKey;
    int userId;

    public FetchUserDetailsInteractorImpl(Executor threadExecutor,
                                          MainThread mainThread,
                                          Callback callback,
                                          UserRepositoryImpl repository,
                                          String apiKey,
                                          int userId
                                          ) {
        super(threadExecutor, mainThread);
        this.mCallback = callback;
        this.mRepository = repository;
        this.apiKey = apiKey;
        this.userId = userId;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettitngUserDetailFail(errorMsg);
            }
        });
    }

    private void postMessage(final UserInfo userInfo){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingUserDetailsSuccess(userInfo);
            }
        });
    }


    @Override
    public void run() {
        final UserInfoWrapper userInfoWrapper = mRepository.fetchUserDetails(apiKey, userId);
        if(userInfoWrapper == null){
            notifyError("Something went wrong");
        }else if(!userInfoWrapper.status){
            notifyError(userInfoWrapper.message);
        }else{
            postMessage(userInfoWrapper.userInfo);
        }
    }
}
