package in.net.webinfotech.ranghar.domain.model.Hotel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Raj on 14-03-2019.
 */

public class HotelListWrapper {
    @SerializedName("status")
    @Expose
    public boolean status;

    @SerializedName("message")
    @Expose
    public String message;

    @SerializedName("code")
    @Expose
    public int code;

    @SerializedName("total_page")
    @Expose
    public int total_page;

    @SerializedName("search_key")
    @Expose
    public String search_key;

    @SerializedName("data")
    @Expose
    public HotelList[] data;

}
