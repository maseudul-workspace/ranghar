package in.net.webinfotech.ranghar.repository.impl;

import android.util.Log;
import android.widget.Adapter;

import com.google.gson.Gson;

import in.net.webinfotech.ranghar.domain.model.Cities.CitiesWrapper;
import in.net.webinfotech.ranghar.repository.APIclient;
import in.net.webinfotech.ranghar.repository.CitiesRepository;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by Raj on 07-03-2019.
 */

public class CitiesRepositoryImpl {
    CitiesRepository mRepository;

    public CitiesRepositoryImpl() {
        mRepository = APIclient.createService(CitiesRepository.class);
    }

    public CitiesWrapper getCities(){
        CitiesWrapper citiesWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> removeItem = mRepository.getCities();

            Response<ResponseBody> response = removeItem.execute();
            if(response.body() != null){
                responseBody = response.body().string();
                Log.e("ErrorMsg", responseBody);
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    citiesWrapper = null;
                }else{
                    citiesWrapper = gson.fromJson(responseBody, CitiesWrapper.class);
                }
            } else {
                citiesWrapper = null;
            }
        }catch (Exception e){
            Log.e("ErrorMsg", e.getMessage());
            citiesWrapper = null;
        }
        return citiesWrapper;
    }

}
