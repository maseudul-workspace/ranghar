package in.net.webinfotech.ranghar.presentation.presenters.impl;

import android.content.Context;
import android.widget.Toast;

import in.net.webinfotech.ranghar.AndroidApplication;
import in.net.webinfotech.ranghar.domain.executor.Executor;
import in.net.webinfotech.ranghar.domain.executor.MainThread;
import in.net.webinfotech.ranghar.domain.interactor.AddToWishListInteractor;
import in.net.webinfotech.ranghar.domain.interactor.DeleteFromWishListInteractor;
import in.net.webinfotech.ranghar.domain.interactor.FetchWishListInteractor;
import in.net.webinfotech.ranghar.domain.interactor.impl.AddToWishListInteractorImpl;
import in.net.webinfotech.ranghar.domain.interactor.impl.DeleteFromWishListInteractorImpl;
import in.net.webinfotech.ranghar.domain.interactor.impl.FetchWishListInteractorImpl;
import in.net.webinfotech.ranghar.domain.model.User.UserInfo;
import in.net.webinfotech.ranghar.domain.model.WishList.DeleteFromWishListResponse;
import in.net.webinfotech.ranghar.domain.model.WishList.WishList;
import in.net.webinfotech.ranghar.presentation.presenters.WishListPresenter;
import in.net.webinfotech.ranghar.presentation.presenters.base.AbstractPresenter;
import in.net.webinfotech.ranghar.presentation.routers.WishListRouter;
import in.net.webinfotech.ranghar.presentation.ui.adapters.WishListAdapter;
import in.net.webinfotech.ranghar.repository.impl.WishListRepositoryImpl;

/**
 * Created by Raj on 22-04-2019.
 */

public class WishListPresenterImpl extends AbstractPresenter implements WishListPresenter,
                                                                        WishListAdapter.Callback,
                                                                        FetchWishListInteractor.Callback,
                                                                        DeleteFromWishListInteractor.Callback,
                                                                        AddToWishListInteractor.Callback
{

    Context mContext;
    WishListPresenter.View mView;
    WishListAdapter adapter = null;
    FetchWishListInteractorImpl mInteractor;
    AndroidApplication androidApplication;
    DeleteFromWishListInteractorImpl deleteFromWishListInteractor;
    AddToWishListInteractorImpl addToWishListInteractor;
    int position;
    WishListRouter mRouter;

    public WishListPresenterImpl(Executor executor,
                                 MainThread mainThread,
                                 Context context,
                                 WishListPresenter.View view,
                                 WishListRouter router
                                 ) {
        super(executor, mainThread);
        this.mContext = context;
        this.mView = view;
        this.mRouter = router;
    }

    @Override
    public void getWishList() {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        mInteractor = new FetchWishListInteractorImpl(mExecutor, mMainThread, this, new WishListRepositoryImpl(), userInfo.api_key, userInfo.user_id);
        mInteractor.execute();
    }

    @Override
    public void onHotelClicked(int hotelId) {
        mRouter.goToHotelDetails(hotelId);
    }

    @Override
    public void onHeartFillClicked(int wishListId, int position) {
        this.position = position;
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        deleteFromWishListInteractor = new DeleteFromWishListInteractorImpl(mExecutor, mMainThread, this, new WishListRepositoryImpl(), userInfo.api_key, userInfo.user_id, wishListId);
        deleteFromWishListInteractor.execute();
    }

    @Override
    public void onHeartOutlineClicked(int hotelId, int position) {
        this.position = position;
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        addToWishListInteractor = new AddToWishListInteractorImpl(mExecutor, mMainThread, this, new WishListRepositoryImpl(), userInfo.api_key,userInfo.user_id, hotelId);
        addToWishListInteractor.execute();
    }

    @Override
    public void onGettingWishListSuccess(WishList[] wishLists) {
        adapter = new WishListAdapter(mContext, wishLists, this);
        mView.loadAdapter(adapter);
        mView.hideShimmerAnimation();
    }

    @Override
    public void onGettingWishListFail(String errorMsg) {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        androidApplication.setWishLists(null);
        mView.hideShimmerAnimation();
    }

    @Override
    public void onDeleteSuccess() {
        adapter.updateWishList(this.position);
        getWishList();
    }

    @Override
    public void onDeleteFail(String errorMsg) {
        Toast.makeText(mContext, errorMsg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onAddToWishListSuccess() {
        adapter.updateWishList(this.position);
        Toast.makeText(mContext, "Successfully added to your wishlist", Toast.LENGTH_SHORT).show();
        getWishList();
    }

    @Override
    public void onAddToWishListFail(String errorMsg) {
        Toast.makeText(mContext, errorMsg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void stop() {

    }

    @Override
    public void destroy() {

    }

}
