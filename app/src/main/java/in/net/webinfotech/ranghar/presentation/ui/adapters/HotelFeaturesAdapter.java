package in.net.webinfotech.ranghar.presentation.ui.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.net.webinfotech.ranghar.R;
import in.net.webinfotech.ranghar.domain.model.Hotel.HotelFeatures;
import in.net.webinfotech.ranghar.util.GlideHelper;

/**
 * Created by Raj on 02-05-2019.
 */

public class HotelFeaturesAdapter extends RecyclerView.Adapter<HotelFeaturesAdapter.ViewHolder>{

    Context mContext;
    ArrayList<HotelFeatures> hotelFeatures;

    public HotelFeaturesAdapter(Context mContext, ArrayList<HotelFeatures> hotelFeatures) {
        this.mContext = mContext;
        this.hotelFeatures = hotelFeatures;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_hotel_features, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        switch (hotelFeatures.get(position).featureName){
            case "AC":
                if(hotelFeatures.get(position).isPresent){
                    GlideHelper.setImageViewWithDrawable(mContext, holder.imgViewFeature, R.drawable.ac);
                }else{
                    GlideHelper.setImageViewWithDrawable(mContext, holder.imgViewFeature, R.drawable.ac_disabled);
                }
                holder.txtViewFeatureName.setText("Air Conditioner");
                break;
            case "Gym":
                if(hotelFeatures.get(position).isPresent){
                    GlideHelper.setImageViewWithDrawable(mContext, holder.imgViewFeature, R.drawable.gym);
                }else{
                    GlideHelper.setImageViewWithDrawable(mContext, holder.imgViewFeature, R.drawable.gym_disabled);
                }
                holder.txtViewFeatureName.setText("Gym");
                break;
            case "Internet":
                if(hotelFeatures.get(position).isPresent){
                    GlideHelper.setImageViewWithDrawable(mContext, holder.imgViewFeature, R.drawable.internet);
                }else{
                    GlideHelper.setImageViewWithDrawable(mContext, holder.imgViewFeature, R.drawable.internet_disabled);
                }
                holder.txtViewFeatureName.setText("Internet");
                break;
            case "RoomService":
                if(hotelFeatures.get(position).isPresent){
                    GlideHelper.setImageViewWithDrawable(mContext, holder.imgViewFeature, R.drawable.room_service);
                }else{
                    GlideHelper.setImageViewWithDrawable(mContext, holder.imgViewFeature, R.drawable.room_service_disabled);
                }
                holder.txtViewFeatureName.setText("Room Service");
                break;
            case "Restaurant":
                if(hotelFeatures.get(position).isPresent){
                    GlideHelper.setImageViewWithDrawable(mContext, holder.imgViewFeature, R.drawable.retaurant);
                }else{
                    GlideHelper.setImageViewWithDrawable(mContext, holder.imgViewFeature, R.drawable.restaurant_disabled);
                }
                holder.txtViewFeatureName.setText("Restaurant");
                break;
            case "Bar":
                if(hotelFeatures.get(position).isPresent){
                    GlideHelper.setImageViewWithDrawable(mContext, holder.imgViewFeature, R.drawable.bar);
                }else{
                    GlideHelper.setImageViewWithDrawable(mContext, holder.imgViewFeature, R.drawable.bar_dsiabled);
                }
                holder.txtViewFeatureName.setText("Bar");
                break;
            case "TV":
                if(hotelFeatures.get(position).isPresent){
                    GlideHelper.setImageViewWithDrawable(mContext, holder.imgViewFeature, R.drawable.tv);
                }else{
                    GlideHelper.setImageViewWithDrawable(mContext, holder.imgViewFeature, R.drawable.tv_disabled);
                }
                holder.txtViewFeatureName.setText("TV");
                break;
            case "Parking":
                if(hotelFeatures.get(position).isPresent){
                    GlideHelper.setImageViewWithDrawable(mContext, holder.imgViewFeature, R.drawable.parking);
                }else{
                    GlideHelper.setImageViewWithDrawable(mContext, holder.imgViewFeature, R.drawable.parking_disabled);
                }
                holder.txtViewFeatureName.setText("Parking");
                break;
            case "Swimming":
                if(hotelFeatures.get(position).isPresent){
                    GlideHelper.setImageViewWithDrawable(mContext, holder.imgViewFeature, R.drawable.swimming);
                }else{
                    GlideHelper.setImageViewWithDrawable(mContext, holder.imgViewFeature, R.drawable.swimming_disabled);
                }
                holder.txtViewFeatureName.setText("Swimming");
                break;
        }
    }

    @Override
    public int getItemCount() {
        return hotelFeatures.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.img_view_feature)
        ImageView imgViewFeature;
        @BindView(R.id.txt_view_feature_name)
        TextView txtViewFeatureName;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
