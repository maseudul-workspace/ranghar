package in.net.webinfotech.ranghar.presentation.ui.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.net.webinfotech.ranghar.R;
import in.net.webinfotech.ranghar.domain.model.Cities.City;

/**
 * Created by Raj on 15-03-2019.
 */

public class CityListLinearAdapter extends RecyclerView.Adapter<CityListLinearAdapter.ViewHolder>{

    public interface Callback{
        void onCityClicked(int cityId, String cityName);
    }

    Context mContext;
    City[] cities;
    Callback mCallback;

    public CityListLinearAdapter(Context mContext, City[] cities, Callback callback) {
        this.mContext = mContext;
        this.cities = cities;
        this.mCallback = callback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_cities_linear_layout, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        holder.txtViewCityName.setText(cities[position].city);
        holder.txtViewCityName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.onCityClicked(cities[position].id, cities[position].city);
            }
        });
    }

    @Override
    public int getItemCount() {
        return cities.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{
        @BindView(R.id.txt_view_city_name)
        TextView txtViewCityName;
        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
