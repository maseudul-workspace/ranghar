package in.net.webinfotech.ranghar.presentation.ui.dialogs;

import android.app.Activity;
import android.content.Context;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import in.net.webinfotech.ranghar.R;

/**
 * Created by Raj on 16-05-2019.
 */

public class BookingSuccessDialog {
    public interface Callback{
        void onPayNowClick();
        void onPayLaterClicked();
    }

    AlertDialog.Builder builder;
    Context mContext;
    Activity mActivity;
    AlertDialog dialog;
    View dialogContainer;
    Callback mCallback;

    public BookingSuccessDialog(Context mContext, Activity mActivity, Callback callback) {
        this.mContext = mContext;
        this.mActivity = mActivity;
        this.mCallback = callback;
    }

    public void showDialog(String hotelName, Double price, String personStatus, String roomStatus, String duration){
        dialogContainer = mActivity.getLayoutInflater().inflate(R.layout.booking_success_dialog, null);

        TextView txtViewHotelName = (TextView) dialogContainer.findViewById(R.id.txt_view_hotel_name);
        ImageView imgViewCancel = (ImageView) dialogContainer.findViewById(R.id.img_view_cancel_dialog);
        TextView txtViewPrice = (TextView) dialogContainer.findViewById(R.id.txt_view_total_price);
        TextView txtViewRoomStatus = (TextView) dialogContainer.findViewById(R.id.txt_view_room_status);
        TextView txtViewPersonStatus = (TextView) dialogContainer.findViewById(R.id.txt_view_person_status);
        Button btnPayNow = (Button) dialogContainer.findViewById(R.id.btn_pay_now);
        Button btnPayLater = (Button) dialogContainer.findViewById(R.id.btn_pay_later);
        TextView txtViewDateDuration = (TextView) dialogContainer.findViewById(R.id.txt_view_booking_date_duration);

        txtViewHotelName.setText(hotelName);
        txtViewPrice.setText(Double.toString(price));
        txtViewRoomStatus.setText(roomStatus);
        txtViewPersonStatus.setText(personStatus);
        txtViewDateDuration.setText(duration);

        imgViewCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        btnPayLater.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                mCallback.onPayLaterClicked();
            }
        });

        btnPayNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                mCallback.onPayNowClick();
            }
        });

        builder = new AlertDialog.Builder(mContext);
        builder.setView(dialogContainer);
        dialog = builder.create();
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        dialog.show();
    }

}
