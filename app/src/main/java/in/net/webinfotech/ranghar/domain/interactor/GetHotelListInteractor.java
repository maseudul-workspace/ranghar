package in.net.webinfotech.ranghar.domain.interactor;

import in.net.webinfotech.ranghar.domain.model.Hotel.HotelList;

/**
 * Created by Raj on 15-03-2019.
 */

public interface GetHotelListInteractor {
    interface Callback{
        void onGettingHotelListSuccess(HotelList[] hotelLists, String searchKey, int totalPage);
        void onGettingHotelListFail(String errorMsg);
    }
}
