package in.net.webinfotech.ranghar.presentation.ui.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.net.webinfotech.ranghar.R;
import in.net.webinfotech.ranghar.domain.model.Hotel.HotelList;
import in.net.webinfotech.ranghar.util.GlideHelper;

/**
 * Created by Raj on 15-03-2019.
 */

public class HotelListAdapter extends RecyclerView.Adapter<HotelListAdapter.ViewHolder>{

    public interface Callback{
        void onHotelClicked(int id);
        void onHeartOutlineClicked(int id, int position);
        void onHeartFillClicked(int id, int position);
    }

    Context mContext;
    HotelList[] hotelLists;
    Callback mCallback;

    public HotelListAdapter(Context mContext, HotelList[] hotelLists, Callback callback) {
        this.mContext = mContext;
        this.hotelLists = hotelLists;
        this.mCallback = callback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_hotel_list, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        GlideHelper.setImageView(mContext, holder.imgViewHotelPoster, mContext.getResources().getString(R.string.base_url) + "uploads/hotel_image/thumb/" + hotelLists[position].main_image);
        holder.txtViewHotelName.setText(hotelLists[position].hotel_name);
        holder.txtViewHotelAddress.setText(hotelLists[position].state + ", " + hotelLists[position].city + ", " + hotelLists[position].locality);
        holder.imgViewHotelPoster.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.onHotelClicked(hotelLists[position].hotel_id);
            }
        });
        if(!hotelLists[position].isRoomAvailable){
            holder.transparentRelativeLayout.setVisibility(View.VISIBLE);
        }else{
            holder.transparentRelativeLayout.setVisibility(View.GONE);
        }
        if(hotelLists[position].availability.count_single_room == 0){
            holder.linearLayoutSingleRoom.setVisibility(View.GONE);
        }
        if(hotelLists[position].availability.count_double_room == 0){
            holder.linearLayoutDoubleRoom.setVisibility(View.GONE);
        }
        holder.txtViewSingleRoomPrice.setText(Double.toString(hotelLists[position].single_room_price));
        holder.txtViewDoubleRoomPrice.setText(Double.toString(hotelLists[position].double_room_price));
//        if(hotelLists[position].isWishListPresent){
//            holder.imgViewHeartFill.setVisibility(View.VISIBLE);
//            holder.imgViewHeartFill.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    mCallback.onHeartFillClicked(hotelLists[position].hotel_id, position);
//                }
//            });
//        }else{
//            holder.imgViewHeartOutline.setVisibility(View.VISIBLE);
//            holder.imgViewHeartOutline.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    mCallback.onHeartOutlineClicked(hotelLists[position].hotel_id, position);
//                }
//            });
//        }
    }

    @Override
    public int getItemCount() {
        return hotelLists.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{
        @BindView(R.id.img_view_room_list_poster)
        ImageView imgViewHotelPoster;
        @BindView(R.id.txt_view_address_room_list)
        TextView txtViewHotelAddress;
        @BindView(R.id.txt_view_room_list_hotel_name)
        TextView txtViewHotelName;
        @BindView(R.id.relative_layout_transparent)
        RelativeLayout transparentRelativeLayout;
        @BindView(R.id.txt_view_single_room_price)
        TextView txtViewSingleRoomPrice;
        @BindView(R.id.txt_view_double_room_price)
        TextView txtViewDoubleRoomPrice;
        @BindView(R.id.linear_layout_single_room)
        LinearLayout linearLayoutSingleRoom;
        @BindView(R.id.linear_layout_double_room)
        LinearLayout linearLayoutDoubleRoom;
//        @BindView(R.id.img_heart_outline)
//        ImageView imgViewHeartOutline;
//        @BindView(R.id.img_heart_fill)
//        ImageView imgViewHeartFill;
        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public void addItems(HotelList[] hotelLists){
        this.hotelLists = hotelLists;
    }
    public void updateItem(int position){
        hotelLists[position].isWishListPresent = !hotelLists[position].isWishListPresent;
        notifyItemChanged(position);
    }

}
