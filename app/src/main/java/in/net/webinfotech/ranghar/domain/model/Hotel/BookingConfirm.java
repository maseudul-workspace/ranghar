package in.net.webinfotech.ranghar.domain.model.Hotel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Raj on 16-05-2019.
 */

public class BookingConfirm {
    @SerializedName("booking_id")
    @Expose
    public int booking_id;

    @SerializedName("hotel_id")
    @Expose
    public int hotel_id;

    @SerializedName("amount_payable")
    @Expose
    public double amount_payable;

    @SerializedName("check_in")
    @Expose
    public String check_in;

    @SerializedName("check_out")
    @Expose
    public String check_out;

    @SerializedName("date")
    @Expose
    public String date;

    @SerializedName("status")
    @Expose
    public int status;

}
