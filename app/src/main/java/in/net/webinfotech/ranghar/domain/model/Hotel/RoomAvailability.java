package in.net.webinfotech.ranghar.domain.model.Hotel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Raj on 14-05-2019.
 */

public class RoomAvailability {
    @SerializedName("count_single_room")
    @Expose
    public int count_single_room;

    @SerializedName("count_double_room")
    @Expose
    public int count_double_room;
}
