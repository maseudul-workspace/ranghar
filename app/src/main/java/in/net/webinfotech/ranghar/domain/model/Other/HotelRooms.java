package in.net.webinfotech.ranghar.domain.model.Other;

/**
 * Created by Raj on 18-03-2019.
 */

public class HotelRooms {
    public int drawableId;
    public int roomType;

    public HotelRooms(int drawableId, int roomType) {
        this.drawableId = drawableId;
        this.roomType = roomType;
    }
}
