package in.net.webinfotech.ranghar.domain.interactor.impl;

import in.net.webinfotech.ranghar.domain.executor.Executor;
import in.net.webinfotech.ranghar.domain.executor.MainThread;
import in.net.webinfotech.ranghar.domain.interactor.HotelBookInteractor;
import in.net.webinfotech.ranghar.domain.interactor.base.AbstractInteractor;
import in.net.webinfotech.ranghar.domain.model.Hotel.BookingConfirm;
import in.net.webinfotech.ranghar.domain.model.Hotel.BookingConfirmWrapper;
import in.net.webinfotech.ranghar.repository.impl.HotelRepositoryImpl;

/**
 * Created by Raj on 15-05-2019.
 */

public class HotelBookInteractorImpl extends AbstractInteractor implements HotelBookInteractor {

    Callback mCallback;
    HotelRepositoryImpl mRepository;
    int userId;
    int hotelId;
    int quantityDouble;
    int quantitySingle;
    String checkIn;
    String checkOut;
    int adult;
    int child;
    int extra_person;
    String apiKey;

    public HotelBookInteractorImpl(Executor threadExecutor,
                                   MainThread mainThread,
                                   Callback callback,
                                   HotelRepositoryImpl repository,
                                   int userId,
                                   int hotelId,
                                   int quantityDouble,
                                   int quantitySingle,
                                   String checkIn,
                                   String checkOut,
                                   int adult,
                                   int child,
                                   int extra_person,
                                   String apiKey
                                                ) {
        super(threadExecutor, mainThread);
        this.mCallback = callback;
        this.mRepository = repository;
        this.userId = userId;
        this.hotelId = hotelId;
        this.quantityDouble = quantityDouble;
        this.quantitySingle = quantitySingle;
        this.checkIn = checkIn;
        this.checkOut = checkOut;
        this.adult = adult;
        this.child = child;
        this.extra_person = extra_person;
        this.apiKey = apiKey;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onBookingFail(errorMsg);
            }
        });
    }

    private void postMessage(final BookingConfirm bookingConfirm){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onBookingSuccess(bookingConfirm);
            }
        });
    }

    @Override
    public void run() {
        BookingConfirmWrapper bookingConfirmWrapper = mRepository.bookHotel(userId, hotelId, quantityDouble, quantitySingle, checkIn, checkOut, adult,child, extra_person, apiKey);
        if(bookingConfirmWrapper == null){
            notifyError("Something went wrong");
        }else if(!bookingConfirmWrapper.status){
            notifyError(bookingConfirmWrapper.message);
        }else{
            postMessage(bookingConfirmWrapper.bookingConfirm);
        }
    }
}
