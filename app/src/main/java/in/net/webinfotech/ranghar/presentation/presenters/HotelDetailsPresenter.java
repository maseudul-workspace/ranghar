package in.net.webinfotech.ranghar.presentation.presenters;

import in.net.webinfotech.ranghar.domain.model.Hotel.BookingConfirm;
import in.net.webinfotech.ranghar.domain.model.Hotel.HotelDetails;
import in.net.webinfotech.ranghar.presentation.presenters.base.BasePresenter;
import in.net.webinfotech.ranghar.presentation.ui.adapters.HotelImagesAdapter;

/**
 * Created by Raj on 16-04-2019.
 */

public interface HotelDetailsPresenter extends BasePresenter {
    void getHotelDetails(int hotelId);
    void addToWishList(int hotelId);
    void removeFromWishList(int hotelId);
    void checkHotelAvailability(int hotelId, int singleRoom, int doubleRoom, int adult, int child, int extraPerson);
    interface View{
        void loadHotelDetails(HotelDetails details, HotelImagesAdapter adapter);
        void onHotelImageClicked(String imageUrl);
        void onAddToWishListSuccess();
        void onDeleteFromWishListSuccess();
        void showProgressBar(String message);
        void hideProgressBar();
        void onBookingSuccess(BookingConfirm bookingConfirm);
    }
}
