package in.net.webinfotech.ranghar.domain.interactor;

/**
 * Created by Raj on 20-04-2019.
 */

public interface DeleteFromWishListInteractor {
    interface Callback{
        void onDeleteSuccess();
        void onDeleteFail(String errorMsg);
    }
}
