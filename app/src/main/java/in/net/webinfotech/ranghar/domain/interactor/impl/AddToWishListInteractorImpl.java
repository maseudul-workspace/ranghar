package in.net.webinfotech.ranghar.domain.interactor.impl;

import in.net.webinfotech.ranghar.domain.executor.Executor;
import in.net.webinfotech.ranghar.domain.executor.MainThread;
import in.net.webinfotech.ranghar.domain.interactor.AddToWishListInteractor;
import in.net.webinfotech.ranghar.domain.interactor.base.AbstractInteractor;
import in.net.webinfotech.ranghar.domain.model.WishList.AddToWishListResponse;
import in.net.webinfotech.ranghar.repository.WishListRepository;
import in.net.webinfotech.ranghar.repository.impl.WishListRepositoryImpl;

/**
 * Created by Raj on 20-04-2019.
 */

public class AddToWishListInteractorImpl extends AbstractInteractor implements AddToWishListInteractor {

    Callback mCallback;
    WishListRepositoryImpl mRepository;
    String apiKey;
    int userId;
    int hotelId;

    public AddToWishListInteractorImpl(Executor threadExecutor,
                                       MainThread mainThread,
                                       Callback callback,
                                       WishListRepositoryImpl repository,
                                       String apiKey,
                                       int userId,
                                       int hotelId
                                       ) {
        super(threadExecutor, mainThread);
        mCallback = callback;
        mRepository = repository;
        this.apiKey = apiKey;
        this.userId = userId;
        this.hotelId = hotelId;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onAddToWishListFail(errorMsg);
            }
        });
    }

    private void postMessage(){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onAddToWishListSuccess();
            }
        });
    }

    @Override
    public void run() {
        final AddToWishListResponse addToWishListResponse = mRepository.addToWishList(apiKey, userId, hotelId);
        if(addToWishListResponse == null){
            notifyError("Something went wrong");
        }else if(!addToWishListResponse.status){
            notifyError(addToWishListResponse.message);
        }else{
            postMessage();
        }
    }
}
