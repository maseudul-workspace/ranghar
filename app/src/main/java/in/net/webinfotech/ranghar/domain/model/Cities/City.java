package in.net.webinfotech.ranghar.domain.model.Cities;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Raj on 07-03-2019.
 */

public class City {
    @SerializedName("id")
    @Expose
    public int id;

    @SerializedName("city")
    @Expose
    public String city;

    @SerializedName("image")
    @Expose
    public String image;
}
