package in.net.webinfotech.ranghar.presentation.ui.activities;

import android.app.ActivityOptions;
import android.content.Intent;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Pair;
import android.util.Patterns;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import in.net.webinfotech.ranghar.R;
import in.net.webinfotech.ranghar.domain.executor.MainThread;
import in.net.webinfotech.ranghar.domain.executor.impl.ThreadExecutor;
import in.net.webinfotech.ranghar.presentation.presenters.LoginPresenter;
import in.net.webinfotech.ranghar.presentation.presenters.impl.LoginPresenterImpl;
import in.net.webinfotech.ranghar.presentation.routers.LoginRouter;
import in.net.webinfotech.ranghar.threading.MainThreadImpl;

public class LoginActivity extends AppCompatActivity implements LoginPresenter.View, LoginRouter {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.img_view_top_logo)
    ImageView imgViewLogo;
    @BindView(R.id.img_view_email)
    ImageView imgViewEmail;
    @BindView(R.id.img_view_password)
    ImageView imgViewPassword;
    @BindView(R.id.login_constraint_layout)
    ConstraintLayout loginConstraintLayout;
    @BindView(R.id.edit_text_email)
    EditText editTextEmail;
    @BindView(R.id.edit_text_password)
    EditText editTextPassword;
    LoginPresenterImpl mPresenter;
    @BindView(R.id.progressBar_layout)
    RelativeLayout progressBarLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.backwhiteicon);
        initialisePresenter();
    }

    public void initialisePresenter(){
        mPresenter = new LoginPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this, this);
    }

    @OnClick(R.id.txt_view_forget_password) void onForgetPasswordClicked(){
        Intent intent = new Intent(this, ForgetPasswordActivity.class);
        Pair[] pairs = new Pair[3];
        pairs[0] = new Pair<View, String> (imgViewLogo, "logoTransition");
        pairs[1] = new Pair<View, String> (imgViewEmail, "mailTransition");
        pairs[2] = new Pair<View, String> (imgViewPassword, "passwordTransition");
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(LoginActivity.this, pairs);
            startActivity(intent, options.toBundle());
        }else{
            startActivity(intent);
        }
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransitionExit();
    }

    @Override
    public void startActivity(Intent intent) {
        super.startActivity(intent);
        overridePendingTransitionEnter();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransitionExit();
    }

    protected void overridePendingTransitionExit() {
        overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right);
    }

    protected void overridePendingTransitionEnter() {
        overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
    }

    @OnClick(R.id.txt_view_sign_up) void onSignUpClicked(){
        Intent intent = new Intent(this, SignUpActivity.class);
        Pair[] pairs = new Pair[3];
        pairs[0] = new Pair<View, String> (imgViewLogo, "logoTransition");
        pairs[1] = new Pair<View, String> (imgViewEmail, "mailTransition");
        pairs[2] = new Pair<View, String> (imgViewPassword, "passwordTransition");
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(LoginActivity.this, pairs);
            startActivity(intent, options.toBundle());
        }else{
            startActivity(intent);
        }
    }

    @OnClick(R.id.btn_login) void onLoginBtnClicked(){
        if(editTextEmail.getText().toString().trim().isEmpty() ||
                editTextPassword.getText().toString().trim().isEmpty()
                )
        {
            Toast toast = Toast.makeText(this, "All fields are mandatory", Toast.LENGTH_SHORT);
            TextView v = (TextView) toast.getView().findViewById(android.R.id.message);
            v.setTextColor(getResources().getColor(R.color.appColor));
            toast.show();
        }else if(!Patterns.EMAIL_ADDRESS.matcher(editTextEmail.getText().toString()).matches()){
            Toast toast =Toast.makeText(this, "Please give a valid email", Toast.LENGTH_SHORT);
            TextView v = (TextView) toast.getView().findViewById(android.R.id.message);
            v.setTextColor(getResources().getColor(R.color.appColor));
            toast.show();
        }else{
            mPresenter.checkLogin(editTextEmail.getText().toString(), editTextPassword.getText().toString());
            showProgressBar();
        }
    }

    @Override
    public void showProgressBar() {
        progressBarLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgressBar() {
        progressBarLayout.setVisibility(View.GONE);
    }

    @Override
    public void goToMain() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
