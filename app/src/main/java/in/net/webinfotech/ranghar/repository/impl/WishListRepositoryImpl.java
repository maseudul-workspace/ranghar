package in.net.webinfotech.ranghar.repository.impl;

import android.util.Log;

import com.google.gson.Gson;

import in.net.webinfotech.ranghar.domain.model.WishList.AddToWishListResponse;
import in.net.webinfotech.ranghar.domain.model.WishList.DeleteFromWishListResponse;
import in.net.webinfotech.ranghar.domain.model.WishList.WishListWrapper;
import in.net.webinfotech.ranghar.repository.APIclient;
import in.net.webinfotech.ranghar.repository.WishListRepository;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by Raj on 20-04-2019.
 */

public class WishListRepositoryImpl {
    WishListRepository mRepository;

    public WishListRepositoryImpl() {
        mRepository = APIclient.createService(WishListRepository.class);
    }

    public AddToWishListResponse addToWishList(String apiKey, int userId, int hotelId){
        AddToWishListResponse addToWishListResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> add = mRepository.addToWishList(apiKey, userId, hotelId);

            Response<ResponseBody> response = add.execute();
            if(response.body() != null){
                responseBody = response.body().string();
                Log.e("ErrorMsg", responseBody);
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    addToWishListResponse = null;
                }else{
                    addToWishListResponse = gson.fromJson(responseBody, AddToWishListResponse.class);
                }
            } else {
                addToWishListResponse = null;
            }
        }catch (Exception e){
            Log.e("ErrorMsg", e.getMessage());
            addToWishListResponse = null;
        }
        return addToWishListResponse;
    }

    public WishListWrapper fetchWishList(String apiKey, int userId){
        WishListWrapper wishListWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> fetch = mRepository.fetchWishList(apiKey, userId);

            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
                Log.e("ErrorMsg", responseBody);
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    wishListWrapper = null;
                }else{
                    wishListWrapper = gson.fromJson(responseBody, WishListWrapper.class);
                }
            } else {
                wishListWrapper = null;
            }
        }catch (Exception e){
            Log.e("ErrorMsg", e.getMessage());
            wishListWrapper = null;
        }
        return wishListWrapper;
    }

    public DeleteFromWishListResponse deleteFromWishList(String apiKey, int userId, int wishListId){
        DeleteFromWishListResponse deleteFromWishListResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> delete = mRepository.deleteFromWishList(apiKey, userId, wishListId);

            Response<ResponseBody> response = delete.execute();
            if(response.body() != null){
                responseBody = response.body().string();
                Log.e("ErrorMsg", responseBody);
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    deleteFromWishListResponse = null;
                }else{
                    deleteFromWishListResponse = gson.fromJson(responseBody, DeleteFromWishListResponse.class);
                }
            } else {
                deleteFromWishListResponse = null;
            }
        }catch (Exception e){
            Log.e("ErrorMsg", e.getMessage());
            deleteFromWishListResponse = null;
        }
        return deleteFromWishListResponse;
    }

}
