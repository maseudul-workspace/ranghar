package in.net.webinfotech.ranghar.presentation.ui.activities;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.Toolbar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import in.net.webinfotech.ranghar.AndroidApplication;
import in.net.webinfotech.ranghar.R;
import in.net.webinfotech.ranghar.domain.executor.impl.ThreadExecutor;
import in.net.webinfotech.ranghar.domain.model.User.UserInfo;
import in.net.webinfotech.ranghar.presentation.presenters.UserDetailsPresenter;
import in.net.webinfotech.ranghar.presentation.presenters.impl.UserDetailsPresenterImpl;
import in.net.webinfotech.ranghar.threading.MainThreadImpl;

public class UserProfileActivity extends AppCompatActivity implements UserDetailsPresenter.View {

    @BindView(R.id.email_card_view)
    CardView emailCardView;
    @BindView(R.id.email_relative_layout)
    RelativeLayout emailRelativeLayout;
    @BindView(R.id.name_relative_layout)
    RelativeLayout nameRelativeLayout;
    @BindView(R.id.name_card_view)
    CardView nameCardView;
    @BindView(R.id.phone_relative_layout)
    RelativeLayout phoneRelativeLayout;
    @BindView(R.id.phone_card_view)
    CardView phoneCardView;
    @BindView(R.id.location_relative_layout)
    RelativeLayout locationRelativeLayout;
    @BindView(R.id.location_card_view)
    CardView locationCardView;
    @BindView(R.id.city_relative_layout)
    RelativeLayout cityRelativeLayout;
    @BindView(R.id.city_card_view)
    CardView cityCardView;
    @BindView(R.id.state_relative_layout)
    RelativeLayout stateRelativeLayout;
    @BindView(R.id.state_card_view)
    CardView stateCardView;
    @BindView(R.id.pin_relative_layout)
    RelativeLayout pinRelativeLayout;
    @BindView(R.id.pin_card_view)
    CardView pinCardView;
    @BindView(R.id.edit_text_email)
    EditText editTextEmail;
    @BindView(R.id.edit_text_phone)
    EditText editTextPhone;
    @BindView(R.id.edit_text_city)
    EditText editTextCity;
    @BindView(R.id.edit_text_location)
    EditText editTextLocation;
    @BindView(R.id.edit_text_state)
    EditText editTextState;
    @BindView(R.id.edit_text_pin)
    EditText editTextPin;
    @BindView(R.id.edit_text_name)
    EditText editTextName;
    @BindView(R.id.progressBar_layout)
    RelativeLayout progressBarLayout;
    @BindView(R.id.txt_view_name)
    TextView txtViewName;
    @BindView(R.id.txt_view_email)
    TextView txtViewEmail;
    @BindView(R.id.txt_view_phone)
    TextView txtViewPhone;
    @BindView(R.id.txt_view_city)
    TextView txtViewCityName;
    @BindView(R.id.txt_view_location)
    TextView txtViewLocation;
    @BindView(R.id.txt_view_state)
    TextView txtViewState;
    @BindView(R.id.txt_view_pin)
    TextView txtViewPin;
    @BindView(R.id.txt_progress_bar)
    TextView txtViewProgressBar;
    AndroidApplication androidApplication;
    UserDetailsPresenterImpl mPresenter;
    @BindView(R.id.toolbar)
    android.support.v7.widget.Toolbar toolbar;
    @BindView(R.id.bottom_navigation)
    BottomNavigationView bottomNavigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_profile);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.backiconblack);
        androidApplication = (AndroidApplication) getApplicationContext();
        initialisePresenter();
        initialiseBottomNavigation();
        mPresenter.fetchUserDetails();
        showProgressBar();
    }

    public void initialisePresenter(){
        mPresenter = new UserDetailsPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    public void initialiseBottomNavigation(){
        Menu menu = bottomNavigationView.getMenu();
        MenuItem menuItem = menu.getItem(3);
        menuItem.setChecked(true);
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()){
                    case R.id.action_home:
                        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                        startActivity(intent);
                        break;
                    case R.id.action_bookmarks:
                        if(androidApplication.getUserInfo(getApplicationContext()) != null){
                            Intent wishListIntent = new Intent(getApplicationContext(), WishListActivity.class);
                            startActivity(wishListIntent);
                        }else{
                            showLogInSnackbar();
                        }
                        break;
                    case R.id.action_booking:
//                        Intent intent1 = new Intent(getApplicationContext(), AboutActivity.class);
//                        startActivity(intent1);
                        break;
                }
                return false;
            }
        });
    }

    @OnClick(R.id.email_relative_layout) void onEmailRelativeLayoutClicked(){
        emailCardView.setVisibility(View.VISIBLE);
        emailRelativeLayout.setVisibility(View.GONE);
        requestFocus(editTextEmail);
        showKeyboard(editTextEmail);
    }

    @OnClick(R.id.name_relative_layout) void onNameRelativeLayoutClicked(){
        nameCardView.setVisibility(View.VISIBLE);
        nameRelativeLayout.setVisibility(View.GONE);
        requestFocus(editTextName);
        showKeyboard(editTextName);
    }

    @OnClick(R.id.phone_relative_layout) void onPhoneRelativeLayoutClicked(){
        phoneRelativeLayout.setVisibility(View.GONE);
        phoneCardView.setVisibility(View.VISIBLE);
        requestFocus(editTextPhone);
        showKeyboard(editTextPhone);
    }

    @OnClick(R.id.location_relative_layout) void onLocationRelativeLayoutClicked(){
        locationCardView.setVisibility(View.VISIBLE);
        locationRelativeLayout.setVisibility(View.GONE);
        requestFocus(editTextLocation);
        showKeyboard(editTextLocation);
    }

    @OnClick(R.id.city_relative_layout) void onCityRelativeLayoutClicked(){
        cityCardView.setVisibility(View.VISIBLE);
        cityRelativeLayout.setVisibility(View.GONE);
        requestFocus(editTextCity);
        showKeyboard(editTextCity);
    }

    @OnClick(R.id.state_relative_layout) void onStateRelativeLayoutClicked(){
        stateCardView.setVisibility(View.VISIBLE);
        stateRelativeLayout.setVisibility(View.GONE);
        requestFocus(editTextState);
        showKeyboard(editTextState);
    }

    @OnClick(R.id.pin_relative_layout) void onPinRelativeLayoutClicked(){
        pinCardView.setVisibility(View.VISIBLE);
        pinRelativeLayout.setVisibility(View.GONE);
        requestFocus(editTextPin);
        showKeyboard(editTextPin);
    }

    public void showKeyboard(View view){
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT);
    }

    public void requestFocus(View view){
        view.requestFocus();
        view.setFocusableInTouchMode(true);
    }

    @Override
    public void showProgressBar() {
        progressBarLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgressBar() {
        progressBarLayout.setVisibility(View.GONE);
    }

    @Override
    public void loadUserDetails(UserInfo userInfo) {
        if(userInfo.name != null){
            txtViewName.setText(userInfo.name);
            editTextName.setText(userInfo.name);
        }
        if(userInfo.email != null){
            txtViewEmail.setText(userInfo.email);
            editTextEmail.setText(userInfo.email);
        }
        if(userInfo.mobile != null){
            txtViewPhone.setText(userInfo.mobile);
            editTextPhone.setText(userInfo.mobile);
        }
        if(userInfo.address.location != null){
            txtViewLocation.setText(userInfo.address.location);
            editTextLocation.setText(userInfo.address.location);
        }
        if(userInfo.address.city != null){
            txtViewCityName.setText(userInfo.address.city);
            editTextCity.setText(userInfo.address.city);
        }
        if(userInfo.address.state != null){
            txtViewState.setText(userInfo.address.state);
            editTextState.setText(userInfo.address.state);
        }
        if(userInfo.address.pin != null){
            txtViewPin.setText(userInfo.address.pin);
            editTextPin.setText(userInfo.address.pin);
        }
    }

    @OnClick(R.id.user_update_btn) void updateUser(){
        androidApplication = (AndroidApplication) getApplicationContext();
        if(androidApplication.getUserInfo(this) != null){
            UserInfo userInfo = androidApplication.getUserInfo(this);
            mPresenter.updateUser(userInfo.api_key, userInfo.user_id,
                                    editTextName.getText().toString(), editTextPhone.getText().toString(), editTextEmail.getText().toString(),
                                    1, 1, 1, editTextPin.getText().toString() );
            txtViewProgressBar.setText("Updating user...");
            showProgressBar();
        }
    }

    @OnClick(R.id.img_view_logout) void logout(){
        androidApplication = (AndroidApplication) getApplicationContext();
        androidApplication.setUserInfo(this, null);
        Toast toast = Toast.makeText(this, "Logged out successfully", Toast.LENGTH_SHORT);
        TextView v = (TextView) toast.getView().findViewById(android.R.id.message);
        v.setTextColor(getResources().getColor(R.color.appColor));
        toast.show();
        goToMainActivtiy();
    }

    @OnClick(R.id.txt_view_reset_password) void onForgetPswdClicked(){
        goToPasswordChangeActivity();
    }

    void goToMainActivtiy(){
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    void goToPasswordChangeActivity(){
        Intent intent = new Intent(this, ResetPasswordActivity.class);
        startActivity(intent);
    }


    @Override
    public void finish() {
        super.finish();
        overridePendingTransitionExit();
    }

    @Override
    public void startActivity(Intent intent) {
        super.startActivity(intent);
        overridePendingTransitionEnter();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransitionExit();
    }

    protected void overridePendingTransitionExit() {
        overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right);
    }

    protected void overridePendingTransitionEnter() {
        overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void showLogInSnackbar(){
        Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content),"You must be logged in",Snackbar.LENGTH_LONG);
        snackbar.setAction("Log In", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent cartIntent = new Intent(getApplicationContext(), LoginActivity.class);
                startActivity(cartIntent);
            }
        });
        snackbar.show();
    }

}
