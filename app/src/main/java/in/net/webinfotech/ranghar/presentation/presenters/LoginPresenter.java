package in.net.webinfotech.ranghar.presentation.presenters;

import in.net.webinfotech.ranghar.presentation.presenters.base.BasePresenter;

/**
 * Created by Raj on 21-03-2019.
 */

public interface LoginPresenter extends BasePresenter {
    void checkLogin(String email, String password);
    interface View{
        void showProgressBar();
        void hideProgressBar();
    }
}
