package in.net.webinfotech.ranghar.presentation.presenters.impl;

import android.content.Context;
import android.widget.Toast;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import in.net.webinfotech.ranghar.AndroidApplication;
import in.net.webinfotech.ranghar.domain.executor.Executor;
import in.net.webinfotech.ranghar.domain.executor.MainThread;
import in.net.webinfotech.ranghar.domain.interactor.AddToWishListInteractor;
import in.net.webinfotech.ranghar.domain.interactor.HotelBookInteractor;
import in.net.webinfotech.ranghar.domain.interactor.DeleteFromWishListInteractor;
import in.net.webinfotech.ranghar.domain.interactor.FetchWishListInteractor;
import in.net.webinfotech.ranghar.domain.interactor.GetHotelDetailsInteractor;
import in.net.webinfotech.ranghar.domain.interactor.impl.AddToWishListInteractorImpl;
import in.net.webinfotech.ranghar.domain.interactor.impl.HotelBookInteractorImpl;
import in.net.webinfotech.ranghar.domain.interactor.impl.DeleteFromWishListInteractorImpl;
import in.net.webinfotech.ranghar.domain.interactor.impl.FetchWishListInteractorImpl;
import in.net.webinfotech.ranghar.domain.interactor.impl.GetHotelDetailsInteractorImpl;
import in.net.webinfotech.ranghar.domain.model.Hotel.BookingConfirm;
import in.net.webinfotech.ranghar.domain.model.Hotel.HotelDetails;
import in.net.webinfotech.ranghar.domain.model.User.UserInfo;
import in.net.webinfotech.ranghar.domain.model.WishList.WishList;
import in.net.webinfotech.ranghar.presentation.presenters.HotelDetailsPresenter;
import in.net.webinfotech.ranghar.presentation.presenters.base.AbstractPresenter;
import in.net.webinfotech.ranghar.presentation.ui.adapters.HotelImagesAdapter;
import in.net.webinfotech.ranghar.repository.impl.HotelRepositoryImpl;
import in.net.webinfotech.ranghar.repository.impl.WishListRepositoryImpl;

/**
 * Created by Raj on 16-04-2019.
 */

public class HotelDetailsPresenterImpl extends AbstractPresenter implements HotelDetailsPresenter,
                                                                            GetHotelDetailsInteractor.Callback,
                                                                            HotelImagesAdapter.Callback,
                                                                            AddToWishListInteractor.Callback,
                                                                            DeleteFromWishListInteractor.Callback,
                                                                            FetchWishListInteractor.Callback,
                                                                            HotelBookInteractor.Callback
{

    Context mContext;
    HotelDetailsPresenter.View mView;
    GetHotelDetailsInteractorImpl mInteractor;
    HotelImagesAdapter adapter;
    AddToWishListInteractorImpl addToWishListInteractor;
    DeleteFromWishListInteractorImpl deleteFromWishListInteractor;
    FetchWishListInteractorImpl fetchWishListInteractor;
    AndroidApplication androidApplication;
    HotelBookInteractorImpl hotelBookInteractor;

    public HotelDetailsPresenterImpl(Executor executor,
                                     MainThread mainThread,
                                     Context context,
                                     HotelDetailsPresenter.View view
                                     ) {
        super(executor, mainThread);
        this.mContext = context;
        this.mView = view;
    }

    @Override
    public void getHotelDetails(int hotelId) {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        String checkInDate = changeDateFormat(androidApplication.checkInDate);
        String checkOutDate = changeDateFormat(androidApplication.checkOutDate);
        mInteractor = new GetHotelDetailsInteractorImpl(mExecutor, mMainThread, this, new HotelRepositoryImpl(), hotelId, checkInDate, checkOutDate);
        mInteractor.execute();
    }

    @Override
    public void addToWishList(int hotelId) {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        addToWishListInteractor = new AddToWishListInteractorImpl(mExecutor, mMainThread, this, new WishListRepositoryImpl(), userInfo.api_key, userInfo.user_id, hotelId);
        addToWishListInteractor.execute();
    }

    @Override
    public void removeFromWishList(int hotelId) {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        WishList[] wishLists = androidApplication.getWishLists();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        int wishListId = 0;
        if(wishLists != null && wishLists.length > 0){
            for(int i = 0; i<wishLists.length; i++){
                if(wishLists[i].hotelId == hotelId){
                    wishListId = wishLists[i].wishListId;
                    break;
                }
            }
            deleteFromWishListInteractor = new DeleteFromWishListInteractorImpl(mExecutor, mMainThread, this, new WishListRepositoryImpl(), userInfo.api_key, userInfo.user_id, wishListId);
            deleteFromWishListInteractor.execute();
        }
    }

    @Override
    public void checkHotelAvailability(int hotelId, int singleRoom, int doubleRoom, int adult, int child, int extraPerson) {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        String checkInDate = changeDateFormat(androidApplication.checkInDate);
        String checkOutDate = changeDateFormat(androidApplication.checkOutDate);
        hotelBookInteractor = new HotelBookInteractorImpl(mExecutor, mMainThread, this, new HotelRepositoryImpl(), userInfo.user_id, hotelId, doubleRoom, singleRoom, checkInDate, checkOutDate, adult, child, extraPerson, userInfo.api_key );
        hotelBookInteractor.execute();
    }

    public void getWishList(){
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        fetchWishListInteractor = new FetchWishListInteractorImpl(mExecutor, mMainThread, this, new WishListRepositoryImpl(), userInfo.api_key, userInfo.user_id);
        fetchWishListInteractor.execute();
    }

    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void stop() {

    }

    @Override
    public void destroy() {

    }

    @Override
    public void onGettingHotelDetailsSuccess(HotelDetails hotelDetails) {
        adapter = new HotelImagesAdapter(hotelDetails.hotelImages, mContext, this);
        mView.loadHotelDetails(hotelDetails, adapter);
        mView.hideProgressBar();
    }

    @Override
    public void onGettingHotelDetailsFail(String errorMsg) {
        mView.hideProgressBar();
        Toast.makeText(mContext, errorMsg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onImageClicked(String imageUrl) {
        mView.onHotelImageClicked(imageUrl);
    }

    @Override
    public void onAddToWishListSuccess() {
        Toast.makeText(mContext, "Added to wishlist", Toast.LENGTH_SHORT).show();
        mView.onAddToWishListSuccess();
        getWishList();

    }

    @Override
    public void onAddToWishListFail(String errorMsg) {
        Toast.makeText(mContext, errorMsg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onDeleteSuccess() {
        Toast.makeText(mContext, "Deleted from wishlist", Toast.LENGTH_SHORT).show();
        mView.onDeleteFromWishListSuccess();
        getWishList();
    }

    @Override
    public void onDeleteFail(String errorMsg) {
        Toast.makeText(mContext, errorMsg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onGettingWishListSuccess(WishList[] wishLists) {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        androidApplication.setWishLists(wishLists);
    }

    @Override
    public void onGettingWishListFail(String errorMsg) {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        androidApplication.setWishLists(null);
    }

    public String changeDateFormat(String dateString){
        SimpleDateFormat dateFormat = new SimpleDateFormat("MMM dd, yyyy");
        Date sourceDate = null;
        try {
            sourceDate = dateFormat.parse(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        SimpleDateFormat targetFormat = new SimpleDateFormat("yyyy-MM-dd");
        return targetFormat.format(sourceDate);
    }


    @Override
    public void onBookingSuccess(BookingConfirm bookingConfirm) {
        mView.hideProgressBar();
        mView.onBookingSuccess(bookingConfirm);
    }

    @Override
    public void onBookingFail(String errorMsg) {
        mView.hideProgressBar();
        Toast.makeText(mContext, errorMsg, Toast.LENGTH_SHORT).show();
    }
}
