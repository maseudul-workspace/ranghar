package in.net.webinfotech.ranghar.presentation.presenters;

import in.net.webinfotech.ranghar.presentation.presenters.base.BasePresenter;

/**
 * Created by Raj on 21-03-2019.
 */

public interface SignUpPresenter extends BasePresenter {
    void register(String name, String mobile, String email, String password);
    interface View{
        void showProgressBar();
        void hideProgressBar();
    }
}
