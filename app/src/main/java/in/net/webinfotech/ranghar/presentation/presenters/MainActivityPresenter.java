package in.net.webinfotech.ranghar.presentation.presenters;

import in.net.webinfotech.ranghar.presentation.presenters.base.BasePresenter;
import in.net.webinfotech.ranghar.presentation.ui.adapters.CitiesAdapter;

/**
 * Created by Raj on 07-03-2019.
 */

public interface MainActivityPresenter extends BasePresenter {
    void getCities();
    void getPosterImage();
    void fetchWishLists(String apiKey, int userId);
    interface View{
        void loadAdapter(CitiesAdapter adapter);
        void onCityClicked(int position, String cityName, int cityId);
        void loadPosterImage(String imageUrl);
    }
}
