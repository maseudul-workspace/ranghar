package in.net.webinfotech.ranghar.presentation.ui.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.net.webinfotech.ranghar.R;
import in.net.webinfotech.ranghar.domain.model.WishList.WishList;
import in.net.webinfotech.ranghar.util.GlideHelper;

/**
 * Created by Raj on 22-04-2019.
 */

public class WishListAdapter extends RecyclerView.Adapter<WishListAdapter.ViewHolder>{

    public interface Callback{
        void onHotelClicked(int hotelId);
        void onHeartFillClicked(int wishListId, int position);
        void onHeartOutlineClicked(int hotelId, int position);
    }

    Context mContext;
    WishList[] wishLists;
    Callback mCallback;

    public WishListAdapter(Context mContext, WishList[] wishLists, Callback callback) {
        this.mContext = mContext;
        this.wishLists = wishLists;
        mCallback = callback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_hotel_list, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        holder.linearLayoutSingleRoom.setVisibility(View.GONE);
        holder.linearLayoutDoubleRoom.setVisibility(View.GONE);
        GlideHelper.setImageView(mContext, holder.imgViewHotelPoster, mContext.getResources().getString(R.string.base_url) + "uploads/hotel_image/thumb/" + wishLists[position].image);
        holder.txtViewHotelName.setText(wishLists[position].hotelName);
        holder.txtViewHotelAddress.setText(wishLists[position].state + ", " + wishLists[position].city + ", " + wishLists[position].area);
//        if(wishLists[position].isWishListPresent){
//            holder.imgViewHeartFill.setVisibility(View.VISIBLE);
//        }else{
//            holder.imgViewHeartOutline.setVisibility(View.VISIBLE);
//        }
//        holder.imgViewHeartFill.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                mCallback.onHeartFillClicked(wishLists[position].wishListId, position);
//            }
//        });
//        holder.imgViewHeartOutline.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                mCallback.onHeartOutlineClicked(wishLists[position].hotelId, position);
//            }
//        });
        holder.imgViewHotelPoster.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.onHotelClicked(wishLists[position].hotelId);
            }
        });

    }

    @Override
    public int getItemCount() {
        return wishLists.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{
        @BindView(R.id.img_view_room_list_poster)
        ImageView imgViewHotelPoster;
        @BindView(R.id.txt_view_address_room_list)
        TextView txtViewHotelAddress;
        @BindView(R.id.txt_view_room_list_hotel_name)
        TextView txtViewHotelName;
        @BindView(R.id.relative_layout_transparent)
        RelativeLayout transparentRelativeLayout;
        @BindView(R.id.txt_view_single_room_price)
        TextView txtViewSingleRoomPrice;
        @BindView(R.id.txt_view_double_room_price)
        TextView txtViewDoubleRoomPrice;
        @BindView(R.id.linear_layout_single_room)
        LinearLayout linearLayoutSingleRoom;
        @BindView(R.id.linear_layout_double_room)
        LinearLayout linearLayoutDoubleRoom;
        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public void updateWishList(int position){
        this.wishLists[position].isWishListPresent = !this.wishLists[position].isWishListPresent;
        notifyItemChanged(position);
    }

}
