package in.net.webinfotech.ranghar.domain.interactor;

import in.net.webinfotech.ranghar.domain.model.WishList.WishList;

/**
 * Created by Raj on 20-04-2019.
 */

public interface FetchWishListInteractor {
    interface Callback{
        void onGettingWishListSuccess(WishList[] wishLists);
        void onGettingWishListFail(String errorMsg);
    }
}
