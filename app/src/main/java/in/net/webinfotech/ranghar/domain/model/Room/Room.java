package in.net.webinfotech.ranghar.domain.model.Room;

/**
 * Created by Raj on 12-03-2019.
 */

public class Room {
    public int adults;
    public int child;

    public Room(int adults, int child) {
        this.adults = adults;
        this.child = child;
    }
}
