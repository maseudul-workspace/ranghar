package in.net.webinfotech.ranghar.presentation.presenters.base;

import in.net.webinfotech.ranghar.domain.executor.Executor;
import in.net.webinfotech.ranghar.domain.executor.MainThread;

/**
 * Created by Raj on 07-03-2019.
 */

public abstract class AbstractPresenter {
    protected Executor mExecutor;
    protected MainThread mMainThread;
    public AbstractPresenter(Executor executor, MainThread mainThread){
        this.mExecutor = executor;
        this.mMainThread = mainThread;
    }
}
