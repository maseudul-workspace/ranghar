package in.net.webinfotech.ranghar.domain.model.Hotel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Raj on 12-04-2019.
 */

public class HotelDetails {
    @SerializedName("hotel_id")
    @Expose
    public int hotel_id;

    @SerializedName("hotel_name")
    @Expose
    public String hotel_name;

    @SerializedName("state")
    @Expose
    public String state;

    @SerializedName("city")
    @Expose
    public String city;

    @SerializedName("area")
    @Expose
    public String area;

    @SerializedName("pin")
    @Expose
    public String pin;

    @SerializedName("star")
    @Expose
    public int star;

    @SerializedName("image")
    @Expose
    public String description;

    @SerializedName("check_in")
    @Expose
    public String check_in;

    @SerializedName("check_out")
    @Expose
    public String check_out;

    @SerializedName("is_24_hour_check_in")
    @Expose
    public int is_24_hour_check_in;

    @SerializedName("main_image")
    @Expose
    public String main_image;

    @SerializedName("gym")
    @Expose
    public int gym;

    @SerializedName("free_internet")
    @Expose
    public int freeInternet;

    @SerializedName("room_service")
    @Expose
    public int roomService;

    @SerializedName("resturant")
    @Expose
    public int restaurant;

    @SerializedName("bar")
    @Expose
    public int bar;

    @SerializedName("ac")
    @Expose
    public int ac;

    @SerializedName("parking")
    @Expose
    public int parking;

    @SerializedName("swimming_pool")
    @Expose
    public int swimmingPool;

    @SerializedName("tv")
    @Expose
    public int tv;

    @SerializedName("single_room_quantity")
    @Expose
    public int single_room_quantity;

    @SerializedName("single_room_price")
    @Expose
    public double single_room_price;

    @SerializedName("double_room_quantity")
    @Expose
    public int double_room_quantity;

    @SerializedName("double_room_price")
    @Expose
    public double double_room_price;

    @SerializedName("is_extra_person")
    @Expose
    public int is_extra_person;

    @SerializedName("extra_person_price")
    @Expose
    public int extra_person_price;

    @SerializedName("images")
    @Expose
    public HotelImages[] hotelImages;

    @SerializedName("availiblity")
    @Expose
    public RoomAvailability availability;

    public boolean isWishListPresent = false;


}
