package in.net.webinfotech.ranghar.domain.model.User;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import butterknife.BindView;

/**
 * Created by Raj on 21-03-2019.
 */

public class UserInfo {
    @SerializedName("user_id")
    @Expose
    public int user_id;

    @SerializedName("api_key")
    @Expose
    public String api_key;

    @SerializedName("name")
    @Expose
    public String name;

    @SerializedName("mobile")
    @Expose
    public String mobile;

    @SerializedName("email")
    @Expose
    public String email;

    @SerializedName("address")
    @Expose
    public Address address;

}
