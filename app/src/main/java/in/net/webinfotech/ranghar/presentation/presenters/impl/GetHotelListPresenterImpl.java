package in.net.webinfotech.ranghar.presentation.presenters.impl;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import in.net.webinfotech.ranghar.AndroidApplication;
import in.net.webinfotech.ranghar.domain.executor.Executor;
import in.net.webinfotech.ranghar.domain.executor.MainThread;
import in.net.webinfotech.ranghar.domain.interactor.AddToWishListInteractor;
import in.net.webinfotech.ranghar.domain.interactor.DeleteFromWishListInteractor;
import in.net.webinfotech.ranghar.domain.interactor.FetchWishListInteractor;
import in.net.webinfotech.ranghar.domain.interactor.GetHotelListInteractor;
import in.net.webinfotech.ranghar.domain.interactor.impl.AddToWishListInteractorImpl;
import in.net.webinfotech.ranghar.domain.interactor.impl.DeleteFromWishListInteractorImpl;
import in.net.webinfotech.ranghar.domain.interactor.impl.FetchWishListInteractorImpl;
import in.net.webinfotech.ranghar.domain.interactor.impl.GetHotelListInteractorImpl;
import in.net.webinfotech.ranghar.domain.model.Hotel.HotelList;
import in.net.webinfotech.ranghar.domain.model.Room.Room;
import in.net.webinfotech.ranghar.domain.model.User.UserInfo;
import in.net.webinfotech.ranghar.domain.model.WishList.WishList;
import in.net.webinfotech.ranghar.presentation.presenters.GetHotelListPresenter;
import in.net.webinfotech.ranghar.presentation.presenters.base.AbstractPresenter;
import in.net.webinfotech.ranghar.presentation.routers.HotelListRouter;
import in.net.webinfotech.ranghar.presentation.ui.adapters.HotelListAdapter;
import in.net.webinfotech.ranghar.repository.impl.HotelRepositoryImpl;
import in.net.webinfotech.ranghar.repository.impl.WishListRepositoryImpl;

/**
 * Created by Raj on 15-03-2019.
 */

public class GetHotelListPresenterImpl extends AbstractPresenter implements GetHotelListPresenter,
                                                                                GetHotelListInteractor.Callback,
                                                                                HotelListAdapter.Callback,
                                                                                AddToWishListInteractor.Callback,
                                                                                FetchWishListInteractor.Callback,
                                                                                DeleteFromWishListInteractor.Callback
{

    Context mContext;
    GetHotelListInteractorImpl mInteractor;
    GetHotelListPresenter.View mView;
    String currentSearchKey;
    HotelListAdapter hotelListAdapter;
    HotelList[] newHotelLists;
    HotelListRouter mRouter;
    AndroidApplication androidApplication;
    AddToWishListInteractorImpl addToWishListInteractor;
    FetchWishListInteractorImpl fetchWishListInteractor;
    DeleteFromWishListInteractorImpl deleteFromWishListInteractor;
    int position;
    int singleRoom = 0;
    int doubleRoom = 0;

    public GetHotelListPresenterImpl(Executor executor,
                                     MainThread mainThread,
                                     Context context,
                                     GetHotelListPresenter.View view,
                                     HotelListRouter router
                                       ) {
        super(executor, mainThread);
        this.mView = view;
        this.mContext = context;
        mRouter = router;
    }

    @Override
    public void getHotelList(String searchKey, int pageNo, int searchType, String checkInDate, String checkOutDate, String type) {
        if(type.equals("refresh")){
            newHotelLists = null;
            calculateRooms();
        }
        checkInDate = changeDateFormat(checkInDate);
        checkOutDate = changeDateFormat(checkOutDate);
        this.currentSearchKey = searchKey;
        mInteractor = new GetHotelListInteractorImpl(mExecutor, mMainThread, this, new HotelRepositoryImpl(), searchKey, pageNo, searchType, checkInDate, checkOutDate);
        mInteractor.execute();
    }

    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void stop() {

    }

    @Override
    public void destroy() {

    }

    @Override
    public void onGettingHotelListSuccess(HotelList[] hotelLists, String searchKey, int totalPage) {
        if(this.currentSearchKey.equals(searchKey)){
            if(hotelLists.length == 0){
                hotelListAdapter = new HotelListAdapter(mContext, hotelLists, this);
                mView.loadAdapter(hotelListAdapter, totalPage);
            }else{
                androidApplication = (AndroidApplication) mContext.getApplicationContext();
                WishList[] wishLists = androidApplication.getWishLists();
                if(wishLists != null && wishLists.length > 0){
                    for(int i = 0; i < hotelLists.length; i++){
                        for(int j = 0; j < wishLists.length; j++){
                            if(hotelLists[i].hotel_id == wishLists[j].hotelId){
                                hotelLists[i].isWishListPresent = true;
                                break;
                            }
                        }
                    }
                }
                for(int i = 0; i < hotelLists.length; i++){
                    if(checkRoomAvailability(hotelLists[i].availability.count_single_room, hotelLists[i].availability.count_double_room)){
                        hotelLists[i].isRoomAvailable = true;
                    }else{
                        hotelLists[i].isRoomAvailable = false;
                    }
                }
                HotelList[] tempHotelList;
                tempHotelList = newHotelLists;
                try {
                    int len1 = tempHotelList.length;
                    int len2 = hotelLists.length;
                    newHotelLists = new HotelList[len1 + len2];
                    System.arraycopy(tempHotelList, 0, newHotelLists, 0, len1);
                    System.arraycopy(hotelLists, 0, newHotelLists, len1, len2);
                    hotelListAdapter.addItems(newHotelLists);
                    hotelListAdapter.notifyDataSetChanged();
                    mView.hidePaginationProgressBar();
                } catch (NullPointerException e) {
                    newHotelLists = hotelLists;
                    hotelListAdapter = new HotelListAdapter(mContext, hotelLists, this);
                    mView.loadAdapter(hotelListAdapter, totalPage);
                    mView.hideShimmerAnimation();
                }
            }
        }
    }

    @Override
    public void onGettingHotelListFail(String errorMsg) {
        newHotelLists = null;
        mView.hidePaginationProgressBar();
        mView.hideShimmerAnimation();
    }

    @Override
    public void onHotelClicked(int id) {
        mRouter.goToHotelDetails(id);
    }

    @Override
    public void onHeartOutlineClicked(int id, int position) {
        this.position = position;
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        addToWishListInteractor = new AddToWishListInteractorImpl(mExecutor, mMainThread, this,
                                                                    new WishListRepositoryImpl(), userInfo.api_key, userInfo.user_id, id);
        addToWishListInteractor.execute();
    }

    @Override
    public void onHeartFillClicked(int id, int position) {
        this.position = position;
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        WishList[] wishLists = androidApplication.getWishLists();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        int wishListId = 0;
        if(wishLists != null && wishLists.length > 0){
            for(int i = 0; i<wishLists.length; i++){
                if(wishLists[i].hotelId == id){
                    wishListId = wishLists[i].wishListId;
                    break;
                }
            }
            deleteFromWishListInteractor = new DeleteFromWishListInteractorImpl(mExecutor, mMainThread, this, new WishListRepositoryImpl(), userInfo.api_key, userInfo.user_id, wishListId);
            deleteFromWishListInteractor.execute();
        }
    }

    @Override
    public void onAddToWishListSuccess() {
        hotelListAdapter.updateItem(this.position);
        Toast.makeText(mContext, "Successfully added to your wishlist", Toast.LENGTH_SHORT).show();
        getWishList();
    }

    @Override
    public void onAddToWishListFail(String errorMsg) {
        Toast.makeText(mContext, errorMsg, Toast.LENGTH_SHORT).show();
    }

    public void getWishList(){
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        fetchWishListInteractor = new FetchWishListInteractorImpl(mExecutor, mMainThread, this, new WishListRepositoryImpl(), userInfo.api_key, userInfo.user_id);
        fetchWishListInteractor.execute();
    }

    @Override
    public void onGettingWishListSuccess(WishList[] wishLists) {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        androidApplication.setWishLists(wishLists);
    }

    @Override
    public void onGettingWishListFail(String errorMsg) {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        androidApplication.setWishLists(null);
    }

    @Override
    public void onDeleteSuccess() {
        hotelListAdapter.updateItem(this.position);
        Toast.makeText(mContext, "Removed from wishlist", Toast.LENGTH_SHORT).show();
        getWishList();
    }

    @Override
    public void onDeleteFail(String errorMsg) {
        Toast.makeText(mContext, errorMsg, Toast.LENGTH_SHORT).show();
    }

    public void calculateRooms(){
        singleRoom = 0;
        doubleRoom = 0;
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        ArrayList<Room> rooms = androidApplication.getRooms();
        for(int i = 0; i < rooms.size(); i++){

            if(rooms.get(i).adults + rooms.get(i).child == 4){
                doubleRoom++;
            }else{
                switch (rooms.get(i).adults){
                    case 3:
                        doubleRoom++;
                        break;
                    case 2:
                        doubleRoom++;
                        break;
                    case 1:
                        if(rooms.get(i).adults + rooms.get(i).child > 2){
                            doubleRoom++;
                        }else{
                            singleRoom++;
                        }
                        break;
                }
            }
        }
    }

    public boolean checkRoomAvailability(int totalSingleRoom, int totalDoubleRoom){
        if(doubleRoom > totalDoubleRoom){
            return false;
        }else if(singleRoom  > totalSingleRoom){
            if(totalSingleRoom == 0){
                if(singleRoom + doubleRoom > totalDoubleRoom){
                    return false;
                }else{
                    return true;
                }
            }else{
                int remaining_single_room = singleRoom - totalSingleRoom;
                singleRoom = totalSingleRoom;
                if(remaining_single_room + doubleRoom > totalDoubleRoom){
                    return false;
                }else{
                    return true;
                }
            }
        }else{
            return true;
        }
    }

    public String changeDateFormat(String dateString){
        SimpleDateFormat dateFormat = new SimpleDateFormat("MMM dd, yyyy");
        Date sourceDate = null;
        try {
            sourceDate = dateFormat.parse(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        SimpleDateFormat targetFormat = new SimpleDateFormat("yyyy-MM-dd");
        return targetFormat.format(sourceDate);
    }

}
