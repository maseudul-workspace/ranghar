package in.net.webinfotech.ranghar.presentation.ui.activities;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Rect;
import android.location.Address;
import android.location.Geocoder;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import in.net.webinfotech.ranghar.AndroidApplication;
import in.net.webinfotech.ranghar.R;
import in.net.webinfotech.ranghar.domain.executor.impl.ThreadExecutor;
import in.net.webinfotech.ranghar.domain.model.Hotel.BookingConfirm;
import in.net.webinfotech.ranghar.domain.model.Hotel.HotelDetails;
import in.net.webinfotech.ranghar.domain.model.Hotel.HotelFeatures;
import in.net.webinfotech.ranghar.domain.model.Room.Room;
import in.net.webinfotech.ranghar.domain.model.WishList.WishList;
import in.net.webinfotech.ranghar.presentation.presenters.HotelDetailsPresenter;
import in.net.webinfotech.ranghar.presentation.presenters.impl.HotelDetailsPresenterImpl;
import in.net.webinfotech.ranghar.presentation.ui.adapters.HotelFeaturesAdapter;
import in.net.webinfotech.ranghar.presentation.ui.adapters.HotelImagesAdapter;
import in.net.webinfotech.ranghar.presentation.ui.dialogs.BookingConfirmationDialog;
import in.net.webinfotech.ranghar.presentation.ui.dialogs.BookingSuccessDialog;
import in.net.webinfotech.ranghar.presentation.ui.dialogs.DatePickerFragment;
import in.net.webinfotech.ranghar.presentation.ui.dialogs.RoomSelectDialog;
import in.net.webinfotech.ranghar.threading.MainThreadImpl;

public class HotelDetailsActivity extends AppCompatActivity implements HotelDetailsPresenter.View,
                                                                        RoomSelectDialog.Callback,
                                                                        DatePickerDialog.OnDateSetListener,
                                                                        BookingConfirmationDialog.Callback,
                                                                        BookingSuccessDialog.Callback
{

    private BottomSheetBehavior bottomSheetBehavior;
    @BindView(R.id.scroll_view_bottom_sheet)
    View bottomSheet;
    @BindView(R.id.coordinatorLayout)
    View coordinatorLayout;
    @BindView(R.id.btn_book)
    Button btnBook;
    @BindView(R.id.txt_view_hotel_name)
    TextView txtViewHotelName;
    @BindView(R.id.txt_view_hotel_address)
    TextView txtViewHotelAddress;
    @BindView(R.id.hotel_details_rating_bar)
    RatingBar ratingBar;
    @BindView(R.id.txt_view_check_in_status)
    TextView txtViewCheckInStatus;
    @BindView(R.id.txt_view_check_in_time)
    TextView txtViewCheckInTime;
    @BindView(R.id.txt_view_check_out_time)
    TextView txtViewCheckOutTime;
    @BindView(R.id.txt_view_room_status)
    TextView txtViewRoomStatus;
    @BindView(R.id.txt_view_person_count)
    TextView txtViewPersonCount;
    @BindView(R.id.txt_view_total_price)
    TextView txtViewTotalPrice;

    @BindView(R.id.linear_layout_hotel_check_in)
    View checkInLinearLayout;
    @BindView(R.id.recycler_view_hotel_images)
    RecyclerView recyclerView;
    @BindView(R.id.img_view_heart_outline)
    ImageView imgViewHeartOutline;
    @BindView(R.id.img_view_heart_fill)
    ImageView imgViewHeartFill;
    @BindView(R.id.recycler_view_hotel_features)
    RecyclerView recyclerViewHotelFeatures;
    HotelDetailsPresenterImpl mPresenter;
    String state;
    String city;
    String area;
    AndroidApplication androidApplication;
    ArrayList<Room> rooms;
    int doubleRoom = 0;
    int extraPerson = 0;
    int singleRoom = 0;
    int totalDoubleRoom = 0;
    int totalSingleRoom = 0;
    double singleRoomPrice = 0;
    double doubleRoomPrice = 0;
    double extraPersonPrice = 0;
    int totalAdultCount = 0;
    int totalChildCount = 0;
    RoomSelectDialog roomSelectDialog;
    WishList[] wishLists;
    int hotelId;
    boolean canExtraPersonAdd;
    RecyclerView.LayoutManager gridLayoutManager;
    ArrayList<HotelFeatures> hotelFeatures;
    HotelDetails hotelDetails;
    String checkInDate;
    String checkOutDate;
    Boolean isCheckIn;
    android.support.v4.app.DialogFragment dialogFragment;
    @BindView(R.id.txt_view_check_in_date)
    TextView txtViewCheckInDate;
    @BindView(R.id.txt_view_check_out_date)
    TextView txtViewCheckOutDate;
    @BindView(R.id.progressBar_layout)
    View progressBarLayout;
    BookingConfirmationDialog bookingConfirmationDialog;
    String personCount;
    String roomStatus;
    Double totalPrice;
    @BindView(R.id.txt_progress_bar)
    TextView txtProgressBar;
    BookingSuccessDialog bookingSuccessDialog;
    Double totalPayableAmount;
    int bookingId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hotel_details);
        ButterKnife.bind(this);
        setBottomSheetHeight();
        setBottomSheet();
        initialisePresenter();
        initialiseRoomSelectDialog();
        initialiseBookingConfirmationDialog();
        initialiseBookingSuccessDialog();
        roomSelectDialog.setUpDialogView();
        int hotelId = getIntent().getIntExtra("hotelId", 1);
        androidApplication = (AndroidApplication) getApplicationContext();
        wishLists = androidApplication.getWishLists();
        showProgressBar("Fetching hotel details");
        mPresenter.getHotelDetails(hotelId);
        setupDates();
    }

    public void initialisePresenter(){
        mPresenter = new HotelDetailsPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    public void initialiseRoomSelectDialog(){
        roomSelectDialog = new RoomSelectDialog(this, this, this);
    }

    public void initialiseBookingConfirmationDialog(){
        bookingConfirmationDialog = new BookingConfirmationDialog(this, this, this);
    }

    public void initialiseBookingSuccessDialog(){
        bookingSuccessDialog = new BookingSuccessDialog(this, this, this);
    }

    public void setBottomSheet(){
        bottomSheetBehavior = BottomSheetBehavior.from(bottomSheet);
        bottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
               switch (newState){
                   case BottomSheetBehavior.STATE_HIDDEN:
                       btnBook.setVisibility(View.GONE);
                       break;
                   case BottomSheetBehavior.STATE_COLLAPSED:
                       btnBook.setVisibility(View.VISIBLE);
                       break;
               }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {

            }
        });
    }

    public void setBottomSheetHeight(){
        ViewTreeObserver vto = coordinatorLayout.getViewTreeObserver();
        vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                // calculate the height in here...
                int peekHeigth = (int) (coordinatorLayout.getHeight() - 250);
                bottomSheetBehavior.setPeekHeight(peekHeigth);
                ViewTreeObserver vto = coordinatorLayout.getViewTreeObserver();
                vto.removeGlobalOnLayoutListener(this);
            }
        });
    }

    @OnClick(R.id.cancel_img_view) void onCancelClicked(){
        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
    }

//    @OnClick(R.id.img_view_main) void onMainImageClicked(){
//        Log.e("ErrorMsg", "bottom sheet state: " + bottomSheetBehavior.getState());
//        if(bottomSheetBehavior.getState() == BottomSheetBehavior.STATE_COLLAPSED){
//            bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
//        }
//    }

    @Override
    public void loadHotelDetails(HotelDetails details, HotelImagesAdapter adapter) {

        hotelDetails = details;
        hotelId = details.hotel_id;
        txtViewHotelName.setText(details.hotel_name);

//        Setting hotel rating
        ratingBar.setNumStars(details.star);
        ratingBar.setRating(details.star);

//        24 hours check in availability
        if(details.is_24_hour_check_in == 2){
            txtViewCheckInStatus.setVisibility(View.VISIBLE);
            txtViewCheckInStatus.setText("24 hours check is available");
            checkInLinearLayout.setVisibility(View.GONE);
        }else{
            checkInLinearLayout.setVisibility(View.VISIBLE);
            txtViewCheckInTime.setText(details.check_in);
            txtViewCheckOutTime.setText(details.check_out);
        }

//        Setting the hotel images adapter
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

//        Setting the hotel location
        state = details.state;
        city = details.city;
        area = details.area;
        String address = details.state + ", " + details.city + ", " + details.area + ", " + details.pin;
        txtViewHotelAddress.setText(address);

//        Setting rooms quantity
        totalSingleRoom = details.availability.count_single_room;
        singleRoomPrice = details.single_room_price;
        totalDoubleRoom = details.availability.count_double_room;
        doubleRoomPrice = details.double_room_price;

//        Checking extra person availability
        if(details.is_extra_person == 1){
            canExtraPersonAdd = true;
            extraPersonPrice = details.extra_person_price;
        }else{
            canExtraPersonAdd = false;
            extraPersonPrice = 0;
        }

//        Checking wishlist present
        if(wishLists != null && wishLists.length > 0){
            for(int j = 0; j < wishLists.length; j++){
                if(details.hotel_id == wishLists[j].hotelId){
                    details.isWishListPresent = true;
                    break;
                }
            }
        }
        if(details.isWishListPresent){
            imgViewHeartFill.setVisibility(View.VISIBLE);
        }else{
            imgViewHeartOutline.setVisibility(View.VISIBLE);
        }

//        Calculate rooms and availability
        calculateRooms();
        checkRoomAvailability();

//        Setting hotel features
        loadFeaturesAdapter();
    }

    @Override
    public void onHotelImageClicked(String imageUrl) {
        if(bottomSheetBehavior.getState() == BottomSheetBehavior.STATE_COLLAPSED){
            bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
        }else{
            Intent intent = new Intent(this, PreviewImageActivity.class);
            intent.putExtra("imageUrl", imageUrl);
            startActivity(intent);
        }
    }

    @Override
    public void onAddToWishListSuccess() {
        imgViewHeartFill.setVisibility(View.VISIBLE);
        imgViewHeartOutline.setVisibility(View.GONE);
    }

    @Override
    public void onDeleteFromWishListSuccess() {
        imgViewHeartFill.setVisibility(View.GONE);
        imgViewHeartOutline.setVisibility(View.VISIBLE);
    }

    @Override
    public void showProgressBar(String message) {
        progressBarLayout.setVisibility(View.VISIBLE);
        txtProgressBar.setText(message);
    }

    @Override
    public void hideProgressBar() {
        progressBarLayout.setVisibility(View.GONE);
    }


    @OnClick(R.id.img_view_map) void onMapIconClicked(){
        try {
            Geocoder coder= new Geocoder(this);
            List<Address> address;

            try
            {
                address = coder.getFromLocationName(area, 5);
                if(address==null)
                {
                    Toast.makeText(this, "Address cannot be found", Toast.LENGTH_SHORT).show();
                }
                Address location = address.get(0);
                location.getLatitude();
                location.getLongitude();
                goToMapAcitivty(location.getLatitude(), location.getLongitude());

            }
            catch (Exception e)
            {

                try
                {
                    address = coder.getFromLocationName(city, 5);
                    if(address==null)
                    {
                        Toast.makeText(this, "Address cannot be found", Toast.LENGTH_SHORT).show();
                    }
                    Address location = address.get(0);
                    location.getLatitude();
                    location.getLongitude();
                    goToMapAcitivty(location.getLatitude(), location.getLongitude());

                }
                catch (Exception ex)
                {

                    Toast.makeText(this, "Something went wrong", Toast.LENGTH_SHORT).show();
                    Log.e("ErrorMsg", ex.getMessage());
                    e.printStackTrace();
                }
                e.printStackTrace();
            }
        }catch (NullPointerException e){

        }
    }

    void goToMapAcitivty(double latitude, double longitude){
        Intent intent = new Intent(this, MapActivity.class);
        intent.putExtra("latitude", latitude);
        intent.putExtra("longitude", longitude);
        startActivity(intent);
    }

    void calculateRooms(){
        singleRoom = 0;
        doubleRoom = 0;
        extraPerson = 0;
        totalChildCount = 0;
        totalAdultCount = 0;
        rooms = androidApplication.getRooms();
        for(int i = 0; i < rooms.size(); i++){
            totalAdultCount = totalAdultCount + rooms.get(i).adults;
            totalChildCount = totalChildCount + rooms.get(i).child;
            if(rooms.get(i).adults + rooms.get(i).child == 4){
                doubleRoom++;
                extraPerson++;
            }else{
                switch (rooms.get(i).adults){
                    case 3:
                        doubleRoom++;
                        extraPerson++;
                        break;
                    case 2:
                        doubleRoom++;
                        break;
                    case 1:
                        if(rooms.get(i).adults + rooms.get(i).child > 2){
                            doubleRoom++;
                        }else{
                            singleRoom++;
                        }
                        break;
                }
            }
        }
    }

    void checkRoomAvailability(){
        long numberOfDays = getNumberOfDays(checkInDate, checkOutDate);
        if(doubleRoom > totalDoubleRoom){
            roomStatus = "Your requirement cannot be processed";
            personCount = "Double room available: " + totalDoubleRoom + ", Single room available: " + totalSingleRoom;
            totalPrice = 0.0;
            setBookingDetails(roomStatus, personCount, 0);
        }else if(singleRoom  > totalSingleRoom){
            if(totalSingleRoom == 0){
                if(singleRoom + doubleRoom > totalDoubleRoom){
                    roomStatus = "Your requirement cannot be processed";
                    personCount = "Double room available: " + totalDoubleRoom + ", Single room available: " + totalSingleRoom;
                    totalPrice = 0.0;
                    setBookingDetails(roomStatus, personCount, 0);
                }else{
                    doubleRoom = doubleRoom + singleRoom;
                    roomStatus = doubleRoom + " Double Rooms";
                    personCount = totalAdultCount + " Adults, " + totalChildCount + " children";
                    totalPrice = doubleRoom * doubleRoomPrice + extraPerson * extraPersonPrice;
                    totalPrice = numberOfDays * totalPrice;
                    singleRoom = 0;
                    setBookingDetails(roomStatus, personCount, totalPrice);
                }
            }else{
                int remaining_single_room = singleRoom - totalSingleRoom;
                singleRoom = totalSingleRoom;
                if(remaining_single_room + doubleRoom > totalDoubleRoom){
                    roomStatus = "Your requirement cannot be processed";
                    personCount = "Double room available: " + totalDoubleRoom + ", Single room available: " + totalSingleRoom;
                    totalPrice = 0.0;
                    setBookingDetails(roomStatus, personCount, 0);
                }else{
                    doubleRoom = doubleRoom + remaining_single_room;
                    roomStatus = singleRoom + " Single Room, " + doubleRoom + " Double Room";
                    personCount = totalAdultCount + " Adults, " + totalChildCount + " children";
                    totalPrice = singleRoom * singleRoomPrice + doubleRoom * doubleRoomPrice + extraPerson * extraPersonPrice;
                    totalPrice = numberOfDays * totalPrice;
                    setBookingDetails(roomStatus, personCount, totalPrice);
                }
            }
        }else{
            if(doubleRoom == 0){
                roomStatus = singleRoom + " Single Room" ;
            }else if(singleRoom == 0){
                roomStatus = doubleRoom + " Double Room";
            }else {
               roomStatus = singleRoom + " Single Room, " + doubleRoom + " Double Room";
            }
            personCount = totalAdultCount + " Adult, " + totalChildCount + " Child";
            totalPrice = singleRoom * singleRoomPrice + doubleRoom * doubleRoomPrice + extraPerson * extraPersonPrice;
            totalPrice = numberOfDays * totalPrice;
            setBookingDetails(roomStatus, personCount, totalPrice);
        }
    }

    void setBookingDetails(String roomStatus, String personCount, double price){
        txtViewRoomStatus.setText(roomStatus);
        txtViewPersonCount.setText(personCount);
        txtViewTotalPrice.setText(Double.toString(price));
    }

    @Override
    public void setFinalRoomStatus(int adultCount, int childCount, int roomCount) {
        calculateRooms();
        checkRoomAvailability();
    }

    @OnClick(R.id.btn_room_edit) void onRoomEditClicked(){
        roomSelectDialog.showDialog();
    }

    @OnClick(R.id.img_view_back) void onBackClicked(){
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @OnClick(R.id.img_view_heart_outline) void onHeartOutlineClicked(){
        try {
            mPresenter.addToWishList(hotelId);
        } catch (NullPointerException e){

        }
    }

    @OnClick(R.id.img_view_heart_fill) void onHeartFillClicked(){
        try {
            mPresenter.removeFromWishList(hotelId);
        }catch (NullPointerException e){

        }
    }

    public void loadFeaturesAdapter(){
        hotelFeatures = new ArrayList<>();

//        Initialising the features
        HotelFeatures featureAC;
        HotelFeatures featureGym;
        HotelFeatures featureInternet;
        HotelFeatures featureRoomService;
        HotelFeatures featureRestaurant;
        HotelFeatures featureBar;
        HotelFeatures featureTV;
        HotelFeatures featureParking;
        HotelFeatures featureSwimming;

        if(hotelDetails.gym == 2){
            featureGym = new HotelFeatures("Gym", true);
        }else{
            featureGym = new HotelFeatures("Gym", false);
        }

        if(hotelDetails.ac == 2){
            featureAC = new HotelFeatures("AC", true);
        }else{
            featureAC = new HotelFeatures("AC", false);
        }

        if(hotelDetails.freeInternet == 2){
            featureInternet = new HotelFeatures("Internet", true);
        }else{
            featureInternet = new HotelFeatures("Internet", false);
        }

        if(hotelDetails.roomService == 2){
            featureRoomService = new HotelFeatures("RoomService", true);
        }else{
            featureRoomService = new HotelFeatures("RoomService", false);
        }

        if(hotelDetails.restaurant == 2){
            featureRestaurant = new HotelFeatures("Restaurant", true);
        }else{
            featureRestaurant = new HotelFeatures("Restaurant", false);
        }

        if(hotelDetails.bar == 2){
            featureBar = new HotelFeatures("Bar", true);
        }else{
            featureBar = new HotelFeatures("Bar", false);
        }

        if(hotelDetails.tv == 2){
            featureTV = new HotelFeatures("TV", true);
        }else {
            featureTV = new HotelFeatures("TV",  false);
        }

        if(hotelDetails.parking == 2){
            featureParking = new HotelFeatures("Parking", true);
        }else{
            featureParking = new HotelFeatures("Parking", false);
        }

        if(hotelDetails.swimmingPool == 2){
            featureSwimming = new HotelFeatures("Swimming", true);
        }else{
            featureSwimming = new HotelFeatures("Swimming", false);
        }

//        Inserting to the feature array list
        hotelFeatures.add(featureAC);
        hotelFeatures.add(featureGym);
        hotelFeatures.add(featureInternet);
        hotelFeatures.add(featureRoomService);
        hotelFeatures.add(featureRestaurant);
        hotelFeatures.add(featureBar);
        hotelFeatures.add(featureTV);
        hotelFeatures.add(featureParking);
        hotelFeatures.add(featureSwimming);

//        Setting the adapter
        HotelFeaturesAdapter featuresAdapter = new HotelFeaturesAdapter(this, hotelFeatures);
        gridLayoutManager = new GridLayoutManager(this, 3);
        recyclerViewHotelFeatures.setLayoutManager(gridLayoutManager);
        recyclerViewHotelFeatures.setAdapter(featuresAdapter);
    }

    @Override
    public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
        if(isCheckIn) {
            Calendar c = Calendar.getInstance();
            c.set(Calendar.YEAR, i);
            c.set(Calendar.MONTH, i1);
            c.set(Calendar.DAY_OF_MONTH, i2);
            checkInDate = DateFormat.getDateInstance().format(c.getTime());
            if(!compareDates(checkInDate, checkOutDate)){
                SimpleDateFormat format = new SimpleDateFormat("MMM dd, yyyy");
                Date dt = new Date();
                try {
                    dt = format.parse(checkInDate);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                c = Calendar.getInstance();
                c.setTime(dt);
                c.add(Calendar.DATE, 1);
                checkOutDate = DateFormat.getDateInstance().format(c.getTime());
            }
        }else{
            Calendar c = Calendar.getInstance();
            c.set(Calendar.YEAR, i);
            c.set(Calendar.MONTH, i1);
            c.set(Calendar.DAY_OF_MONTH, i2);
            checkOutDate = DateFormat.getDateInstance().format(c.getTime());
        }
        androidApplication.setDates(checkInDate, checkOutDate);
        setDates(checkInDate, checkOutDate);
        showProgressBar("Fetching hotel details");
        mPresenter.getHotelDetails(hotelId);
    }

    public boolean compareDates(String checkIn, String checkOut){
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("MMM dd, yyyy", Locale.US);
            Date checkInDateObj = sdf.parse(checkIn);
            Date checkOutDateObj = sdf.parse(checkOut);
            if(checkInDateObj.before(checkOutDateObj)){
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public void setupDates(){
        dialogFragment = new DatePickerFragment();
        androidApplication = (AndroidApplication) getApplicationContext();
        checkInDate = androidApplication.getCheckInDate();
        checkOutDate = androidApplication.getCheckOutDate();
        setDates(checkInDate, checkOutDate);

    }

    public void setDates(String checkInDateString, String checkOutDateString){
        txtViewCheckInDate.setText(checkInDateString);
        txtViewCheckOutDate.setText(checkOutDateString);
    }

    @OnClick(R.id.img_view_check_in) void checkInClicked(){
        isCheckIn = true;
        Bundle args = new Bundle();
        Date dt = Calendar.getInstance().getTime();
        args.putString("minimumDate", DateFormat.getDateInstance().format(dt));
        args.putString("currentDate", checkInDate);
        dialogFragment.setArguments(args);
        dialogFragment.show(getSupportFragmentManager(), "date picker");
    }

    @OnClick(R.id.img_view_check_out) void checkOutClicked(){
        isCheckIn = false;
        SimpleDateFormat format = new SimpleDateFormat("MMM dd, yyyy");
        Date dt = new Date();
        try {
            dt = format.parse(checkInDate);
        } catch (ParseException e) {
            Log.e("ErrorMsg", e.getMessage());
            e.printStackTrace();
        }
        Calendar c = Calendar.getInstance();
        c.setTime(dt);
        c.add(Calendar.DATE, 1);
        Bundle args = new Bundle();
        args.putString("minimumDate", DateFormat.getDateInstance().format(c.getTime()));
        args.putString("currentDate", checkOutDate);
        dialogFragment.setArguments(args);
        dialogFragment.show(getSupportFragmentManager(), "date picker");
    }

    @OnClick(R.id.btn_book) void onBookingClicked(){
        String duration = checkInDate + " - " + checkOutDate;

        bookingConfirmationDialog.showDialog(hotelDetails.hotel_name, totalPrice, personCount, roomStatus, duration);

    }

    public long getNumberOfDays(String checkIn, String checkOut){
        SimpleDateFormat dateFormat = new SimpleDateFormat("MMM dd, yyyy");
        Date startDate = null;
        Date endDate = null;
        try {
            startDate = dateFormat.parse(checkIn);
            endDate = dateFormat.parse(checkOut);
            long timeDiff = endDate.getTime() - startDate.getTime();
            return TimeUnit.DAYS.convert(timeDiff, TimeUnit.MILLISECONDS);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return 0;
    }

    @Override
    public void requestRoom() {
        try {
            showProgressBar("Checking Availability");
            mPresenter.checkHotelAvailability(hotelDetails.hotel_id, singleRoom, doubleRoom, totalAdultCount, totalChildCount, extraPerson);
        }catch (NullPointerException e){
            hideProgressBar();
        }

    }

    @Override
    public void onBookingSuccess(BookingConfirm bookingConfirm) {
        totalPayableAmount = bookingConfirm.amount_payable;
        bookingId = bookingConfirm.booking_id;
        Log.e("ErrorMsg", "Booking id: " + bookingId);
        String duration = checkInDate + " - " + checkOutDate;
        bookingSuccessDialog.showDialog(hotelDetails.hotel_name, totalPayableAmount, personCount, roomStatus, duration);
    }

    @Override
    public void onPayNowClick() {
        String amount = Double.toString(totalPayableAmount);
        String purpose = roomStatus + " booked for " + personCount;
        Intent paymentIntent = new Intent(this, PaymentActivity.class);
        paymentIntent.putExtra("hotelName", hotelDetails.hotel_name);
        paymentIntent.putExtra("amount", amount);
        paymentIntent.putExtra("purpose", purpose);
        paymentIntent.putExtra("bookingId", bookingId);
        startActivity(paymentIntent);
        finish();
    }

    @Override
    public void onPayLaterClicked() {

    }
}
