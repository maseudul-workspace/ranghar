package in.net.webinfotech.ranghar.presentation.presenters.impl;

import in.net.webinfotech.ranghar.domain.executor.Executor;
import in.net.webinfotech.ranghar.domain.executor.MainThread;
import in.net.webinfotech.ranghar.presentation.presenters.ForgetPasswordPresenter;
import in.net.webinfotech.ranghar.presentation.presenters.base.AbstractPresenter;

/**
 * Created by Raj on 21-03-2019.
 */

public class ForgetPasswordPresenterImpl extends AbstractPresenter implements ForgetPasswordPresenter {

    public ForgetPasswordPresenterImpl(Executor executor, MainThread mainThread) {
        super(executor, mainThread);
    }

    @Override
    public void changePassword(String email, String password) {

    }

    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void stop() {

    }

    @Override
    public void destroy() {

    }
}
